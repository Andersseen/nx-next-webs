export interface NavigationLink {
  label: string;
  route: string;
}

export interface HeaderData {
  srcImg: string;
  title: string;
  subtitle: string;
  pages: PageBreadCrumbs;
}

export interface PageBreadCrumbs {
  prevPage: string;
  currentPage: string;
}
