export interface FeatureContentData {
  title: string;
  paragraphs: string[];
  button: Button;
  image: Image;
}
export interface TextContentData {
  title: string;
  paragraphs: string[];
}

// export interface ParagraphsData {
//   row: string;
// }

export interface Image {
  srcImg: string;
  altImg: string;
}

export interface Button {
  name: string;
  color:
    | 'default'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'warning'
    | 'danger'
    | undefined;
  size?: string;
}
