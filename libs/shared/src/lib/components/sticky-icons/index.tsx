import Link from 'next/link';

type StickyIconsProps = {
  stickyIconsData: StickyIconsData[];
};

export type StickyIconsData = {
  title: string;
  route: string;
  icon: JSX.Element;
};

export const StickyIcons = ({ stickyIconsData }: StickyIconsProps) => {
  return (
    <div className="z-20 fixed top-80 left-0 w-36 flex flex-col">
      {stickyIconsData.map((item, index) => (
        <Link
          key={index}
          target="_blank"
          href={item.route}
          className="transform -translate-x-24 border-r-50 text-left uppercase  hover:translate-x-0 transition-all duration-800 bg-main-500 px-2 rounded-r-full"
          rel="noreferrer"
        >
          {item.title === 'Instagram' ? (
            <p className="flex items-center">
              {item.title}
              {item.icon}
            </p>
          ) : (
            <p className="flex items-center gap-2">
              {item.title}
              {item.icon}
            </p>
          )}
        </Link>
      ))}
    </div>
  );
};
