'use client';
import { Breadcrumbs } from './breadcrumbs';
import { HeaderData } from '../../utils';
import { motion } from 'framer-motion';

export const HeaderSection = ({
  headerData,
  resolvedTheme,
}: {
  headerData: HeaderData;
  resolvedTheme: string;
}) => {
  const filters = {
    light: {
      brightness: 'brightness-100',
      saturate: 'contrast-50',
      bgColor: 'bg-main-100',
    },
    dark: {
      brightness: 'brightness-50',
      saturate: 'saturate-50',
      bgColor: 'bg-main-900',
    },
  };

  type ThemeType = 'light' | 'dark';
  const correctThemeType = resolvedTheme as ThemeType;

  const themeFilters = filters[correctThemeType] || filters.light;
  return (
    <section
      className={`bg-center bg-no-repeat ${themeFilters.brightness} ${themeFilters.saturate} bg-fixed shadow-xl`}
      style={{ backgroundImage: `url(${headerData.srcImg})` }}
    >
      <div className="px-4 mx-auto relative max-w-screen-xl text-center py-24 lg:py-56 flex flex-col justify-center">
        <motion.h6
          variants={{
            hidden: { opacity: 0, y: 75 },
            visible: { opacity: 1, y: 0 },
          }}
          initial="hidden"
          animate="visible"
          transition={{
            duration: 0.5,
            delay: 0.3,
            ease: 'easeIn',
          }}
          className={`w-fit self-center mb-8 text-lg font-normal lg:text-xl sm:px-16 lg:px-48 ${themeFilters.bgColor}`}
        >
          {headerData.title}
        </motion.h6>

        <motion.h1
          variants={{
            hidden: { opacity: 0, x: 75 },
            visible: { opacity: 1, x: 0 },
          }}
          initial="hidden"
          animate="visible"
          transition={{ duration: 0.5, delay: 0.3, ease: 'easeIn' }}
          className={`w-fit self-center mb-4 text-4xl font-extrabold tracking-tight leading-none md:text-5xl lg:text-6xl ${themeFilters.bgColor}`}
        >
          {headerData.subtitle}
        </motion.h1>
        <motion.div
          variants={{
            hidden: { opacity: 0, y: -75 },
            visible: { opacity: 1, y: 0 },
          }}
          initial="hidden"
          animate="visible"
          transition={{ duration: 0.5, delay: 0.3, ease: 'easeIn' }}
          className={`flex flex-col w-fit self-center space-y-4 sm:flex-row sm:justify-center sm:space-y-0 sm:space-x-4 ${themeFilters.bgColor}`}
        >
          <Breadcrumbs pages={headerData.pages} />
        </motion.div>
      </div>
    </section>
  );
};
