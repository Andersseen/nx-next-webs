import Link from 'next/link';

export type BreadcrumbsProps = {
  pages: {
    prevPage: string;
    currentPage: string;
  };
};

export const Breadcrumbs = ({ pages }: BreadcrumbsProps) => {
  return (
    <nav className="w-full rounded-md flex justify-center z-10">
      <ol className="list-reset flex">
        <li>
          <Link href="/">{pages.prevPage}</Link>
        </li>
        <li>
          <span className="mx-2 text-neutral-500 dark:text-neutral-400">/</span>
        </li>
        <li className="text-neutral-500 dark:text-neutral-400">
        <Link href="/"> {pages.currentPage}</Link>
         
        </li>
      </ol>
    </nav>
  );
};
