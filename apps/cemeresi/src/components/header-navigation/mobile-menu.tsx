/* eslint-disable react-hooks/rules-of-hooks */
'use client';
import React from 'react';
import { Accordion, AccordionItem, Button } from '@nextui-org/react';
import Link from 'next/link';

export default function MobileMenu({ listMenu, onCloseMenu }) {
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([null]));
  return listMenu.map((item, index) => (
    <React.Fragment key={`${item.id}-${index}`}>
      {item.links.length ? (
        <Accordion
          variant="bordered"
          className="text-center px-4"
          selectedKeys={selectedKeys}
          onSelectionChange={setSelectedKeys}
        >
          <AccordionItem
            key={index}
            aria-label="Accordion 1"
            title={item.name}
            className="text-center"
          >
            <RenderItems item={item.links} onCloseMenu={onCloseMenu} />
          </AccordionItem>
        </Accordion>
      ) : (
        <Button
          key={`${item.id}-${index}`}
          className="w-full py-8"
          href={item.route}
          as={Link}
          variant="flat"
          size="lg"
        >
          <span className="w-full text-start">{item.name}</span>
        </Button>
      )}
    </React.Fragment>
  ));
}

const RenderItems = ({ item, onCloseMenu }) => {
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([null]));
  return item.map(({ id, name, route, children }, key) => (
    <div key={key} className="w-full">
      {children ? (
        <Accordion
          variant="light"
          className="text-centert px-0"
          selectedKeys={selectedKeys}
          onSelectionChange={setSelectedKeys}
        >
          <AccordionItem
            key={key}
            aria-label="Accordion 1"
            title={name}
            className="text-center pl-10"
          >
            <RenderItems item={children} onCloseMenu={onCloseMenu} />
          </AccordionItem>
        </Accordion>
      ) : (
        <Button
          className="w-full p-2 rounded-md border pl-10"
          href={route}
          as={Link}
          variant="bordered"
          size="lg"
          onClick={onCloseMenu}
        >
          <span className="w-full text-start text-md"> {name}</span>
        </Button>
      )}
    </div>
  ));
};
