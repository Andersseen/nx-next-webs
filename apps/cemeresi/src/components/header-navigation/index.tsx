'use client';
import React from 'react';
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarMenuToggle,
  NavbarMenu,
} from '@nextui-org/react';
import Link from 'next/link';
import { menuItems } from '@cemeresi/data/links';
import MobileMenu from './mobile-menu';
import NavbarWithMegaMenu from './desktop-menu';

export default function HeaderNavigation({
  logo,
  themeSwitcher,
}: {
  logo: React.ReactNode | string;
  themeSwitcher: React.ReactNode;
}) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const closeMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <Navbar isMenuOpen={isMenuOpen} onMenuOpenChange={closeMenu}>
      <NavbarContent justify="start">
        <NavbarMenuToggle aria-label="Toggle Menu" className="lg:hidden" />
        <NavbarBrand>
          <Link href="/">{logo}</Link>
        </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden lg:flex gap-4" justify="center">
        <NavbarWithMegaMenu listMenu={menuItems} />
      </NavbarContent>
      <NavbarContent justify="end">{themeSwitcher}</NavbarContent>
      <NavbarMenu>
        <MobileMenu listMenu={menuItems} onCloseMenu={closeMenu} />
      </NavbarMenu>
    </Navbar>
  );
}
