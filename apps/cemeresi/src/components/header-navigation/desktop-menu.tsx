'use client';
import React from 'react';
import {
  List,
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
  Button,
} from '@material-tailwind/react';
import { ArrowUpToDownRounded, ArrowLeftToRightRounded } from '@nx-next/shared';
import Link from 'next/link';

export default function NavbarWithMegaMenu({ listMenu }) {
  return (
    <List className="mt-4 mb-6 p-0 lg:mt-0 lg:mb-0 lg:flex-row lg:p-1">
      {listMenu.map((item, index) => (
        <React.Fragment key={index}>
          {item.links.length ? (
            <NavListMenu item={item} />
          ) : (
            <Button
              variant="text"
              className="text-[var(--foreground-color)]  hover:bg-main-400 whitespace-nowrap"
            >
              {item.name}
            </Button>
          )}
        </React.Fragment>
      ))}
    </List>
  );
}

const NavListMenu = ({ item }) => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  return (
    <Menu
      animate={{
        mount: { y: 0 },
        unmount: { y: 25 },
      }}
      open={isMenuOpen}
      handler={setIsMenuOpen}
      offset={{ mainAxis: 15 }}
      placement="bottom"
      allowHover
    >
      <MenuHandler>
        <Button
          variant="text"
          className={`text-[var(--foreground-color)] hover:bg-main-400 ${
            isMenuOpen ? 'bg-main-400' : ''
          } p-2 whitespace-nowrap flex`}
        >
          <span className="self-center">{item.name}</span>
          <ArrowUpToDownRounded isHovered={isMenuOpen} />
        </Button>
      </MenuHandler>
      <MenuList className="hidden rounded-xl lg:block w-fit bg-[var(--main-color)] border-none">
        <RenderItems item={item.links} />
      </MenuList>
    </Menu>
  );
};

// eslint-disable-next-line react/display-name
const RenderItems = React.forwardRef<HTMLDivElement, { item }>(
  ({ item }, ref) => {
    return item.map(({ name, route, children }, key) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const [isMenuOpen, setIsMenuOpen] = React.useState(false);

      return (
        <div key={key} ref={ref}>
          {children ? (
            <Menu
              placement="right-start"
              allowHover
              offset={15}
              open={isMenuOpen}
              handler={setIsMenuOpen}
            >
              <MenuHandler
                className={`flex items-center justify-between ${
                  isMenuOpen ? 'bg-main-400' : ''
                }`}
              >
                <MenuItem className="uppercase">
                  {name}
                  <ArrowLeftToRightRounded isHovered={isMenuOpen} />
                </MenuItem>
              </MenuHandler>
              <MenuList className="hidden rounded-xl lg:block w-fit bg-[var(--main-color)]">
                <RenderItems item={children} />
              </MenuList>
            </Menu>
          ) : (
            <Link href={route}>
              <MenuItem className="uppercase">{name}</MenuItem>
            </Link>
          )}
        </div>
      );
    });
  }
);
