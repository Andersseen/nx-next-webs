'use client';
import React from 'react';
import { motion } from 'framer-motion';

export default function HeroSecond() {
  return (
    <section className="mx-auto max-w-7xl px-4 py-12">
      <div className="mb-8 flex flex-col items-start justify-between gap-4 md:flex-row md:items-end md:px-8">
        <h2 className="max-w-lg text-4xl font-bold md:text-5xl">
          Alguna información
        </h2>
      </div>
      <div className="mb-4 grid grid-cols-12 gap-4">
        <BounceCard className="col-span-12 md:col-span-4" img="/img/5-min.webp">
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold ">CAPILAR</span>
          </div>
        </BounceCard>
        <BounceCard className="col-span-12 md:col-span-8" img="/img/1-min.webp">
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold ">
              MEDICINA ESTÉTICA
            </span>
          </div>
        </BounceCard>
      </div>
      <div className="mb-4 grid grid-cols-12 gap-4">
        <BounceCard
          className="col-span-12 md:col-span-8"
          img="/img/12-min.webp"
        >
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold">
              CIRUGÍA ESTÉTICA
            </span>
          </div>
        </BounceCard>
        <BounceCard className="col-span-12 md:col-span-4" img="/img/8-min.webp">
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold">
              FISIOTERAPIA
            </span>
          </div>
        </BounceCard>
      </div>
      <div className="mb-4 grid grid-cols-12 gap-4">
        <BounceCard className="col-span-12 md:col-span-4" img="/img/7-min.webp">
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold ">PSICOLOGÍA</span>
          </div>
        </BounceCard>
        <BounceCard className="col-span-12 md:col-span-8" img="/img/2-min.webp">
          <div className="absolute bottom-0 left-4 right-4 top-32 translate-y-8 rounded-t-2xl bg-gradient-to-br from-main-400/60 to-main-800/60 p-4 transition-transform duration-[250ms] group-hover:translate-y-4 group-hover:rotate-[2deg]">
            <span className="block text-center font-semibold ">
              ESTÉTICA INTEGRAL
            </span>
          </div>
        </BounceCard>
      </div>
    </section>
  );
}

const BounceCard = ({
  img,
  className,
  children,
}: {
  img?: string;
  className: string;
  children: React.ReactNode;
}) => {
  return (
    <motion.div
      whileHover={{ scale: 0.95, rotate: '-1deg' }}
      className={`group relative min-h-[300px] cursor-pointer overflow-hidden rounded-2xl p-8 ${className}`}
      style={{
        backgroundImage: `url(${img})`,
        backgroundSize: 'cover',
      }}
    >
      {children}
    </motion.div>
  );
};
