'use client';
import { Gradient } from 'whatamesh';
import React, { useEffect } from 'react';

export default function Hero() {
  useEffect(() => {
    const canvas = document.getElementById('gradient-canvas');
    if (canvas) {
      const gradient: any = new Gradient();
      gradient.initGradient('#gradient-canvas');
    }
  }, []);

  return (
    <section className="relative">
      <canvas className="min-h-screen" id="gradient-canvas"></canvas>
      <header className="absolute inset-0 flex items-center min-h-screen justify-center z-10">
        <div className="text-center">
          <h1 className="text-4xl sm:text-5xl md:text-6xl lg:text-8xl xl:text-9xl drop-shadow-xl">
            Clínica Cemeresi
          </h1>
          <p className="text-base sm:text-lg md:text-xl lg:text-2xl xl:text-3xl drop-shadow-lg">
            Su clínica de medicina y cirugía preferida
          </p>
        </div>
      </header>
    </section>
  );
}
