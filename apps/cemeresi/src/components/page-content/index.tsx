'use client';
import {
  HeaderData,
  LightWrapper,
  HeaderSection,
  FeatureContent,
  FeatureContentData,
} from '@nx-next/shared';
import { useTheme } from 'next-themes';
import ListContent from '@cemeresi/components/list-content';
import { BodyTreatment } from '@cemeresi/data/esthetics/facial-aesthetic';
import TabsContent, { Tab } from '../tabs-content';

export const PageFeatureContent = ({
  headerData,
  featureContentData,
}: {
  headerData: HeaderData;
  featureContentData: FeatureContentData;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      <LightWrapper>
        <FeatureContent featureContentData={featureContentData} />
      </LightWrapper>
    </>
  );
};

export const PageListContent = ({
  headerData,
  facialTreatmentsData,
}: {
  headerData: HeaderData;
  facialTreatmentsData: BodyTreatment[];
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      <ListContent facialTreatments={facialTreatmentsData} />
    </>
  );
};

export const PageTabsContent = ({
  headerData,
  params,
  tabs,
  withoutLight,
}: {
  headerData: HeaderData;
  params: string;
  tabs: Tab[];
  withoutLight?: boolean;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      {withoutLight ? (
        <TabsContent tabs={tabs} param={params} />
      ) : (
        <LightWrapper>
          <TabsContent tabs={tabs} param={params} />
        </LightWrapper>
      )}
    </>
  );
};

export const PageContent = ({
  headerData,
  children,
}: {
  headerData: HeaderData;
  children: React.ReactNode;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      {children}
    </>
  );
};
