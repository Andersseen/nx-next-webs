'use client';
import React, { Key } from 'react';
import { Tabs, Tab } from '@nextui-org/react';

export interface Tab {
  title: string;
  key: string;
  haveIcon?: boolean;
  iconTitle?: JSX.Element;
  page: JSX.Element;
}

type TabsContentProps = {
  tabs: Tab[];
  param: string;
};

export default function TabsContent({ tabs, param }: TabsContentProps) {
  const [selected, setSelected] = React.useState<Key>(param);
  React.useEffect(() => {
    setSelected(param);
  }, [param]);

  return (
    <div className="flex w-full flex-col mt-32">
      <Tabs
        className="justify-center"
        aria-label="Options"
        variant="bordered"
        color="secondary"
        size="lg"
        selectedKey={selected}
        onSelectionChange={(key) => setSelected(key)}
      >
        {tabs.map((item: Tab, index: number) =>
          item.haveIcon ? (
            <Tab
              key={item.key}
              title={
                <div className="flex items-center space-x-2">
                  {item.iconTitle}
                  <span>{item.title}</span>
                </div>
              }
            >
              {item.page}
            </Tab>
          ) : (
            <Tab key={item.key} title={item.title}>
              {item.page}
            </Tab>
          )
        )}
      </Tabs>
    </div>
  );
}
