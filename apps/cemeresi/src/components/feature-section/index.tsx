'use client';
import { Image, Button } from '@nextui-org/react';
import Link from 'next/link';

type FeatureSectionProps = {
  item: any;
  link: string;
};

export default function FeatureSection({ item, link }: FeatureSectionProps) {
  return (
    <section className="min-h-screen w-full shadow-xl sm:grid sm:grid-cols-2 pt-12">
      <div className="p-8 md:p-12 lg:px-16 lg:py-24">
        <div className="mx-auto max-w-xl text-center ltr:sm:text-left rtl:sm:text-right">
          <h2 className="text-2xl font-bold md:text-3xl">{item?.title}</h2>
          <div className="text-left">
            <p className="md:mt-4 md:block">{item?.description}</p>
          </div>
          <div className="mt-4 md:mt-8">
            <Link href={link}>
              <Button color="secondary">Volver</Button>
            </Link>
          </div>
        </div>
      </div>
      <Image alt="image" src="/3.jpg" />
    </section>
  );
}
