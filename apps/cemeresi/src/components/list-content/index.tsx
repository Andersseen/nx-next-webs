'use client';
import React from 'react';
import { SVGProps } from 'react';
import { BodyTreatment } from '@cemeresi/data/esthetics/facial-aesthetic';
import ModalComponent from '../modal';

type ListContentProps = {
  pageTitle?: string | null;
  facialTreatments: BodyTreatment[];
};

export default function ListContent({
  pageTitle,
  facialTreatments,
}: ListContentProps) {
  const [isOpen, setBoolToModal] = React.useState(false);
  const [treatment, setTreatment] = React.useState({});

  const handleModalChange = (bool, treatment) => {
    setTreatment(treatment);
    setBoolToModal(bool);
  };

  return (
    <section className="w-full px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8 shadow-xl">
      <div className="mx-auto max-w-lg text-center">
        <h2 className="text-3xl font-bold sm:text-4xl">{pageTitle}</h2>
      </div>
      <div className="mt-8 lg:px-8 grid grid-cols-1 gap-8 md:grid-cols-2 lg:grid-cols-3 text-center">
        {facialTreatments.map((item) => (
          <Card
            key={item.id}
            item={item}
            bool={isOpen}
            boolChange={handleModalChange}
          />
        ))}
      </div>
      <ModalComponent
        treatment={treatment}
        isOpen={isOpen}
        onOpenChange={setBoolToModal}
      />
    </section>
  );
}

export const Card = ({ item, bool, boolChange }) => {
  return (
    <button
      className="w-full rounded-xl relative overflow-hidden group border border-[var(--main-color)] p-2 shadow-xl transition hover:border-[var(--foreground-color)] hover:shadow-main-400/10"
      onClick={() => boolChange(!bool, item)}
    >
      <div className="absolute inset-0 bg-gradient-to-r from-main-600 to-main-400 translate-y-[100%] group-hover:translate-y-[0%] transition-transform duration-300" />

      <SpaRoundedIcon className="absolute z-10 -top-12 -right-12 text-9xl text-main-600 group-hover:text-main-400 group-hover:rotate-12 transition-transform duration-300" />
      <SpaRoundedIcon className="mb-2 text-2xl text-main-400 group-hover:text-200 transition-colors relative z-10 duration-300" />
      <h3 className="font-medium text-lg text-[var(--foreground-color)] group-hover:text-[var(--main-color)] relative z-10 duration-300">
        {item.title}
      </h3>
    </button>
  );
};
export const SpaRoundedIcon = (props: SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        fill="currentColor"
        d="M13 20.85q.05.475-.313.813t-.812.262q-4.35-.65-6.9-3.363t-2.9-7.387q-.05-.5.288-.838t.837-.262q4.575.625 7.05 3.525T13 20.85ZM11.225 2.975q.3-.425.788-.412T12.8 3q1.125 1.575 1.963 3.45t1.012 3.15q-.975.45-2.175 1.463t-1.575 1.562q-.35-.55-1.613-1.613T8.275 9.6q.2-1.25 1.012-3.162t1.938-3.463Zm9.65 7.1q.475-.05.8.263t.3.812q-.2 4.025-2.188 6.538T14.976 21.2q-.05-1.525-.462-3.462T13.225 14.4q1.075-1.65 3.188-2.85t4.462-1.475Z"
      />
    </svg>
  );
};
