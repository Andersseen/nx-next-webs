import Link from 'next/link';

export default function NotFound() {
  return (
    <main className="h-screen text-center flex flex-col items-center justify-center">
      <h2 className="text-xl lg:text-3xl">
        No se ha podido encontrar esta página.
      </h2>
      <p>
        Quiere volver al{' '}
        <Link
          className="text-main-200 bg-main-800 dark:text-main-800 dark:bg-main-200 rounded-lg px-2 py-1"
          href="/"
        >
          inicio
        </Link>
      </p>
    </main>
  );
}
