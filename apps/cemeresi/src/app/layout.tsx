import './global.css';
import { LINKS } from '../data/links';
import { FooterWrapper } from '../components/footer-section';
import { Logo } from '../components/logo';
import { StickyIcons, ThemeSwitcher } from '@nx-next/shared';
import Providers from './providers';
import HeaderNavigation from '../components/header-navigation';
import { StickyIconsContent } from '@cemeresi/data/sticky-iconts';

export const metadata = {
  title: 'Clínica Cemeresi',
  description: 'Bienvenido a nuestro centro',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <body className="relative">
        <Providers>
          <HeaderNavigation logo={<Logo />} themeSwitcher={<ThemeSwitcher />} />
          <StickyIcons stickyIconsData={StickyIconsContent} />
          <main
            className="z-10 relative h-min-screen"
            style={{
              background: `var(--main-color)`,
            }}
          >
            {children}
          </main>
          <FooterWrapper navLinks={LINKS} />
        </Providers>
      </body>
    </html>
  );
}
