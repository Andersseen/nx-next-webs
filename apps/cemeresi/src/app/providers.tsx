'use client';
import { NextUIProvider } from '@nextui-org/react';
import { ThemeProvider } from 'next-themes';
import React, { useState, useEffect } from 'react';
import { ThemeProvider as ThemeProviderMaterial } from '@material-tailwind/react';

export default function Providers({ children }: { children: React.ReactNode }) {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) {
    return <>{children}</>;
  }

  const customTheme = {
    menu: {
      styles: {
        base: {
          item: {
            initial: {
              borderRadius: 'rounded-md',
              textAlign: 'text-start whitespace-nowrap',
              lightHeight: 'leading-tight',
              bg: 'hover:bg-main-400 hover:bg-opacity-80 active:bg-blue-gray-50 active:bg-opacity-80',
              color: 'text-[var(--foreground-color)] active:text-blue-gray-900',
            },
          },
        },
      },
    },
  };

  return (
    <NextUIProvider>
      <ThemeProvider
        attribute="class"
        defaultTheme="dark"
        themes={['light', 'dark']}
      >
        <ThemeProviderMaterial value={customTheme}>
          {children}
        </ThemeProviderMaterial>
      </ThemeProvider>
    </NextUIProvider>
  );
}
