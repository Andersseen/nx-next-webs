import Hero from '../components/hero';
import HeroSecond from '../components/hero/hero-second';
import { PageWrapper } from '@nx-next/shared';

export default function Index() {
  return (
    <PageWrapper>
      <section className="shadow-lg">
        <Hero />
        <HeroSecond />
      </section>
    </PageWrapper>
  );
}
