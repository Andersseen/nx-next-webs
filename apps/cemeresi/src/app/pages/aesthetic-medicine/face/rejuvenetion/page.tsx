import { HeaderData, PageWrapper } from '@nx-next/shared';
import { PageListContent } from '../../../../../components/page-content';
import { FACIAL_QUALITY_SKIN_TREATMENTS as facialTreatments } from '../../../../../data/treatment-sections/aesthetics-medicine';

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: '/6.jpg',
    title: 'CLINICA CEMERESI',
    subtitle: 'MEDICINA ESTÉTICA',
    pages: {
      prevPage: 'INICIO',
      currentPage: 'MEDICINA ESTÉTICA - FACIAL - REJUVENECIMIENTO',
    },
  };

  return (
    <PageWrapper>
      <PageListContent
        headerData={headerContent}
        facialTreatmentsData={facialTreatments}
      />
    </PageWrapper>
  );
}
