import { FACIAL_QUALITY_SKIN_TREATMENTS as facialTreatments } from '../../../../../../data/treatment-sections/aesthetics-medicine';
import FeatureSection from '../../../../../../components/feature-section';

async function loadTreatment(id: string) {
  const bothArray = [...facialTreatments];
  const result = bothArray.find((obj) => obj.id === id);
  return result;
}

export default async function Page({ params }) {
  const treatment = await loadTreatment(params.id);

  return (
    <>
      <FeatureSection
        item={treatment}
        link="/pages/aesthetic-medicine/face/quality-skin"
      />
    </>
  );
}
