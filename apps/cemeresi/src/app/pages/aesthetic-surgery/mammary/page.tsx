import { HeaderData, PageWrapper } from '@nx-next/shared';
import { PageListContent } from '../../../../components/page-content';
import { MAMARIA_SURGERY_TREATMENTS as treatments } from '../../../../data/treatment-sections';

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: '/6.jpg',
    title: 'CLINICA CEMERESI',
    subtitle: 'CIRUGÍA ESTÉTICA',
    pages: {
      prevPage: 'INICIO',
      currentPage: 'CIRUGÍA ESTÉTICA - MAMARIA',
    },
  };

  return (
    <PageWrapper>
      <PageListContent
        headerData={headerContent}
        facialTreatmentsData={treatments}
      />
    </PageWrapper>
  );
}
