import { HeaderData, PageWrapper } from '@nx-next/shared';
import { PageListContent } from '../../../../../components/page-content';
import { FACIAL_REJUVENATION_SURGERY_TREATMENTS as facialTreatments } from '../../../../../data/treatment-sections';

export default function Page() {
  const headerContent: HeaderData = {
    srcImg: '/6.jpg',
    title: 'CLINICA CEMERESI',
    subtitle: 'CIRUGÍA ESTÉTICA',
    pages: {
      prevPage: 'INICIO',
      currentPage: 'CIRUGÍA ESTÉTICA - FACIAL - REJUVENECIMIENTO',
    },
  };

  return (
    <PageWrapper>
      <PageListContent
        headerData={headerContent}
        facialTreatmentsData={facialTreatments}
      />
    </PageWrapper>
  );
}
