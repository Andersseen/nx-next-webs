import {
  IconFacebook,
  IconInstagram,
  IconWhatsapp,
  StickyIconsData,
} from '@nx-next/shared';

export const StickyIconsContent: StickyIconsData[] = [
  {
    title: 'Instagram',
    route: 'https://www.instagram.com/clinicacemeresi',
    icon: <IconInstagram />,
  },
  {
    title: 'Facebook',
    route: 'https://www.facebook.com/Cemeresi',
    icon: <IconFacebook />,
  },
  {
    title: 'Whatsapp',
    route: 'https://wa.me/+34637687934',
    icon: <IconWhatsapp />,
  },
];
