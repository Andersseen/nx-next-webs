import { AesteticMedicineTreatment } from '..';

export const BODY_QUALITY_SKIN_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Peeling',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '22222w2',
    title: 'Laser PLX fraccionado',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '33333e3',
    title: 'Eliminación lunares/verrugas',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '444444r4',
    title: 'Escleroterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '55555t5',
    title: 'Sudoración excesiva',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '66666m6',
    title: 'Sueroterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
];
export const BODY_REMODELING_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Mesoterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Carboxiterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Intralipoterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
export const BODY_FLACIDITY_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Carboxiterapia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/',
  },
  {
    id: '22222w2',
    title: ' Ácido poliláctico',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
