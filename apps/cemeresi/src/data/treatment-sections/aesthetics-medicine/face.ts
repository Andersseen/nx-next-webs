import { AesteticMedicineTreatment } from '..';

export const FACIAL_QUALITY_SKIN_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Peeling químico',
    description: `El peeling químico es un tratamiento para renovar las capas más superficiales de la piel y cuyo objetivo es la corrección de diversos defectos de la misma, como manchas, acné, arrugas poco profundas y marcas. La palabra peeling se traduce del inglés como “pelar” y eso es precisamente lo que consigue este tratamiento, retirar las capas dañadas de la piel dejando el rostro con un aspecto mucho más hidratado y jugoso.

    Se recomienda una limpieza facial profunda una semana antes de la realización del peeling.`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '22222w2',
    title: 'Dermapen',
    description: `Dermapen es un dispositivo para tratamientos faciales de Medicina Estética, cuyo objetivo es mejorar el aspecto y la textura de la piel.

    Se trata de una técnica muy sencilla de aplicar y sin efectos secundarios, lo que permite que pueda realizarse con total seguridad sobre todo tipo de pieles independientemente de la edad, estado o pigmentación.
    
    Las sesiones consisten en la realización de microperforaciones en la piel a través de las pequeñas agujas de titanio del dispositivo. El objetivo de realizar estas micropunciones es abrir pequeños canales en la dermis que faciliten la penetración y la absorción de los principios activos que se aplican, consiguiendo maximizar así su penetrabilidad y efectividad, mejorando el resultado que queramos conseguir.
    
    El tratamiento es prácticamente indoloro ya que las agujas del dispositivo son muy finas y antes del tratamiento se aplica anestesia tópica para evitar molestias al paciente. El procedimiento tiene una duración aproximada de 30 minutos y posteriormente se requiere la aplicación de alguna mascarilla o crema calmante. Para conseguir los mejores resultados se recomienda realizar al menos 6 sesiones con intervalos de 15 o 30 días.
    
    Dermapen se puede aplicar en cualquier parte del cuerpo, aunque donde más se utiliza es en el rostro.`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '33333e3',
    title: 'Dermaplasma',
    description: `Derma Plasma creó esta revolucionaria fórmula con Argireline y Ácido Hialurónico, incluidos los antioxidantes Vitamina C y Vitamina E para aumentar la apariencia de una tez firme. La hidratación liberada ayuda a fortalecer la capacidad de la piel para repararse a sí misma y mejorar la complexión general de la piel, lo que le ayuda a retardar los signos de envejecimiento.

    Gracias a su combinación única de antioxidantes potentes como  Retinol, Vitamina C,  Ácido Hialurónico y los Péptidos-8.  Ilumina la apariencia de las ojeras mejorando la apariencia de la edad y la elasticidad de la piel al promover la producción de colágeno, ayudando con la firmeza, reducción de las arrugas y la ingravidez.`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '444444r4',
    title: 'Mesoterapia',
    description: `La mesoterapia corporal es una técnica no invasiva que introduce por vía intradérmica medicamentos homeopáticos destinados a disolver la grasa localizada en distintas zonas del cuerpo, y que estimula el metabolismo ayudando a eliminar esta grasa y reduciendo la celulitis de forma efectiva e indolora.`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '55555t5',
    title: ' Hidratación labios',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
  {
    id: '66666m6',
    title: 'Laser PLX fraccionado',
    description: `El láser fraccionado es un tipo de láser molecular que trabaja con una fuente de calor y dióxido de carbono. El láser emite energía pulsada que genera la vaporización de las capas más superficiales de la piel, favoreciendo así el inicio del proceso de regeneración natural de la piel y las células. ¿El resultado? Una piel completamente nueva, sin ningún tipo de lesión.

    El rejuvenecimiento de la piel mediante láser CO2 es el procedimiento por excelencia para suavizar arrugas, cerrar poros dilatados, atenuar hiperpigmentaciones y suavizar cicatrices de acné u otras causas.
    
    El objetivo es mejorar la textura, luminosidad y calidad de la piel. En SOFT lo empleamos como el complemento ideal de una blefaroplastia, para poder mejorar no solo las bolsas, sino también la piel de los párpados, a veces muy castigada, sin necesidad de cicatrices ni puntos en la piel.`,
    route: '/pages/aesthetic-medicine/face/quality-skin',
  },
];
export const FACIAL_REJUVENATION_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Eliminación arrugas',
    description: `La eliminación de arrugas en el tercio superior se realiza disminuyendo la contracción de los músculos faciales temporalmente, dejando la piel tersa y tonificada, corrigiendo las arrugas y previniendo su progresión.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Eliminación ojeras',
    description: `Este tratamiento es uno de los más efectivos cuando se trata de eliminar las bolsas en los ojos u ojeras. Debido a que la piel del contorno de los ojos es unas 10 veces más fina que la del rostro en general, a veces se ve más hundida y puede cambiar su tono de color a uno más oscuro. El ácido hialurónico para las ojeras permite recuperar el volumen perdido y rellena la ojera para igualar y proyectar la zona. Con las infiltraciones, la piel se ve más gruesa, con un tono mucho más unificado, disimulando así las ojeras y el aspecto del cansancio de la mirada.

    El ácido hialurónico tiene una propiedad que hidrata en profundidad la piel y eso ayuda a la producción del colágeno y la elastina.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Código de barras',
    description: `El blanching a novedosa técnica infalible para borra las arrugas superficiales. Consiste en realizar múltiples inyecciones muy superficiales de ácido hialurónico, dejando microdepósitos justo en la hendidura de la arruga, sin provocar abultamientos o exceso de producto, así logra atenuar las arrugas finas estáticas y mejorar sustancialmente las arrugas de expresión sin afectar la expresión y proporcionando además una hidratación visible en la piel.

    Se inyecta donde no hay músculo, sin afectar a la expresión facial potenciando la naturalidad. Se trata de un “relleno dérmico”, especialmente para los surcos naso genianos, las comisuras de los labios, las “patas de gallo”, las líneas finas de la frente y las arruguitas de la parte superior de los labios o “código de barras”.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '444444r4',
    title: 'Corrección nasogeniano',
    description: `El “surco nasogeniano” hace referencia a las arrugas que se producen desde el final de la nariz y que descienden oblicuamente hasta ambos límites de la boca. Este signo de expresión se hace notorio con la edad, ya que los tejidos de la piel se van relajando y se produce una pérdida de tonicidad muscular.

    Microinyecciones de ácido hialurónico prácticamente indoloras que se realizan en un corto espacio de tiempo. No requiere preparación previa aunque se puede colocar anestesia si la persona lo requiere. Aporta hidratación y volumen a la zona tratada, aspecto más luminoso de la piel. Corrección de surcos en la piel.
    
    Resultados naturales sin cirugía y de una forma rápida.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '55555t5',
    title: 'Hidratación y redensificación arrugas finas',
    description: `La redensificación es un nuevo tratamiento de rejuvenecimiento facial sin cirugía el cual se enfoca en la corrección y prevención del envejecimiento cutáneo que activa las funciones de las células de la piel proporcionando una mejora en textura, luminosidad, elasticidad e hidratación.

    Este tratamiento facial de rejuvenecimiento con ácido hialurónico y vitaminas rellena y corrige las arrugas respetando perfectamente la expresión del rostro.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '66666m6',
    title: 'Carboxiterapia',
    description: `Es un  método no quirúrgico el cual consiste en el uso terapéutico del gas dióxido de  Carbono (CO2) por vía subcutánea. El CO2 actúa en la zona afectada y se elimina  muy rápidamente. En la actualidad, la carboxiterapia es una muy buena terapia corporal. Rompe los acúmulos de fibrina del tejido graso, que son los que producen la temida piel de naranja, mejorando la circulación arterial  y mejorando el retorno linfántico y venoso, a la vez que elimina la toxinas de la zona que es tratada y la flacidez del tejido.

    También se utiliza en estética facial para eliminar las ojeras, líneas de expresión, estrías y acné entre otras aplicaciones. La forma en la que  se realiza la carboxiterapia es a través de un equipo especialmente preparado el  cual permite regular la velocidad del flujo, tiempo de inyección y monitorear el  porcentaje de dosis administrada.`,
    route: '/pages/aesthetic-medicine',
  },
];
export const FACIAL_HARMONIZATION_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Aumento de labios',
    description: `El perfilado de labios con ácido hialurónico busca conseguir un efecto de recuperación y rejuvenecimiento. Se trata de devolver a los labios su mejor aspecto, pero de forma no agresiva y sin alterar las facciones.

    El relleno de labios con ácido hialurónico persigue un objetivo diferente al perfilado. En esencia, lo que se busca es cambiar la forma del labio, dándole un aspecto más carnoso y pronunciado (incluso aunque no presente imperfecciones).`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: ' Rinomodelación',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Foxy eyes',
    description: `Consiste en la elevación de la cola de la ceja y el tratamiento de las arrugas de patas de gallo, consiguiendo así un ligero cambio en la forma del ojo (forma almendrada), lo que proyecta una mirada felina y sexy, tan popular hoy en día.

    Se trata de una intervención ambulatoria, realizada con anestesia local, con recuperación inmediata y resultados visibles desde el primer momento. Consiste en la inserción de un hilo tensor reabsorbible en los puntos estratégicos sobre la ceja. Estos hilos cuentan con espículas o conos (dependiendo del tipo del hilo) que se anclan en la piel y permiten desplazarla y fijar en el graso suficiente para conseguir el efecto deseado de mirada felina.
    
    La gran ventaja de los tratamientos con los materiales reabsorbibles es la mejora estética general en la zona tratada, ya que ayudan a la regeneración del colágeno propio del paciente y por ello aportan más calidad al estado general de la piel.
    
    El procedimiento no es doloroso y no requiere grandes cuidados posteriores. Por ello permite una recuperación laboral y social inmediata. La duración de los resultados varía del estado inicial de la piel y del tipo de hilos aplicados, de media suelen ser entre 12 y 18 meses.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '444444r4',
    title: 'Definición de pómulos',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '55555t5',
    title: 'Proyección del mentón',
    description: `Tratamiento sin necesidad de pasar por el quirófano que se realiza mediante la utilización de infiltraciones de ácido hialurónico de alta reticulación. Podemos obtener en una sola sesión resultados realmente sorprendentes. El procedimiento se puede realizar en la consulta, es prácticamente indoloro y permite incorporarse al día siguiente al trabajo y a la vida social. Al tratarse de ácido hialurónico se reabsorberá pasado un tiempo con una duración de los resultados de aproximadamente 18 meses.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '66666m6',
    title: 'Hilos tensores',
    description: `Conseguir un rostro rejuvenecido, reafirmado y con las arrugas disimuladas es posible gracias a los hilos tensores.

    Una técnica indolora, no invasiva y muy segura que se ha convertido en una auténtica revolución en el ámbito de la medicina estética. Los resultados son inmediatos y no es necesario pasar por el quirófano. Gracias a lifting facial sin cirugía con hilos se tensa la piel a través de la inserción de una serie de hilos en la dermis con una aguja. El tratamiento es 100 % individualizado para cada paciente. Por lo tanto, de manera previa se realiza un estudio de la piel, evaluando la edad, el grado de flacidez y la fisionomía de cada paciente`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '77777O7',
    title: 'Eliminación papada',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '88888R8',
    title: 'Feminización facial',
    description: `La Feminización Facial es el conjunto de procesos estéticos NO QUIRÚRGICOS que permiten definir, dar forma y/o volumen a aquellos rasgos faciales que están inexistentes en la mujer como por ejemplo: pómulos, nariz, labios, mentón, cuadrantes maxilofaciales, relleno de ojeras, etc.

    Este conjunto de procesos estéticos lleva el nombre comercial de Feminización Facial, y es elaborado por Médicos Estéticos Certificados con rellenos Biocompatibles y Reabsorbibles como lo es el Ácido Hialurónico, el cuál es completamente amigable con el organismo.`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '99999F9',
    title: ' Masculinización facial',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
