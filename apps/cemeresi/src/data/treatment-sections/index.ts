export interface AesteticMedicineTreatment {
  id: string;
  title: string;
  description: string;
  route: string;
}

export * from '../treatment-sections/aesthetic-surgery';
export * from '../treatment-sections/aesthetics-medicine';
