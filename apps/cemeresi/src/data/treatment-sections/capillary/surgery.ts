import { AesteticMedicineTreatment } from '..';

export const CAPILLARY__SURGERY_TECHNIQUES: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Técnica FUE (Follicular Unit Extraction)',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Técnica con IMPLANTER',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Técnica con ZAFIRO',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
export const CAPILLARY__SURGERY_PHASES: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Fase de recuperación',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: ' Bichectomia',
    description: `Fase de tratamiento quirúrgico`,
    route: '/pages/aesthetic-medicine',
  },
];
export const CAPILLARY__SURGERY_RECOMMENDATIONS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Recomendaciones antes de la cirugía',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Recomendaciones post cirugía',
    description: `Fase de tratamiento quirúrgico`,
    route: '/pages/aesthetic-medicine',
  },
];
