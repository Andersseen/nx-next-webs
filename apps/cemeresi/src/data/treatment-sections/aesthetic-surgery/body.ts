import { AesteticMedicineTreatment } from '..';

export const BODY_REMODELING_SURGERY_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Liposucción',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Lipoescultura',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Lipotransferencia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '555555t5',
    title: 'Abdominoplastia',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '6666666y6',
    title: 'Lipectomia de brazos/ muslos',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
export const BODY_MORE_SURGERY_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Retirada de biopolímeros',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Corrección de cicatrices',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
