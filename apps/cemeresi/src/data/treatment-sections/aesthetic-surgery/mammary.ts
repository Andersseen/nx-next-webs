import { AesteticMedicineTreatment } from '..';

export const MAMARIA_SURGERY_TREATMENTS: AesteticMedicineTreatment[] = [
  {
    id: '11111q1',
    title: 'Aumento de pecho',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '22222w2',
    title: 'Mastopexia con o sin prótesis',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '33333e3',
    title: 'Reducción de pecho',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '444444R4',
    title: 'Corrección de mamas tuberosas',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '5555555T5',
    title: 'Recambio de prótesis encapsuladas',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
  {
    id: '6666Y66',
    title: 'Recambio de prótesis mamarias',
    description: `Descripción`,
    route: '/pages/aesthetic-medicine',
  },
];
