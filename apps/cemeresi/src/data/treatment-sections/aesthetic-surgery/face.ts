import { AesteticMedicineTreatment } from '..';

export const FACIAL_REJUVENATION_SURGERY_TREATMENTS: AesteticMedicineTreatment[] =
  [
    {
      id: '11111q1',
      title: 'Blefaroplastia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '22222w2',
      title: 'Lifting facial',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '33333e3',
      title: 'Lifting cuello',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
  ];
export const FACIAL_HARMONIZATION_SURGERY_TREATMENTS: AesteticMedicineTreatment[] =
  [
    {
      id: '11111q1',
      title: 'Rinoplastia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '22222w2',
      title: ' Bichectomia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '33333e3',
      title: 'Liplift',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '444444r4',
      title: 'Lifting cejas',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '55555t5',
      title: 'Mentoplástia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '66666m6',
      title: 'Lobuloplástia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
    {
      id: '77777O7',
      title: 'Otoplástia',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
  ];
export const FACIAL_REMODELING_SURGERY_TREATMENTS: AesteticMedicineTreatment[] =
  [
    {
      id: '11111q1',
      title: ' Eliminación papada',
      description: `Descripción`,
      route: '/pages/aesthetic-medicine',
    },
  ];
