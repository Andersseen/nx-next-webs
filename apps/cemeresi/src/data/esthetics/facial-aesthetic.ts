export interface BodyTreatment {
  id: string;
  title: string;
  description: string;
  route: string;
}
export const FACIAL_TREATMENT: BodyTreatment[] = [
  {
    id: '11111q1',
    title: 'Limpieza facial con punta de diamante (microdermoabrasión)',
    description: `La limpieza facial profunda es un tratamiento que nos ayuda a eliminar las impurezas del rostro a causa de la contaminación, polvo o los residuos de maquillaje. 

      Gracias al tratamiento con Microdermoabrasión con punta de diamante, eliminamos las células muertas y los puntos negros de la piel,  mejorando así nuestra piel, revitalizándola y dándole un aspecto fresco, luminoso y que permite que la epidermis respire para regenerar las células que estimulan la producción de colágeno y elastina. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '22222w2',
    title: 'Radiofrecuencia facial ',
    description: `Es la utilización de ondas electromagnéticas en la piel del rostro que, a través de unos manípulos específicos, penetran desde las capas más superficiales hasta las más profundas de la epidermis. 

    El resultado es una piel más tersa y  firme, consiguiendo disimular pequeñas arrugas en la cara. También aporta luminosidad y ayuda a eliminar el aspecto de rosto cansado. 
    
    El número de sesiones es en función de cada paciente. Mínimo entre 8 y 10 sesiones. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '33333e3',
    title: 'INDIBA',
    description: `En la actualidad, se considera el mejor regenerador celular que existe. Estimula la producción de colágeno y elastina de la piel, reduce la flacidez, difumina las arrugas y las líneas de expresión, atenúa las bolsas de los ojos y redefine el óvalo facial. Se aplica con éxito contra la flacidez facial. 

    Beneficios: 
    
    - Elimina arrugas y reduce las líneas de expresión  
    
    - Tensa los músculos de la cara. 
    
    - Mejora el aspecto de los pómulos. 
    
    - Actúa como lifting de cuello y rostro. 
    
    - Disminuye la piel con flacidez. 
    
    - Da un mejor tono y color al cutis generando uniformidad. 
    
    - Quita cúmulos de grasa en la papada. 
    
    - También tiene otras indicaciones ya que sus efectos mejoran tanto el acné como las placas de psoriasis.  `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '444444r4',
    title: 'Máscara LED',
    description: `La tecnología LED se inventó a principios de los años 60, aunque no ha sido hasta estos últimos diez años cuando su uso se ha disparado con múltiples funciones, desarrollando distintos tipos y formatos. 

    En el caso de las máscaras LED, “se insertan en ellas pequeñas y múltiples lucecitas LED llamadas arrays”. Este tratamiento de LED facial, también conocido como fototerapia, funciona cuando “la luz penetra en la piel a través de su longitud de onda y actúa a nivel celular en las diferentes capas de la misma”, añade Mitsunaga. 
    
    Lo que busca este tratamiento LED es la biomodulación, un efecto que se usó por primera vez en la NASA para mantener en buenas condiciones la piel de los astronautas.  
    
    En vez de utilizar la luz a dosis elevadas como ocurre con el uso del láser, este tipo de tratamiento lo hace en dosis pequeñas. Al usar dosis muy pequeñas es necesario realizar sesiones repetidas, aplicándolo unas tres veces por semana durante varias semanas para conseguir efectos. A mayor longitud de onda, mayor es la penetración en la piel: cada longitud de onda tiene un color, los cuales a su vez tienen un efecto diferente en la piel. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '55555t5',
    title: 'Maderoterapia facial ',
    description: `La maderoterapia facial es una técnica de masaje natural y no invasiva que se lleva a cabo utilizando instrumentos de madera con el objetivo de reafirmar y tonificar el rostro para conseguir un efecto rejuvenecedor. 

    La maderoterapia actúa sobre la piel activando los fibroblastos que son los encargados de aumentar la producción de colágeno y elastina, combatiendo la flacidez y rejuveneciendo. 
    
    Beneficios:  
    
    - Reafirma, tonifica y moldea. 
    
    - Rejuvenece el rostro. 
    
    - Estimula y mejora la circulación sanguínea y linfática. 
    
    - Revitaliza y reactiva el sistema nervioso. 
    
    - Reactiva la producción de elastina, colágeno y vitamina E. 
    
    - Mejora el aspecto de la flacidez de la piel y del tejido en general. 
    
    - Mejora la tonicidad muscular. 
    
    -Equilibra los centros energéticos, mejora la respiración y relaja profundamente  `,
    route: '/pages/integral-esthetics',
  },
];
export const BODY_TREATMENT: BodyTreatment[] = [
  {
    id: '1q11111',
    title: 'Maderoterapia corporal ',
    description: `La maderoterapia corporal consiste en una serie de masajes combinando técnica manual con utensilios de madera, que ayuda a tonificar el cuerpo, tratar la grasa localizada, reducir la celulitis y estimular la producción de colágeno y elastina. 

      También favorece la circulación sanguínea y ayuda a la retención de líquidos. 
      
      BENEFICIOS: 
      
       - Combate la celulitis 
      
       - Elimina la grasa localizada 
      
       - Moldea el cuerpo 
      
       - Tonifica la piel 
      
       - Activa la circulación sanguínea (piernas más ligeras) 
      
       - Ayuda a la retención de líquidos 
      
       - Estimula la producción de colágeno y elastina 
      
      - Minimiza los niveles de estrés. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '2e22222',
    title: 'Masajes post-quirúrgicos (método colombiano) ',
    description: `Son un beneficio inminente para la mayoría de las recuperaciones luego de una liposucción o lipectomia, ya que estos promueven una rápida y efectiva cicatrización, mejorando la desinflamación y aún más la apariencia del paciente.  

    Estos masajes son aplicados de manera manual, logrando estimular el sistema linfático y ayudando a eliminar líquidos y toxinas del cuerpo. Son ampliamente recomendados por cirujanos plásticos, dado que sirven para moldear el área que busca ser mejorada. 
    
    Previene la aparición de fibrosis o alguna superficie irregular en el área tratada, dentro de sus beneficios ayuda a desinflamar, disminuye hematomas, evita la acumulación de líquidos llamados seromas y permite una rápida cicatrización. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '3x333333',
    title: 'Drenaje linfático ',
    description: `Consiste en un masaje suave y repetitivo, cuyo ritmo, más lento que el del masaje tradicional, sin la ayuda de productos favorecen la activación de la linfa y la eliminación de los líquidos estancados. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '4c444444',
    title: 'INDIBA',
    description: `La aplicación de la hipertermia del método Indiba por su efecto vasodilatador activa la circulación de la sangre en la zona, nutriendo y revitalizando las células de la dermis. LA ZONA TRATADA IRRADIA SALUD. 

    BENEFICIOS: 
    
    -  Su aplicación corporal permite eliminar los depósitos grasos que causan la celulitis y reafirmar los tejidos en brazos, pecho, abdomen y piernas. 
    
    -  Su efecto reafirmante además tiene aplicación destacadísima en senos y glúteos, arrugas, cicatrices, etc. 
    
    -  Remodelado de glúteos y vientre. 
    
    -  Puede utilizarse como complemento a una dieta de adelgazamiento. 
    
    - También está denominado como tratamiento anticelulítico. 
    
    -  Se aplica con éxito en tratamientos postquirúrgicos. 
    
    -  Eficaz en tratamientos post-liposucción. `,
    route: '/pages/integral-esthetics',
  },
  {
    id: '5z5555555',
    title: 'Radiofrecuencia corporal ',
    description: `La radiofrecuencia corporal es un tratamiento estético indoloro, no invasivo y sin efectos secundarios que ayuda a mejorar la piel de naranja, la celulitis y la flacidez resultantes de cambios de peso, edad o mala alimentación. 

    BENEFICIOS: 
    
     - Su principal función es combatir la flacidez de la piel 
    
     - Tratamiento no invasivo e indoloro 
    
     - Aplicación de ondas electromagnéticas sobre la piel 
    
     - Incrementa la producción de colágeno y elastina, por lo tanto rejuvenece y renueva el aspecto de la piel 
    
     - La piel se verá más tersa desde la primera sesión `,
    route: '/pages/integral-esthetics',
  },

  {
    id: '6v666666',
    title: 'Cavitación',
    description: `La cavitación es una técnica no quirúrgica para eliminar la grasa localizada mediante el uso de ultrasonidos de baja frecuencia, que se aplican sobre la zona donde se concentra la grasa para disolver las células adiposas desde su interior. Posteriormente la grasa se elimina con la orina o a través del sistema linfático. Esta técnica se utiliza para eliminar la piel de naranja y la piel de colchón devolviendo el aspecto normal a la piel de las zonas tratadas, al tiempo que mejora la circulación, se eliminan toxinas y aumenta el tono y la elasticidad de los tejidos. `,
    route: '/pages/integral-esthetics',
  },

  {
    id: '7u777777',
    title: 'Radiofrecuencia corporal',
    description: `Se trata de una tecnología que utiliza Energía Electromagnética Focalizada de Alta Intensidad (HIFEM, High Intensity Focused Electromagnetic) para generar contracciones máximas (20.000 sentadillas o abdominales por sesión) para definir contorno y generar tonificación. 

     - Mejora la musculatura, creando miofibrillas. 
    
     - Moldea la figura, gracias a la ruptura de la grasa (cascada apoptótica) 
    
     - Levanta los glúteos 
    
     - Tonifica el abdomen 
    
    Es un tratamiento indoloro que te permite volver a tu rutina sin necesidad de hacer reposo. Además los resultados son visibles y duraderos a partir de la 4a sesión de tratamiento pudiendo realizarte 2 sesiones en una semana para acortar los tiempos del tratamiento. `,
    route: '/pages/integral-esthetics',
  },

  {
    id: '8o88888888',
    title: 'Gimnasia pasiva',
    description: `La gimnasia pasiva es una técnica utilizada en medicina que funciona a través de la electro estimulación. Esto quiere decir que genera impulsos eléctricos, mediante electrodos conductivos, que va directamente a la musculatura. De esta forma, se copia la señal que manda el cerebro para que se muevan los músculos. En este sentido, el estímulo se transforma en contracción, fortalece el tejido y elimina los líquidos retenidos en él. `,
    route: '/pages/integral-esthetics',
  },

  {
    id: '9p9999999',
    title: 'Presoterapia',
    description: `La presoterapia consiste en aplicar presiones de aire sobre diferentes partes del cuerpo para favorecer el drenaje linfático. Esta técnica promueve, además, la oxigenación de los tejidos y recupera la elasticidad cutánea facilitando la regeneración tisular. 

    Los equipos de presoterapia constan de un par de botas, fajín y brazos con diferentes cámaras de aire que permiten el llenado y vaciado de los diferentes compartimentos y se consigue un efecto llamado oleaje. Esto, logra un vaciado de los vasos linfáticos mucho más eficaz que cualquier masaje manual. 
    
    De esta forma estimulamos el sistema circulatorio contribuyendo así a la eliminación de líquidos, grasas y toxinas que generan la celulitis, edemas, linfoedemas y trastornos venosos como las varices. `,
    route: '/pages/integral-esthetics',
  },
];
