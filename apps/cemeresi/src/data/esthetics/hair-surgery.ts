import { BodyTreatment } from './facial-aesthetic';

export const HAIR_SURGERY: BodyTreatment[] = [
  {
    id: '11111q1',
    title: 'TÉCNICA FUE (FOLLICULAR UNIT EXTRACTION)',
    description: `Es la técnica de trasplante de pelo más avanzada en la actualidad.

    Su principal ventaja es que no deja cicatriz y no son necesario puntos quirúrgicos.
    
    Consiste en la obtención de las unidades foliculares una a una de la zona donante (zona posterior y lateral de la cabeza) para ser implantadas posteriormente en las zonas afectadas por la alopecia.
    
    Esta técnica se puede realizar mediante un rasurado de la zona donante de manera total o parcial.
    
    Se realiza a través de un bisturí circular rotatorio que aíslan el folículo del cuero cabelludo, posteriormente se realiza la extracción de los foliculos con la ayuda de unas pinzas especiales.
    
    Se realiza el proceso muy minuciosamente para evitar daño en el folículo a extraer.
    
    Durante todo el proceso las unidades foliculares se clasifican y se mantienen en conservación para garantizar la viabilidad de los mismos.
    
    Aunque sea una cirugía menor siempre es necesario un protocolo para garantizar que el proceso sea el más adecuado y satisfactorio para el paciente.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '22222w2',
    title: 'TÉCNICA CON IMPLANTER',
    description: `Con esta técnica en lugar de realizar la implantación con incisiones, esta es a través de un Implanter que es un instrumento quirúrgico específico para este tipo de cirugías.

    Este método no es una técnica en si sino una forma de inserción del folículo piloso con un tipo especial de aplicador quirúrgico.
    
     Las características más importantes de este método son:
    
    1.- El tratamiento del folículo es más delicado por lo que la probabilidad de la supervivencia del mismo es mayor.
    
    2.- Esta técnica usa un instrumento quirúrgico específico de diferentes medidas que ayudan a controlar la posición y profundidad del folículo implantado.
    
    3.- Por otro lado es posible conseguir una mayor densidad.
    
    4.- El diámetro de incisión es menor que con las incisiones.
    
    5.- El tiempo de cicatrización es menor.
    
    En otros métodos de implante de cabello, en primer lugar se realiza la apertura de canales en un primer tiempo y posteriormente se implantan las raíces por lo que se puede considerar que se realiza en dos tiempos o etapas. En el caso de un trasplante con la técnica de Implanter, el canal y el implante se puede realizar en un solo tiempo.
    
    Ciertamente el método con Implanter requiere una experiencia y destreza adicional que con las demás prácticas, por lo que es necesario acudir a un centro especializado.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '33333e3',
    title: 'TÉCNICA DE ZAFIRO',
    description: `Con esta técnica se realizan las incisiones para un posterior implante del folículo con unas hojas especiales fabricadas de piedra preciosa de zafiro en lugar de realizar las incisiones con una hoja de acero quirúrgico.

    Esta técnica en si es una forma específica de apertura de canales en el cuero cabelludo que nos aporta una serie de ventajas con respecto a la hoja quirúrgica.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '444444r4',
    title: 'DISEÑO Y PLANIFICACIÓN',
    description: `El diseño de la línea frontal es uno de los puntos en la planificación de una intervención capilar más importante para que el trasplante capilar tenga un aspecto natural.

    Las fotos del paciente tomadas antes de la pérdida del cabello pueden ser muy útiles para volver a diseñar su línea frontal como la tenía anteriormente.
    
    Datos a considerar en el diseño de la línea frontal:
    
    - La piel de la línea frontal no debe tener movimiento por la musculatura del musculo frontal 
    
    - Hay una transición entre la piel donde crece el pelo y la piel de la frente en relación al grosor y tonalidad.
    
    - La línea de implantación frontal se puede diseñar de varias formas siempre de acuerdo a la solicitud del paciente y el criterio médico.
    
    - Para que un implante sea natural la primera línea del cabello debe tener una densidad alta reservándose las unidades foliculares de un cabello para que el resultado sea natural.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '55555t5',
    title: 'FASES DE RECUPERACIÓN',
    description: `Para ver el resultado final de la intervención hay que esperar al menos 1 año antes de alcanzar los resultados deseados.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '666666y6',
    title: 'FASES DEL TRATAMIENTO QUIRÚRGICO',
    description: `La cirugía aunque sea menor necesita de un protocolo para garantizar que el proceso sea el más adecuado.`,
    route: '/pages/hair-surgery',
  },
  {
    id: '7777777n7',
    title: 'RECOMENDACIONES ANTES DE LA CIRUGÍA',
    description: `RECOMENDACIONES ANTES DE LA CIRUGÍA
    Las siguientes indicaciones, son muy importantes antes de la cirugía capilar, para garantizar la mejor calidad de la intervención:
    
    Es necesario analíticas que reflejen datos como “hemograma, glicemia, ionograma, coagulograma, VIH y Hepatitis C”. Se requerirá también un Electrocardiograma con no más de 2 semanas antes de la intervención si el medico lo requiere.
    Si utiliza Minoxidil deberá suspenderlo 2 semanas antes de la cirugía, si utiliza Finasteride siga tomando la dosis habitual.
    Las aspirinas o anti-inflamatorios no se deben tomar de 7 a 14 días antes de su cirugía. Informe al médico si está utilizando algún anticoagulante por cualquier causa antes del trasplante.
    No tome ningún complejo vitamínico, que incluya vitaminas E y B, 7 días antes ya que aumenta el sangrado. 
    No ingiera alcohol al menos 3 días antes del trasplante y evite bebidas con cafeína (Ej. Té y Café), debido a que influyen negativamente en el sangrado y la sensibilidad a los fármacos utilizados en el trasplante.
    Puede continuar con su medicación habitual de hipertensión arterial, hipoglucemiantes, protector gástrico, etc.
    El día anterior a la cirugía se recomienda que ingiera agua en abundancia.
    No es recomendable fumar 24/48 h antes a la intervención quirúrgica, debido a que se retrasa la cicatrización y aumenta la probabilidad de infección, por lo que se puede ver comprometida la supervivencia de los microinjertos.
    La misma mañana de la intervención dúchese y lave su cabeza con champú normal asegurándote de quitar cualquier resto de producto que esté utilizando para su cabello (gomina, laca, etc…).
    Es importante que no se corte el pelo antes de la intervención ya que es de gran ayuda al médico conocer las características del cabello propias de cada paciente.
    Desayune con normalidad la mañana del trasplante, evitando bebidas con cafeína (Ej. Café, Té, Bebidas Energéticas, etc…)
    Le recomendamos que no conduzca de regreso a casa ya la medicación administrada durante la cirugía puede causarle sueño, por lo que se recomienda que le recojan en la clínica.
    No es recomendable realizar ningún deporte o esfuerzo físico, 3 días antes de  la cirugía.`,
    route: '/pages/hair-surgery',
  },
];
