import { NavigationLink } from '@nx-next/shared';
export const LINKS: NavigationLink[] = [
  { label: 'MEDICINA ESTÉTICA', route: '/pages/aesthetic-medicine' },
  { label: 'CIRUGÍA CAPILAR', route: '/pages/hair-surgery' },
  { label: 'MEDICINA REGENERATIVA', route: '/pages/regenerative-medicine' },
  { label: 'ESTÉTICA INTEGRAL', route: '/pages/integral-esthetics' },
  { label: 'FISIOTERAPIA AVANZADA', route: '/pages/advanced-physiotherapy' },
  { label: 'PSICOLOGÍA', route: '/pages/psychology' },
];

export const menuItems = [
  {
    id: '11111111',
    name: 'MEDICINA ESTÉTICA',
    route: '',
    links: [
      {
        name: 'FACIAL',
        route: '/pages/aesthetic-medicine',
        id: '1111111qc',
        children: [
          {
            id: 1,
            name: 'CALIDAD DE PIEL',
            route: '/pages/aesthetic-medicine/face/quality-skin',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'REJUVENECIMIENTO',
            route: '/pages/aesthetic-medicine/face/rejuvenetion',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'ARMONIZACIÓN',
            route: '/pages/aesthetic-medicine/face/harmonization',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'CORPORAL',
        route: '/pages/aesthetic-medicine',
        id: '22222e2',
        children: [
          {
            id: 1,
            name: 'CALIDAD DE PIEL',
            route: '/pages/aesthetic-medicine/body/quality-skin',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'REMODELACIÓN',
            route: '/pages/aesthetic-medicine/body/remodeling',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'FLACIDEZ',
            route: '/pages/aesthetic-medicine/body/flacidity',
            cName: 'submenu-item',
          },
        ],
      },
    ],
  },
  {
    id: '222222',
    name: 'CIRUGÍA ESTÉTICA',
    route: '',
    links: [
      {
        name: 'FACIAL',
        route: '/pages/aesthetic-surgery/face',
        id: '1111111i1',
        children: [
          {
            id: 1,
            name: 'REJUVENECIMIENTO',
            route: '/pages/aesthetic-surgery/face/harmonization',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'ARMONIZACIÓN',
            route: '/pages/aesthetic-surgery/face/rejuvenetion',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'REMODELACIÓN',
            route: '/pages/aesthetic-surgery/face/remodeling',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'MAMARIA',
        route: '/pages/aesthetic-surgery/mammary',
        id: '22222u2',
      },
      {
        name: 'CORPORAL',
        route: '/pages/aesthetic-surgery/body',
        id: '333333m4',
        children: [
          {
            id: 1,
            name: 'REMODELACIÓN',
            route: '/pages/aesthetic-surgery/body/remodeling',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'MÁS',
            route: '/pages/aesthetic-surgery/body/more',
            cName: 'submenu-item',
          },
        ],
      },
    ],
  },
  {
    id: '333333',
    name: 'CAPILAR',
    route: '',
    links: [
      {
        name: 'CIRUGÍA',
        route: '/pages/hair-surgery/surgery',
        id: '111111o1',
        children: [
          {
            id: 1,
            name: 'TÉCNICAS',
            route: '/pages/hair-surgery/surgery',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'FASES',
            route: '/pages/hair-surgery/surgery',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'DISEÑO Y PLANIFICACIÓN',
            route: '/pages/hair-surgery/surgery',
            cName: 'submenu-item',
          },
          {
            id: 4,
            name: 'RECOMENDAIONES',
            route: '/pages/hair-surgery/surgery',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'TRATAMIENTOS',
        route: '/pages/hair-surgery/treatments',
        id: '222m2',
        children: [
          {
            id: 1,
            name: 'MESOTERAPIA',
            route: '/pages/hair-surgery/treatments',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'BIOESTIMULACIÓN',
            route: '/pages/hair-surgery/treatments',
            cName: 'submenu-item',
          },
        ],
      },
    ],
  },
  {
    id: '4444444',
    name: 'ESTÉTICA INTEGRAL',
    route: '',
    links: [
      {
        name: 'FACIAL',
        route: '/pages/integral-esthetics/face',
        treatment: 'facial',
        id: '1111z1',
        children: [
          {
            id: 1,
            name: 'Limpieza facial con Punta de Diamante',
            route: '/pages/integral-esthetics/face',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'INDIBA',
            route: '/pages/integral-esthetics/face',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'Radiofrecuencia facial',
            route: '/pages/integral-esthetics/face',
            cName: 'submenu-item',
          },
          {
            id: 4,
            name: 'Máscara LED',
            route: '/pages/integral-esthetics/face',
            cName: 'submenu-item',
          },
          {
            id: 5,
            name: 'Maderoterapia facial',
            route: '/pages/integral-esthetics/face',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'CORPORAL',
        route: '/pages/integral-esthetics/body',
        treatment: 'body',
        id: '2222x2',
        children: [
          {
            id: 1,
            name: 'Maderoterapia corporal',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'Masaje post-quirúrgico (método colombiano)',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'Drenaje linfático',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 4,
            name: 'INDIBA',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 5,
            name: 'Radiofrecuencia corporal',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 6,
            name: 'Cavitación',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 7,
            name: 'Body Sculpt',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 8,
            name: 'Gimnasia pasiva',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
          {
            id: 9,
            name: 'Presoterapia',
            route: '/pages/integral-esthetics/body',
            cName: 'submenu-item',
          },
        ],
      },
    ],
  },
  {
    id: '555555',
    name: 'FISIOTERAPIA',
    route: '',
    links: [
      {
        name: 'TRATAMIENTOS',
        route: '/pages/advanced-physiotherapy',
        id: '1111111o1',
        children: [
          {
            id: 1,
            name: 'Masaje descontracturante',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'Ondas de choque',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'Punción seca',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 4,
            name: 'Magnetoterapia',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 5,
            name: 'Ejercicios terapéuticos',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'TERCERA EDAD',
        route: '/pages/advanced-physiotherapy',
        id: '22e2',
        children: [
          {
            id: 1,
            name: 'Tratamiento específico para personas de tercera edad',
            route: '/pages/advanced-physiotherapy',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'PATOLOGÍAS',
        route: '/pages/physiotherapy',
        id: '333333y3',
        children: [
          {
            id: 1,
            name: 'Dolores de espalda',
            route: '/pages/physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 2,
            name: 'Tendinitis',
            route: '/pages/physiotherapy',
            cName: 'submenu-item',
          },
          {
            id: 3,
            name: 'Esguinces',
            route: '/pages/physiotherapy',
            cName: 'submenu-item',
          },
        ],
      },
      {
        name: 'PILATES TERRAPÉUTICOS',
        route: '/pages/physiotherapy',
        id: '44444i4',
        children: [
          {
            id: 1,
            name: 'Pilates suelo con implementos',
            route: '/qaindex',
            cName: 'submenu-item',
          },
        ],
      },
    ],
  },
  {
    id: '6666666',
    name: 'PSICOLOGÍA',
    route: '/pages/physiotherapy',
    links: [
      {
        name: 'ADULTOS',
        route: '/pages/psychology',
        id: '11111111p1',
      },
      { name: 'NIÑOS', route: '/pages/psychology', id: '222222r2' },
      {
        name: 'ONLINE',
        route: '/pages/psychology',
        id: '333333333b3',
      },
    ],
  },
  // {
  //   id: '777777',
  //   name: 'ejemplo',
  //   route: '',
  //   links: [],
  // },
];
