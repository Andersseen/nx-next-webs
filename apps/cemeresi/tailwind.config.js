const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');
const { nextui } = require('@nextui-org/react');
const withMT = require('@material-tailwind/react/utils/withMT');

/** @type {import('tailwindcss').Config} */
module.exports = withMT({
  content: [
    '../../node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
    '../../node_modules/@material-tailwind/react/components/**/*.{js,ts,jsx,tsx}',
    '../../node_modules/@material-tailwind/react/theme/components/**/*.{js,ts,jsx,tsx}',
    join(
      __dirname,
      '{src,pages,components,app}/**/*!(*.stories|*.spec).{ts,tsx,html}'
    ),
    ...createGlobPatternsForDependencies(__dirname),
  ],

  theme: {
    extend: {
      colors: {
        main: {
          50: '#f8f7f8',
          100: '#f3f0f3',
          200: '#e7e3e7',
          300: '#d5ccd5',
          400: '#baacb9',
          500: '#a897a7',
          600: '#8b7789',
          700: '#746271',
          800: '#61535f',
          900: '#534851',
          950: '#30272f',
        },
      },
    },
  },

  darkMode: 'class',
  plugins: [
    nextui({
      themes: {
        light: {
          layout: {}, // light theme layout tokens
          colors: {
            background: '#f3f0f3',
            foreground: '#30272f',
            secondary: {
              DEFAULT: '#8b7789',
              foreground: '#534851',
            },
          },
        },
        dark: {
          layout: {}, // dark theme layout tokens
          colors: {
            background: '#30272f',
            foreground: '#f3f0f3',
            secondary: {
              DEFAULT: '#8b7789',
              foreground: '#f3f0f3',
            },
          },
        },
      },
    }),
  ],
});
