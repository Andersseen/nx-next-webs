const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');
const { nextui } = require('@nextui-org/react');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    '../../node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
    join(
      __dirname,
      '{src,pages,components,app}/**/*!(*.stories|*.spec).{ts,tsx,html}'
    ),
    ...createGlobPatternsForDependencies(__dirname),
  ],
  theme: {
    extend: {
      colors: {
        main: {
          50: '#f0f0f0',
          100: '#e3e3e3',
          200: '#cccccc',
          300: '#ababab',
          400: '#828282',
          500: '#696969',
          600: '#575757',
          700: '#4a4a4a',
          800: '#404040',
          900: '#383838',
          950: '#212121',
        },
      },
    },
  },

  darkMode: 'class',
  plugins: [
    nextui({
      themes: {
        light: {
          layout: {}, // light theme layout tokens
          colors: {
            background: '#e3e3e3',
            foreground: '#212121',
            secondary: {
              DEFAULT: '#828282',
              foreground: '#212121',
            },
          },
        },
        dark: {
          layout: {}, // dark theme layout tokens
          colors: {
            background: '#212121',
            foreground: '#f6f6f6',
            secondary: {
              DEFAULT: '#575757',
              foreground: '#f6f6f6',
            },
          },
        },
      },
    }),
  ],
};
