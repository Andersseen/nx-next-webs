type Treatment = {
  name: string;
  price: string;
};

type TreatmentSection = {
  id: number;
  treatments: Treatment[];
  name: string;
  doubleRow?: boolean;
};

export const TREATMENT_PRICES: TreatmentSection[] = [
  {
    id: 1,
    name: 'TRATAMIENTOS FACIALES',
    treatments: [
      { name: 'Higiene facial', price: '47 €' },
      { name: 'Microexfoliación', price: '50 €' },
      { name: 'O2 (oxigenoterapia)', price: '80 €' },
      { name: 'Multivitaminas (MESOBOOST)', price: '85 €' },
      { name: 'Peeling Carbón Hollywood', price: '85 €' },
      { name: 'Eliminación hiperpigmentaciones:', price: '' },
      { name: ' · · ·  S/S', price: '60 €' },
      { name: ' · · ·  Bono de 2', price: '100 €' },
      { name: ' · · ·  Bono de 3', price: '150 €' },
      { name: 'Coolifting (carboxiterapia)', price: '80 €' },
      { name: 'Radiofrecuencia facial', price: '75 €' },
    ],
  },
  {
    id: 2,
    name: 'TRATAMIENTOS CORPORALES',
    treatments: [
      { name: 'Radiofrecuencia corporal', price: 'Bonos' },
      { name: 'Maderoterapia', price: 'Bonos' },
      { name: 'Drenaje linfático manual', price: 'Bonos' },

      {
        name: 'Microvibracion compresiva (Roll-active-duo)',
        price: 'Bonos',
      },

      {
        name: 'Ondas electromagnéticas (tesla)',
        price: 'Bonos',
      },
      { name: 'Presoterapia S/S', price: '40 €' },
      { name: 'Masaje manual :', price: '' },
      { name: ' · · ·  1/2 h', price: '30 €' },
      { name: ' · · ·  45 m', price: '55 €' },
      { name: 'Higiene de espalda', price: '50 €' },
      { name: 'Exfoliación corporal', price: '40 €' },
    ],
  },
  {
    id: 3,
    name: 'BRONCEADO',
    treatments: [
      { name: 'Rayos UVA :', price: '' },
      { name: " · · ·  Bonos 140'", price: '48 €' },
      { name: " · · ·  Bonos 60'", price: '33 €' },
      { name: " · · ·  S/S 10'", price: '10 €' },
      { name: " · · ·  S/S 5'", price: '8 €' },
      { name: 'Bronceado caña de azúcar :', price: '' },
      { name: ' · · ·  Cuerpo entero más exfoliación', price: '35 €' },
      { name: ' · · ·  Cuerpo entero', price: '25 €' },
      { name: ' · · ·  Facial', price: '10 €' },
    ],
  },
  {
    id: 4,
    treatments: [
      { name: 'Tinte de cejas y pestañas', price: '18 €' },
      { name: 'Tinte de cejas o pestañas', price: '12 €' },
      { name: 'Maquillaje', price: 'Desde 45€ (consultar)' },
      { name: 'Henna cejas', price: '20 €' },
      { name: 'Henna + diseño', price: '25 €' },
      { name: 'Permanente y tinte de pestañas', price: '45 €' },
      { name: 'Laminado de cejas', price: '20 €' },
      { name: 'Laminado de cejas + tinte', price: '25 €' },
      { name: 'Laminado + tinte + diseño', price: '35 €' },
      { name: 'Microblanding', price: 'Consultar' },
      { name: 'Microshading', price: 'Consultar' },
      { name: 'Micropigmentacion', price: 'Consultar' },
    ],
    name: 'OTROS SERVICIOS',
    doubleRow: true,
  },
  {
    id: 5,
    treatments: [
      {
        name: 'Solo esmaltado de uñas manos/pies con esmaltado normal',
        price: '8 €',
      },
      { name: 'Solo esmalte permanente uñas manos/pies', price: '12 €' },
      { name: 'Manicura exprés con esmaltado normal', price: '15 €' },
      { name: 'Manicura con hidratación y esmaltado normal', price: '18 €' },
      { name: 'Manicura exprés + esmaltado permanente', price: '23 €' },
      {
        name: 'Manicura con hidratación + esmaltado permanente	',
        price: '29 €',
      },
      { name: 'Pedicura exprés y esmaltado normal', price: '20 €' },
      { name: 'Pedicura con hidratación y esmaltado normal', price: '33 €' },
      { name: 'Pedicura exprés + esmaltado permanente', price: '28 €' },
      {
        name: 'Pedicura con hidratación y esmaltado permanente	',
        price: '36 €',
      },
      {
        name: 'Retirado de esmaltado permanente manos/pies suplemento',
        price: '3 / 8 €',
      },
      { name: 'Uñas gel (primera puesta) color o francesa', price: '65 €' },
      { name: 'Decoraciones en uñas', price: 'Desde 5 €' },
    ],
    name: 'PEDICURA/MANICURA',
  },
  {
    id: 6,
    name: 'DEPILACIÓN FEMENINA',
    treatments: [
      { name: 'Piernas enteras sin ingles', price: '22 €' },
      { name: 'Piernas enteras con ingles', price: '26 €' },
      { name: 'Piernas enteras con ingles con medio pubis', price: '28 €' },
      { name: 'Piernas enteras con ingles brasileñas', price: '33 €' },
      { name: 'Piernas enteras con pubis completo', price: '40 €' },
      { name: 'Medias piernas', price: '15 €' },
      { name: 'Muslos', price: '16 €' },
      { name: 'Brazos / Medios brazos', price: '17/14 €' },
      { name: 'Ingles, axilas o glúteos', price: '10 €' },
      { name: 'Ingles con medio pubis', price: '12 €' },
      { name: 'Ingles brasileñas', price: '18 €' },
      { name: 'Pubis completo', price: '22 €' },
      { name: 'Labio, ceja o mentón / con hilo', price: '7 / 10 €' },
      { name: 'Depilación Láser Diodo Lightsheer', price: 'Consultar' },
      { name: "Depilación eléctrica 5'", price: '10 € (consultar)' },
    ],
  },
  {
    id: 7,
    name: 'DEPILACIÓN MASCULINA',
    treatments: [
      { name: 'Espalda', price: '22 €' },
      { name: 'Hombros', price: '12 €' },
      { name: 'Cuello ', price: '12 €' },
      { name: 'Pecho', price: '22 €' },
      { name: 'Pecho o espalda con hombros ', price: '33 €' },
    ],

    doubleRow: true,
  },
];
