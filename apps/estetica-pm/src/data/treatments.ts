export type TreatmentService = {
  id: number;
  name: string;
  img: string;
  route: string;
};

export const TREATMENTS_SERVICES: TreatmentService[] = [
  {
    id: 1,
    name: 'Tratamientos Faciales',
    img: '/1.webp',
    route: '/pages/services/facial',
  },
  {
    id: 2,
    name: 'Tratamientos Corporales',
    img: '/2.webp',
    route: '/pages/services/body',
  },
  {
    id: 3,
    name: 'Depilación Laser',
    img: '/3.webp',
    route: '/pages/services/laser',
  },
  {
    id: 4,
    name: 'Bronceado',
    img: '/7.webp',
    route: '/pages/services/bronceado',
  },
  {
    id: 5,
    name: 'Pedicura - Manicura',
    img: '/4.webp',
    route: '/pages/services/manicura',
  },
  {
    id: 6,
    name: 'Otros Servicios',
    img: '/5.webp',
    route: '/pages/services/other',
  },
];
