import { NavigationLink } from '@nx-next/shared';
export const LINKS: NavigationLink[] = [
  { label: 'Inicio', route: '/' },
  { label: 'Nuestro centro', route: '/pages/about' },
  { label: 'Tratamientos y servicios', route: '/pages/treatment' },
  { label: 'Precios', route: '/pages/price' },
  { label: 'Contacto', route: '/pages/contact' },
];

export const menuItems = [
  {
    id: '11111111',
    name: 'Inicio',
    route: '/',
    links: [],
  },
  {
    id: '222222',
    name: 'Nuestro centro',
    route: '/pages/about',
    links: [],
  },
  {
    id: '333333',
    name: 'Tratamientos y servicios',
    route: '',
    links: [
      {
        id: '1111q11',
        name: 'Tratamientos Faciales',

        route: '/pages/services/facial',
      },
      {
        id: '222m2',
        name: 'Tratamientos Corporales',

        route: '/pages/services/body',
      },
      {
        id: '33333r3',
        name: 'Depilación Laser',

        route: '/pages/services/laser',
      },
      {
        id: '4444q44',
        name: 'Bronceado',

        route: '/pages/services/bronceado',
      },
      {
        id: '5555t5',
        name: 'Pedicura - Manicura',

        route: '/pages/services/manicura',
      },
      {
        id: '6q66q66',
        name: 'Otros Servicios',

        route: '/pages/services/other',
      },
    ],
  },
  {
    id: '4444444',
    name: 'Precios',
    route: '/pages/price',
    links: [],
  },
  {
    id: '555555',
    name: 'Contacto',
    route: '/pages/contact',
    links: [],
  },
];
