import {
  IconFacebook,
  IconInstagram,
  IconWhatsapp,
  StickyIconsData,
} from '@nx-next/shared';

export const StickyIconsContent: StickyIconsData[] = [
  {
    title: 'Instagram',
    route: 'https://www.instagram.com/esteticapalomamolero',
    icon: <IconInstagram />,
  },
  {
    title: 'Facebook',
    route: 'https://www.facebook.com/estetica.palomamolero',
    icon: <IconFacebook />,
  },
  {
    title: 'Whatsapp',
    route: 'https://wa.me/+34696797483',
    icon: <IconWhatsapp />,
  },
];
