import './global.css';
import { LINKS } from '../data/links';
import { StickyIcons } from '@nx-next/shared';
import { ThemeSwitcher } from '@nx-next/shared';
import { FooterWrapper } from '../components/footer-section';
import Providers from './providers';
import { Logo } from '../components/logo';
import { StickyIconsContent } from '@estetica-pm/data/sticky-iconts';
import HeaderNavigation from '@estetica-pm/components/header-navigation';

export const metadata = {
  title: 'Estética Paloma Molero',
  description: 'Bienvenido a nuestro centro',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <body className="relative">
        <Providers>
          <HeaderNavigation logo={<Logo />} themeSwitcher={<ThemeSwitcher />} />
          <StickyIcons stickyIconsData={StickyIconsContent} />
          <main
            className="z-10 relative h-min-screen"
            style={{
              background: `var(--main-color)`,
            }}
          >
            {children}
          </main>
          <FooterWrapper navLinks={LINKS} />
        </Providers>
      </body>
    </html>
  );
}
