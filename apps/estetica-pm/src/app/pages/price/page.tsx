import { TreatmentPrices } from '@estetica-pm/components/card-list';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  return (
    <PageWrapper>
      <section className="w-full min-h-screen shadow-lg p-2 md:p-8">
        <TreatmentPrices />
      </section>
    </PageWrapper>
  );
}
