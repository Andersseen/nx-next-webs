import DepilationLaserPage from '@estetica-pm/components/pages/depilation';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  // const treatmnts: Treatment[] = [
  //   {
  //     id: '1111111e2',
  //     name: 'PIERNAS ENTERAS SIN INGLES',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e3',
  //     name: 'PIERNAS ENTERAS CON INGLES',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '1111111e4',
  //     name: ' PIERNAS ENTERAS CON INGLES CON MEDIO PUBIS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e5',
  //     name: 'PIERNAS ENTERAS CON INGLES BRASILEÑAS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e6',
  //     name: 'PIERNAS ENTERAS CON PUBIS COMPLETO',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e7',
  //     name: 'MEDIAS PIERNAS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e8',
  //     name: 'MUSLOS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e9',
  //     name: 'INGLES, AXILAS O GLÚTEOS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e10',
  //     name: 'INGLES CON MEDIO PUBIS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e711',
  //     name: 'INGLES BRASILEÑAS',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e412',
  //     name: 'PUBIS COMPLETO',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e513',
  //     name: 'DEPILACIÓN LÁSER DIODO LIGHTSHEER',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e614',
  //     name: 'DEPILACIÓN ELÉCTRICA',
  //     img: '/3.webp',
  //     route: '/accountant',
  //   },
  // ];

  // const data: TreatmentSection = {
  //   title: 'DEPILACIÓN : LÁSER DIODO LIGHSHEER TRADICIONAL A LA CERA ELÉCTRICA',
  //   description: [
  //     'LÁSER DIODO LIGHSEER',
  //     'Elimina el vello de una forma permanente para que muestres una piel sedosa y bonita durante todo el año.',
  //     'Un tratamiento sin ningún tipo de riesgo para tu piel y salud si se toman las precauciones adecuadas.',
  //     'Una forma segura y eficaz para eliminar el vello de forma permanente.',
  //     'TRADICIONAL A LA CERA',
  //     'La cera es uno de los métodos de depilación más utilizado por su larga duración (entre 2-8 semanas) y rapidez de aplicación.',
  //     'La depilación con cera más efectiva es la que se realiza cuando el vello tiene unos 5 mm de longitud.',
  //     'El resultado es una piel suave y en muchos casos un crecimiento del vello cada vez mas débil y menos denso.',
  //     'ELÉCTRICA',
  //     'Sistema Apilus',
  //   ],
  //   button: {
  //     buttonName: 'Volver',
  //     routeBack: '/pages/treatment',
  //   },
  // };
  return (
    <PageWrapper>
      <DepilationLaserPage />
    </PageWrapper>
  );
}
