import FacialPage from '@estetica-pm/components/pages/facial';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  // const treatmnts: Treatment[] = [
  //   {
  //     id: '1111111e2',
  //     name: 'HIGIENE FACIAL',
  //     img: '/1.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e3',
  //     name: 'COOLIFTING( CARBOXITERAPIA)',
  //     img: '/1.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '1111111e4',
  //     name: 'MICROEXFOLIACIÓN',
  //     img: '/1.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e5',
  //     name: 'O2 (oxigenoterapia)',
  //     img: '/1.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e6',
  //     name: 'RADIOFRECUENCIA FACIAL',
  //     img: '/1.webp',
  //     route: '/accountant',
  //   },
  // ];

  // const data: TreatmentSection = {
  //   title: 'TRATAMIENTOS FACIALES',
  //   description: [
  //     'La piel es el principal órgano de belleza, revela nuestra edad, nuestra salud y hasta nuestro estado de ánimo.',
  //     'A ella se lo debemos todo. Nos protege de agentes nocivos, regula el intercambio molecular de oxígeno, agua, minerales… Por todo ello hay que saber compensarla y mantenerla con los tratamientos más adecuados.',
  //     'Nuestra filosofía se basa en un seguimiento personalizado, calidad y duración en nuestros tratamientos.',
  //     'Con el paso del tiempo es inevitable que se formen líneas y arrugas porque la piel pierde flexibilidad y elasticidad. Para impedir este deterioro disponemos de una amplia carta de tratamientos personalizados que previenen y combaten el envejecimiento de nuestra piel.',
  //   ],
  //   button: {
  //     buttonName: 'Volver',
  //     routeBack: '/pages/treatment',
  //   },
  // };
  return (
    <PageWrapper>
      <FacialPage />
    </PageWrapper>
  );
}
