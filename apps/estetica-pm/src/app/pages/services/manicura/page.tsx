import ManicuraPage from '@estetica-pm/components/pages/manicura';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  // const treatmnts: Treatment[] = [
  //   {
  //     id: '1111111e2',
  //     name: 'Solo esmaltado de uñas manos/pies con esmaltado norma',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e3',
  //     name: 'Solo esmalte permanente uñas manos/pies',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '1111111e4',
  //     name: 'Manicura exprés',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e5',
  //     name: 'Manicura con hidratación',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e6',
  //     name: 'Manicura exprés + esmaltado permanente',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e7',
  //     name: 'Manicura con hidratación + esmaltado permanente',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e8',
  //     name: 'Pedicura exprés',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e9',
  //     name: 'Pedicura con hidratación',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '1111111e10',
  //     name: 'Pedicura exprés + esmaltado permanente',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e11',
  //     name: 'Pedicura con hidratación y esmaltado permanente',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e12',
  //     name: 'Retirado de esmaltado permanente manos/pies suplemento',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e13',
  //     name: 'Uñas de gel manos/pies',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e14',
  //     name: 'Relleno de gel',
  //     img: '/4.webp',
  //     route: '/accountant',
  //   },
  // ];

  // const data: TreatmentSection = {
  //   title: 'PEDICURA - MANICURA',
  //   description: [
  // 'Es importante dedicar tiempo a las manos para mantenerlas sanas, hidratadas y con las cutículas y las uñas en buen estado.',
  // 'Unas manos cuidadas y bonitas son la mejor carta de presentación. De igual manera tener los pies libres de durezas y callosidades y con las uñas bien recortadas además de belleza le aportará salud y descanso.',
  // 'Algunos de los tratamientos más destacados son:',
  // 'Esmaltado permanente.',
  // 'Permanece hasta 2-3 semanas en perfectas condiciones en manicuras e incluso más en pedicuras.',
  // 'Su aplicación es un poco más larga que la manicura convencional ya que es necesario secar las distintas capas con lámpara de LED.',
  // 'Es aconsejable acudir a un centro de estética especializado para su retirada.',
  // 'Disponible en una gran variedad de colores y en manicura francesa.',
  // 'Esmalte especialmente resistente de gran durabilidad y brillo. Excelente opción para el verano, viajes, luna de miel…',
  // 'Ahorrarás tiempo y tus uñas lucirán siempre como recién pintadas.',
  //   ],
  //   button: {
  //     buttonName: 'Volver',
  //     routeBack: '/pages/treatment',
  //   },
  // };
  return (
    <PageWrapper>
      <ManicuraPage />
    </PageWrapper>
  );
}
