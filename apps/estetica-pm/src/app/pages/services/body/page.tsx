import BodyPage from '@estetica-pm/components/pages/body';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  // const treatmnts: Treatment[] = [
  //   {
  //     id: '22222222e2',
  //     name: 'RADIOFRECUENCIA CORPORAL',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '22222222e3',
  //     name: 'ENDERMOLOGÍA (ENDOMASAJE)',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '22222222e4',
  //     name: 'PRESOTERAPIA S/S',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '22222222e5',
  //     name: 'DRENAJE LINFÁTICO MANUAL',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '22222222e6',
  //     name: 'HIGIENE DE ESPALDA',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '22222222e7',
  //     name: ' EXFOLIACIÓN CORPORAL',
  //     img: '/2.webp',
  //     route: '/accountant',
  //   },
  // ];

  // const data: TreatmentSection = {
  //   title: 'TRATAMIENTOS CORPORALES',
  //   description: [
  //     'El paso del tiempo, hábitos de vida poco saludables, estrés, sol… hace que nuestro organismo experimente cambios físicos considerables: celulitis, flacidez cutánea y muscular, sobrepeso…',
  //     'Nuestro objetivo es ofrecer un tratamiento integral para obtener el mejor resultado, por lo que es importante un control de peso, ya que si durante el tratamiento el peso corporal aumenta los resultados no serán los óptimos.',
  //     'Combinaremos los tratamientos estéticos no invasivos más avanzados: endermología, cavitación, presoterapia, radiofrecuencia, drenaje linfático, masaje reductor, reafirmante…',
  //     'Se pautará el tratamiento más indicado de acuerdo con el problema a tratar y se irá adaptando a medida que avanza el tratamiento para conseguir el objetivo deseado.',
  //   ],
  //   button: {
  //     buttonName: 'Volver',
  //     routeBack: '/pages/treatment',
  //   },
  // };
  return (
    <PageWrapper>
      <BodyPage />
    </PageWrapper>
  );
}
