import BronceadoPage from '@estetica-pm/components/pages/bronceado';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  return (
    <PageWrapper>
      <BronceadoPage />
    </PageWrapper>

    // <section className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 sm:py-24 lg:px-8">
    //   <div className="max-w-3xl">
    //     <h2 className="text-3xl font-bold sm:text-4xl">
    //       BRONCEADO: CAÑA DE AZÚCAR
    //     </h2>
    //     <p>
    //       El Bronceado en DHA es la forma más sana y libre de riesgos para
    //       lograr un bonito bronceado de una manera rápida e inocua. Ideal para
    //       eventos, bodas, noches de fiesta y otras ocasiones especiales o por el
    //       simple placer de lucir un bronceado todo el año.
    //     </p>
    //   </div>

    //   <div className="grid grid-cols-1 gap-8 lg:grid-cols-2 lg:gap-16">
    //     <Image src="/7.webp" alt="BRONCEADO" className="w-full m-5" />

    //     <article className="space-y-4">
    //       <h6>RECOMENDACIONES</h6>
    //       <h5>ANTES DEL BRONCEADO</h5>
    //       <p>
    //         Exfoliarse el mismo día o el día anterior a la sesión de bronceado
    //         haciendo hincapié en manos, pies, codos y rodillas. Dejar pasar 24h
    //         entre una depilación o 2h tras el afeitado. Durante la sesión la
    //         piel debe estar limpia y libre de cremas, perfumes, desodorantes y
    //         maquillaje.
    //       </p>

    //       <h5>DESPUÉS DEL BRONCEADO</h5>
    //       <p>
    //         Evitar ducharse o ejercicios fuertes durante las 6-8h posteriores
    //         asegurando así una perfecta fijación del DHA. Utilizar ropa oscura y
    //         suelta. Después de la aplicación y hasta la primera ducha restos de
    //         producto pueden impregnarse en la ropa clara. Estos se lavan más
    //         fácilmente en las fibras naturales como el algodón que en las fibras
    //         sintéticas.
    //       </p>
    //       <h5>CONTRAINDICACIONES</h5>
    //       <p>
    //         No aplicar durante los primeros tres meses de embarazo. Aunque no
    //         tiene efectos nocivos ni sobre la madre ni el feto en estos días se
    //         pueden producir alteraciones naturales en la melanina de la mujer.
    //         No aplicar en el pecho durante la lactancia.
    //       </p>
    //       <h6>BRONCEADO:</h6>
    //       <h5>RAYOS UVA</h5>
    //       <p>
    //         Los rayos UVA proporcionan un color estable para todo el año, sus
    //         beneficios para combatir soriasis y deficiencias de vitaminas son
    //         muy recomendables, nuestra maquina esta controlada y revisada para
    //         un uso responsable, sano y seguro.
    //       </p>
    //     </article>
    //   </div>
    //   <Link href="/pages/treatment">
    //     <Button
    //       className="mt-8 border-main-400"
    //       variant="bordered"
    //       endContent={<BackIcon />}
    //     >
    //       Volver
    //     </Button>
    //   </Link>
    // </section>
  );
}
