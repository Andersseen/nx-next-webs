import OtherPage from '@estetica-pm/components/pages/other';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  // const treatmnts: Treatment[] = [
  //   {
  //     id: '1111111e2',
  //     name: 'PERMANENTE Y TINTE DE PESTAÑAS',
  //     img: '/5.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e3',
  //     name: 'TINTE DE CEJAS Y PESTAÑAS',
  //     img: '/5.webp',
  //     route: '/accountant',
  //   },

  //   {
  //     id: '1111111e4',
  //     name: 'EXTENSIÓN DE PESTAÑAS',
  //     img: '/5.webp',
  //     route: '/accountant',
  //   },
  //   {
  //     id: '1111111e5',
  //     name: 'MAQUILLAJE',
  //     img: '/5.webp',
  //     route: '/accountant',
  //   },
  // ];

  // const data: TreatmentSection = {
  //   title: 'OTROS SERVICIOS',
  //   description: [
  //     'Unas pestañas bonitas ayudan a enmarcar la mirada y hacen que los ojos parezcan más grandes y expresivos.',
  //     'El tinte de pestañas se aplica sobre las pestañas naturales para aportar color y luminosidad al ojo aumentando así la intensidad de la mirada. Es la solución perfecta si tienes las pestañas claras o si tus pestañas son escasas ya que les dará mayor espesor.',
  //     'La permanente de pestañas (moldeado) permite rizar la pestaña natural. La curvatura del rizo dependerá de la longitud de la pestaña y del efecto que deseemos conseguir. El resultado será una mirada mas abierta y unas pestañas bien peinadas y en forma de abanico.',
  //     'La micropigmentación es una técnica que consiste básicamente en aplicar bajo las primeras capas de la piel pigmentos que dan coloración y la sensación de estar maquillada siempre. Imagen perfecta 24h al día, 365 días al año.',
  //     'Algunas aplicaciones de la micropigmentación son:',
  //     '• Micro de cejas.',
  //     '• Micro de ojos.',
  //     '• Micro de labios.',
  //     '• Micro de areolas.',
  //     '• Micro paramédica / oncológica.',
  //     '• Micro capilar (alternativo a la alopecia).',
  //     'Extensiones de pestañas',
  //     `Las extensiones de pestañas es un tratamiento de belleza semipermanente que consiste en aplicar a cada pestaña propia una pestaña de pelo sintético.

  //     Conseguirá unas pestañas bonitas, largas y siempre densas sin necesidad de maquillarlas. Con ellas podrá hacer vida normal (ducharse, cuidados faciales habituales, hacer deporte, ir a la piscina…).

  //     La primera visita dura aproximadamente 1,5-2 h. Las pestañas postizas se aplican de una manera indolora mediante un adhesivo específico hipoalergénico y a prueba de agua. Las pestañas de pelo sintético se irán cayendo poco a poco durando un máximo de 4-5 semanas.

  //     Para un resultado más duradero se recomienda hacer un retoque entre la segunda y la tercera semana. En esta fase con solo 1h podrás volver a tener el mismo efecto que el primer día.`,
  //     'Maquillaje',
  //     `El maquillaje es un elemento importante en el día a día de toda mujer. Actualmente el mundo de la cosmética y la belleza se ha desarrollado mucho y por ello en Centro de Estética Paloma Molero te queremos ofrecer el mejor servicio de maquillaje, de calidad y personalizado a tu medida.`,
  //   ],
  //   button: {
  //     buttonName: 'Volver',
  //     routeBack: '/pages/treatment',
  //   },
  // };
  return (
    <PageWrapper>
      <OtherPage />
    </PageWrapper>
  );
}
