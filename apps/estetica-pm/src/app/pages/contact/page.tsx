import Contact from '@estetica-pm/components/contact';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  return (
    <PageWrapper>
      <section className="w-full min-h-screen shadow-lg">
        <Contact />
      </section>
    </PageWrapper>
  );
}
