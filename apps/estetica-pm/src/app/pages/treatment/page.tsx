import { TiltCardList } from '@estetica-pm/components/tilt-card';
import { TREATMENTS_SERVICES as TreatmentsService } from '@estetica-pm/data/treatments';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  return (
    <PageWrapper>
      <div className="w-full min-h-screen shadow-lg grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3 place-content-center place-items-center px-4 py-12 text-[var(--main-color)]">
        <TiltCardList treatments={TreatmentsService} />
      </div>
    </PageWrapper>
  );
}
