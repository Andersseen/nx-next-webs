import About from '@estetica-pm/components/about';
import { PageWrapper } from '@nx-next/shared';

export default function Page() {
  return (
    <PageWrapper>
      <About />
    </PageWrapper>
  );
}
