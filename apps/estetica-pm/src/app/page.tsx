import { BackgroundComponent } from '../components/backgrount-section';
import { PageWrapper } from '@nx-next/shared';
import {
  Treatment,
  TreatmentSection,
} from '@estetica-pm/components/treatments-section';
import Products from '@estetica-pm/components/products';
import Landing from '@estetica-pm/components/landing';

export default function Index() {
  const treatmnts: Treatment[] = [
    {
      id: '22222222e2',
      name: 'Tratamientos Faciales',
      img: '/1.webp',
      route: '/accountant',
    },
    {
      id: '22222222e3',
      name: 'Tratamientos Corporales',
      img: '/2.webp',
      route: '/accountant',
    },

    {
      id: '22222222e4',
      name: 'Depilación Laser',
      img: '/3.webp',
      route: '/accountant',
    },
    {
      id: '22222222e5',
      name: 'Bronceado',
      img: '/7.webp',
      route: '/accountant',
    },
    {
      id: '22222222e6',
      name: 'Pedicura - Manicura',
      img: '/4.webp',
      route: '/accountant',
    },
    {
      id: '22222222e7',
      name: 'Otros Servicios',
      img: '/5.webp',
      route: '/accountant',
    },
  ];

  const data: TreatmentSection = {
    title: 'Tratamientos y servicios',
    description: [
      `En Centro de Estética Paloma Molero ofrecemos un amplio abanico de servicios que ponemos a disposición de todos nuestros clientes para cuidar de su cuerpo, su salud y su belleza. Nos adaptamos a las necesidades y requerimientos del cliente. El centro se encuentran en el barrio de las letras de Madrid desde el año 1988. En nuestro centro encontrarás novedosas soluciones estéticas. Nuestro equipo de trabajo esta en constante formación para ofrecer servicios de calidad. Combinamos una tecnología más actual con primeras marcas en cosmética. Nuestros servicios personalizados se amoldan a cualquier cliente y circunstancia.`,
    ],
    button: {
      buttonName: 'Ver los catálogos',
      routeBack: '/pages/treatment',
    },
  };
  return (
    <PageWrapper>
      <section className="shadow-lg">
        <div className="relative">
          <BackgroundComponent />
          <header className="absolute inset-0 flex items-center justify-center z-10">
            <div className="text-white text-center">
              <h1 className="text-4xl sm:text-5xl md:text-6xl lg:text-8xl xl:text-9xl">
                Paloma Molero
              </h1>
              <p className="text-base sm:text-lg md:text-xl lg:text-2xl xl:text-3xl">
                Centro de estética avanzada
              </p>
            </div>
          </header>
        </div>
        <Landing />
        <Products />
      </section>
    </PageWrapper>
  );
}
