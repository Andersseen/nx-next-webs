import { Image } from '@nextui-org/react';

export default function Products() {
  return (
    <section>
      <div className="max-w-screen-xl px-4 py-8 mx-auto sm:py-12 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-4 lg:grid-cols-3 lg:items-stretch">
          <div className="grid p-6 rounded place-content-center sm:p-8">
            <div className="max-w-md mx-auto text-center lg:text-left">
              <header>
                <h2 className="text-xl font-bold sm:text-3xl">Productos</h2>

                <p className="mt-4">
                  También vendemos productos de Jorge de la Garza y Germaine de
                  Capuccini
                </p>
                <p className="font-semibold mt-4">Cruelty free and vegan</p>
                <p className="mt-4">Pregunta en nuestro centro de estética</p>
              </header>
            </div>
          </div>

          <div className="lg:col-span-2 lg:py-8">
            <ul className="grid grid-cols-2 gap-4">
              <li>
                <Image
                  isBlurred
                  src="/gc.webp"
                  alt="NextUI Album Cover"
                  className="m-5 w-full"
                />
              </li>

              <li>
                <Image
                  isBlurred
                  src="/jdlg.webp"
                  alt="NextUI Album Cover"
                  className="m-5 w-full"
                />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
}
