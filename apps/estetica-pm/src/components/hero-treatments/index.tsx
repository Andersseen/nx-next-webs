'use client';
import { motion } from 'framer-motion';
import { useEffect, useRef, useState } from 'react';

const ShuffleHero = () => {
  return (
    <section className="w-full min-h-screen p-4 grid grid-cols-1 md:grid-cols-2 items-center gap-8 max-w-6xl m-auto">
      <div>
        <span className="block mb-4 text-xs md:text-sm font-medium">
          Estética Paloma Molero
        </span>

        <p className="text-base md:text-lgmy-4 md:my-6">
          En Centro de Estética Paloma Molero ofrecemos un amplio abanico de
          servicios que ponemos a disposición de todos nuestros clientes para
          cuidar de su cuerpo, su salud y su belleza. Nos adaptamos a las
          necesidades y requerimientos del cliente. El centro se encuentran en
          el barrio de las letras de Madrid desde el año 1988. En nuestro centro
          encontrarás novedosas soluciones estéticas. Nuestro equipo de trabajo
          esta en constante formación para ofrecer servicios de calidad.
          Combinamos una tecnología más actual con primeras marcas en cosmética.
          Nuestros servicios personalizados se amoldan a cualquier cliente y
          circunstancia.
        </p>
      </div>
      <ShuffleGrid />
      <h3 className="text-xl md:text-2xl font-semibold">
        ¿Sobre que tratamiento quieres sabe?
      </h3>
    </section>
  );
};

const shuffle = (array) => {
  let currentIndex = array.length,
    randomIndex;

  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

const squareData = [
  {
    id: 1,
    src: '/shuffle/1-min.jpg',
  },
  {
    id: 2,
    src: '/shuffle/2-min.jpg',
  },
  {
    id: 3,
    src: '/shuffle/3-min.jpg',
  },
  {
    id: 4,
    src: '/shuffle/4-min.jpg',
  },
  {
    id: 5,
    src: '/shuffle/5-min.jpg',
  },
  {
    id: 6,
    src: '/shuffle/6-min.jpg',
  },
  {
    id: 7,
    src: '/shuffle/7-min.jpg',
  },
  {
    id: 8,
    src: '/shuffle/8-min.jpg',
  },
  {
    id: 9,
    src: '/shuffle/9-min.jpg',
  },
  {
    id: 10,
    src: '/shuffle/10-min.jpg',
  },
  {
    id: 11,
    src: '/shuffle/11-min.jpg',
  },
  {
    id: 12,
    src: '/shuffle/12-min.jpg',
  },
  {
    id: 13,
    src: '/5.jpg',
  },
  {
    id: 14,
    src: '/3.jpg',
  },
  {
    id: 15,
    src: '/2.jpg',
  },
  {
    id: 16,
    src: '/1.jpg',
  },
];

const generateSquares = () => {
  return shuffle(squareData).map((sq) => (
    <motion.div
      key={sq.id}
      layout
      transition={{ duration: 1.5, type: 'spring' }}
      className="w-full h-full"
      style={{
        backgroundImage: `url(${sq.src})`,
        backgroundSize: 'cover',
      }}
    ></motion.div>
  ));
};

const ShuffleGrid = () => {
  const timeoutRef = useRef(null);
  const [squares, setSquares] = useState(generateSquares());

  useEffect(() => {
    shuffleSquares();

    return () => clearTimeout(timeoutRef.current);
  }, []);

  const shuffleSquares = () => {
    setSquares(generateSquares());

    timeoutRef.current = setTimeout(shuffleSquares, 3000);
  };

  return (
    <div className="grid grid-cols-4 grid-rows-4 h-[450px] gap-1">
      {squares.map((sq) => sq)}
    </div>
  );
};

export default ShuffleHero;
