import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Popover,
  PopoverTrigger,
  PopoverContent,
  Button,
} from '@nextui-org/react';
import { FacebookIcon, InstagramIcon } from '@nx-next/shared';
import { SVGProps } from 'react';

const iconProps = {
  fill: 'var(--foreground-color)',
  width: '32px',
  height: '32px',
};

export default function Contact() {
  return (
    <section className="body-font relative min-h-screen flex justify-end">
      <div className="absolute inset-0">
        <iframe
          width="100%"
          height="100%"
          title="map"
          loading="lazy"
          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12151.289730856768!2d-3.6983502!3d40.4127843!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42262a064c5be7%3A0x978f2f4cff66454!2sPaloma%20Molero!5e0!3m2!1ses!2ses!4v1699703890789!5m2!1ses!2ses"
        ></iframe>
      </div>
      <div className="p-8">
        <Popover placement="bottom" showArrow={true} backdrop="opaque">
          <PopoverTrigger>
            <Button size="lg" variant="shadow" startContent={<SearchIcon />}>
              Contactos
            </Button>
          </PopoverTrigger>
          <PopoverContent>
            <ContactCard />
          </PopoverContent>
        </Popover>
      </div>
    </section>
  );
}

const ContactCard = () => {
  return (
    <Card className="max-w-[400px]">
      <CardHeader className="flex gap-3">
        <div className="flex flex-col">
          <p className="text-md">C/ AMOR DE DIOS 9. MADRID</p>
          <p className="text-small">WhatsApp: 696 79 74 83</p>
          <p className="text-md">TEL: 914 294 686</p>
          <p className="text-small">esteticapaloma.molero@gmail.com</p>
        </div>
      </CardHeader>
      <Divider />
      <CardBody className="flex flex-col">
        <p className="text-md">HORARIO</p>
        <p className="text-small">DE LUNES A VIERNES DE 10:00 A 20:00 H.</p>
        <p className="text-md">SÁBADOS DE 10:00 A 14:00 H.</p>
        <p className="text-small">Servicio a domicilio consultar precios</p>
      </CardBody>
      <Divider />
      <CardFooter className="flex justify-evenly">
        <FacebookIcon props={iconProps} />
        <InstagramIcon props={iconProps} />
      </CardFooter>
    </Card>
  );
};

export function SearchIcon(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        fill="currentColor"
        d="M17.25 10a7.25 7.25 0 1 0-2.681 5.63l4.9 4.9l.085.073a.75.75 0 0 0 .976-1.133l-4.9-4.901A7.22 7.22 0 0 0 17.25 10ZM11 7a1 1 0 1 1-2 0a1 1 0 0 1 2 0Zm-1 2a.75.75 0 0 1 .75.75v3.5a.75.75 0 0 1-1.5 0v-3.5A.75.75 0 0 1 10 9Z"
      />
    </svg>
  );
}
