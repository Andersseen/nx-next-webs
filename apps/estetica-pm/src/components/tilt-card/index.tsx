'use client';
import React from 'react';
import { motion, useMotionValue, useSpring, useTransform } from 'framer-motion';
import Link from 'next/link';
import { TreatmentService } from '@estetica-pm/data/treatments';

type TiltCardProps = {
  treatment: TreatmentService;
};

export const TiltCard = ({ treatment }: TiltCardProps) => {
  const x = useMotionValue(0);
  const y = useMotionValue(0);

  const mouseXSpring = useSpring(x);
  const mouseYSpring = useSpring(y);

  const rotateX = useTransform(
    mouseYSpring,
    [-0.5, 0.5],
    ['17.5deg', '-17.5deg']
  );
  const rotateY = useTransform(
    mouseXSpring,
    [-0.5, 0.5],
    ['-17.5deg', '17.5deg']
  );

  const handleMouseMove = (e) => {
    const rect = e.target.getBoundingClientRect();

    const width = rect.width;
    const height = rect.height;

    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;

    const xPct = mouseX / width - 0.5;
    const yPct = mouseY / height - 0.5;

    x.set(xPct);
    y.set(yPct);
  };

  const handleMouseLeave = () => {
    x.set(0);
    y.set(0);
  };

  return (
    <Link href={treatment.route}>
      <motion.div
        onMouseMove={handleMouseMove}
        onMouseLeave={handleMouseLeave}
        style={{
          rotateY,
          rotateX,
          transformStyle: 'preserve-3d',
        }}
        className="relative h-80 w-80 rounded-xl bg-[var(--foreground-color)]"
      >
        <p
          style={{
            transform: 'translateZ(50px)',
          }}
          className="text-center text-2xl font-bold"
        >
          {treatment.name}
        </p>
        <div
          style={{
            backgroundImage: `url(${treatment.img})`,
            backgroundSize: 'cover',
            transform: 'translateZ(75px)',
            transformStyle: 'preserve-3d',
          }}
          className="absolute inset-4 top-8 grid place-content-center gap-4 place-items-center rounded-xl shadow-lg"
        ></div>
      </motion.div>
    </Link>
  );
};

type TiltCardListProps = {
  treatments: TreatmentService[];
};

export const TiltCardList = ({ treatments }: TiltCardListProps) => {
  return (
    <>
      {treatments.map((item, index) => (
        <motion.div
          key={item.id}
          variants={{
            hidden: { opacity: 0, y: 75 },
            visible: { opacity: 1, y: 0 },
          }}
          initial="hidden"
          animate="visible"
          transition={{
            duration: 0.5,
            delay: Number(`0.${index}`),
            ease: 'easeIn',
          }}
        >
          <TiltCard treatment={item} />
        </motion.div>
      ))}
    </>
  );
};
