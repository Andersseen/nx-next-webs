'use client';
import React, { useState, useEffect, useRef } from 'react';
import * as THREE from 'three';
import VantaFog from 'vanta/dist/vanta.fog.min';

export const BackgroundComponent: React.FC = () => {
  const [vantaEffect, setVantaEffect] = useState<any>(null);
  const myRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (!vantaEffect) {
      const myVantaEffect = VantaFog({
        el: myRef.current!,
        THREE,
        mouseControls: true,
        touchControls: true,
        gyroControls: false,
        blurFactor: 0.7,
        speed: 0.3,
        zoom: 0.6,
        highlightColor: 0x5d5d5d,
        midtoneColor: 0x888888,
        lowlightColor: 0xd1d1d1,
        baseColor: 0xf6f6f6,
      });

      setVantaEffect(myVantaEffect);
    }

    return () => {
      if (vantaEffect) {
        vantaEffect.destroy();
      }
    };
  }, [vantaEffect]);

  return <section ref={myRef} className="min-h-screen w-full" />;
};
