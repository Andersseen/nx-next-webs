'use client';
import React from 'react';
import {
  Link,
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  Button,
  DropdownItem,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  NavbarMenuToggle,
  NavbarMenu,
  NavbarMenuItem,
  Accordion,
  AccordionItem,
} from '@nextui-org/react';
import { usePathname } from 'next/navigation';
import { menuItems } from '@estetica-pm/data/links';
import { useTheme } from 'next-themes';

type ColourBgType = {
  active: string;
  notActive: string;
};
export default function HeaderNavigation({
  logo,

  themeSwitcher,
}: {
  logo: React.ReactNode | string;
  themeSwitcher: React.ReactNode;
}) {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const { resolvedTheme } = useTheme();

  const colourBg: Record<string, ColourBgType> = {
    light: {
      active: 'text-main-950 font-bold bg-main-300',
      notActive: 'text-main-900',
    },
    dark: {
      active: 'text-main-100 font-bold bg-main-600',
      notActive: 'text-main-300',
    },
  };

  const themeColour = colourBg[resolvedTheme || 'light'];

  let pathname = usePathname() || '/';

  if (pathname.includes('/writing/')) {
    pathname = '/writing';
  }

  const [hoveredPath, setHoveredPath] = React.useState(pathname);

  return (
    <Navbar>
      <NavbarContent justify="start">
        <NavbarMenuToggle
          aria-label={isMenuOpen ? 'Close menu' : 'Open menu'}
          className="sm:hidden"
        />
        <NavbarBrand>
          <Link color="foreground" href="/">
            {logo}
          </Link>
        </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        {menuItems.map((item, index) => {
          const isActive = item.route === pathname;
          return (
            <div key={`${item.id}_${index}`}>
              {item.route ? (
                <NavbarItem>
                  <Link
                    className={`w-full text-center rounded-xl ${
                      isActive ? themeColour.active : themeColour.notActive
                    }`}
                    href={item.route}
                    aria-current="page"
                    color="foreground"
                  >
                    <Button
                      disableRipple
                      className="p-0 bg-transparent data-[hover=true]:bg-main-400"
                      radius="sm"
                      variant="light"
                    >
                      {item.name}
                    </Button>
                  </Link>
                </NavbarItem>
              ) : (
                <Dropdown
                  key={`${item.id}__${index}`}
                  classNames={{
                    content:
                      'py-1 px-1 border border-default-200 bg-gradient-to-br from-main-600 to-main-300',
                  }}
                >
                  <NavbarItem key={`${item.id}+${index}`}>
                    <DropdownTrigger>
                      <Button
                        disableRipple
                        className="p-0 bg-transparent data-[hover=true]:bg-main-400"
                        radius="sm"
                        variant="light"
                      >
                        {item.name}
                      </Button>
                    </DropdownTrigger>
                  </NavbarItem>
                  <DropdownMenu
                    itemClasses={{
                      base: [
                        'gap-4',
                        'rounded-md',
                        'dark:data-[hover=true]:bg-main-600',
                        'data-[hover=true]:bg-main-200',
                        'data-[selectable=true]:focus:bg-main-50',
                        'data-[pressed=true]:opacity-70',
                        'data-[focus-visible=true]:ring-main-500',
                      ],
                    }}
                  >
                    {item.links.map((item, index) => (
                      <DropdownItem key={`${item.id}.${index}`}>
                        <Link
                          className="w-full text-center"
                          href={item.route}
                          aria-current="page"
                          color="foreground"
                        >
                          {item.name}
                        </Link>
                      </DropdownItem>
                    ))}
                  </DropdownMenu>
                </Dropdown>
              )}
            </div>
          );
        })}
      </NavbarContent>
      <NavbarContent justify="end">{themeSwitcher}</NavbarContent>
      <NavbarMenu>
        {menuItems.map((item, index) => (
          <NavbarMenuItem key={`${index}_-${item.id}`}>
            {item.route ? (
              <Button
                className="w-full justify-start"
                href={item.route}
                as={Link}
                variant="light"
                size="lg"
              >
                <span>{item.name}</span>
              </Button>
            ) : (
              <Accordion variant="light" className="text-center">
                <AccordionItem
                  key={index}
                  aria-label="Accordion 1"
                  title={item.name}
                  className="text-center"
                >
                  {item.links.map((item, index) => (
                    <Button
                      key={`${item.id}-${index}`}
                      className="w-full text-center"
                      href={item.route}
                      as={Link}
                      variant="light"
                      size="lg"
                    >
                      {item.name}
                    </Button>
                  ))}
                </AccordionItem>
              </Accordion>
            )}
          </NavbarMenuItem>
        ))}
      </NavbarMenu>
    </Navbar>
  );
}
