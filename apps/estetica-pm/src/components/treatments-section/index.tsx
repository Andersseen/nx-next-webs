'use client';
import { Card, CardFooter, Image, Button, Link } from '@nextui-org/react';
import { SVGProps } from 'react';

export type Treatment = {
  id: string;
  name: string;
  img: string;
  route: string;
};
type TreatmentSectionButton = {
  routeBack: string;
  buttonName: string;
};
export type TreatmentSection = {
  title: string;
  description: string | string[];
  button: TreatmentSectionButton | null;
};

type FacialTreatmentsProps = {
  data: TreatmentSection;
  treatments: Treatment[];
};

export default function TreatmentsSection(props: FacialTreatmentsProps) {
  return (
    <section className="w-full min-h-screen px-4 py-8 sm:px-6 sm:py-12 lg:px-8 lg:py-16 shadow-lg">
      <div className="grid grid-cols-1 gap-y-8 lg:grid-cols-2 lg:items-center lg:gap-x-16">
        <div className="mx-auto text-left lg:mx-0 ltr:lg:text-left rtl:lg:text-right">
          <h2 className="text-3xl font-bold sm:text-4xl">{props.data.title}</h2>
          {Array.isArray(props.data.description) ? (
            <>
              {props.data.description.map((item, index) => (
                <p key={index} className="mt-4">
                  {item}
                </p>
              ))}
            </>
          ) : (
            <p className="mt-4">{props.data.description}</p>
          )}
          <>
            {props.data.button && (
              <Link href={props.data.button.routeBack}>
                <Button
                  className="mt-8 border-main-400"
                  variant="bordered"
                  endContent={<BackIcon />}
                >
                  {props.data.button.buttonName}
                </Button>
              </Link>
            )}
          </>
        </div>

        <div className="grid grid-cols-2 gap-4 sm:grid-cols-3">
          {props.treatments.map((treatment) => (
            <Link key={treatment.id} href={treatment.route}>
              <Card
                isFooterBlurred
                radius="lg"
                className="border border-[var(--foreground-color)] shadow-sm hover:border-[var(--foreground-color)] hover:ring-1 hover:ring-[var(--foreground-color)] focus:outline-none focus:ring"
              >
                <Image
                  alt="Woman listing to music"
                  className="object-cover h-44 w-60"
                  src={treatment.img}
                />
                <CardFooter className="justify-center before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
                  <p className="text-xl">{treatment.name}</p>
                </CardFooter>
              </Card>
            </Link>
          ))}
        </div>
      </div>
    </section>
  );
}

export function BackIcon(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 24 24"
      {...props}
    >
      <g fill="none">
        <path d="M24 0v24H0V0h24ZM12.593 23.258l-.011.002l-.071.035l-.02.004l-.014-.004l-.071-.035c-.01-.004-.019-.001-.024.005l-.004.01l-.017.428l.005.02l.01.013l.104.074l.015.004l.012-.004l.104-.074l.012-.016l.004-.017l-.017-.427c-.002-.01-.009-.017-.017-.018Zm.265-.113l-.013.002l-.185.093l-.01.01l-.003.011l.018.43l.005.012l.008.007l.201.093c.012.004.023 0 .029-.008l.004-.014l-.034-.614c-.003-.012-.01-.02-.02-.022Zm-.715.002a.023.023 0 0 0-.027.006l-.006.014l-.034.614c0 .012.007.02.017.024l.015-.002l.201-.093l.01-.008l.004-.011l.017-.43l-.003-.012l-.01-.01l-.184-.092Z" />
        <path
          fill="currentColor"
          d="M7.16 10.972A7 7 0 0 1 19.5 15.5a1.5 1.5 0 1 0 3 0c0-5.523-4.477-10-10-10a9.973 9.973 0 0 0-7.418 3.295L4.735 6.83a1.5 1.5 0 1 0-2.954.52l1.042 5.91c.069.391.29.74.617.968c.403.282.934.345 1.385.202l5.644-.996a1.5 1.5 0 1 0-.52-2.954l-2.788.491Z"
        />
      </g>
    </svg>
  );
}
