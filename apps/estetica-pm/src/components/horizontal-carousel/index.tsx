'use client';
import { motion, useTransform, useScroll } from 'framer-motion';
import { useRef } from 'react';
import { Card, CardFooter, Image, Button } from '@nextui-org/react';
import {
  TreatmentService,
  TREATMENTS_SERVICES as TreatmentServices,
} from '@estetica-pm/data/treatments';
import Link from 'next/link';

const HorizontalCarousel = () => {
  const targetRef = useRef(null);
  const { scrollYProgress } = useScroll({
    target: targetRef,
  });

  const x = useTransform(scrollYProgress, [0, 1], ['1%', '-95%']);

  return (
    <section ref={targetRef} className="relative h-[300vh]">
      <div className="sticky top-0 flex h-screen items-center overflow-hidden">
        <motion.div style={{ x }} className="flex gap-4">
          {TreatmentServices.map((card) => {
            return <TreatmentCard card={card} key={card.id} />;
          })}
        </motion.div>
      </div>
    </section>
  );
};

const TreatmentCard = ({ card }: { card: TreatmentService }) => {
  return (
    <Card
      isFooterBlurred
      key={card.id}
      className="group relative h-[450px] w-[450px] overflow-hidden rounded-lg"
    >
      <Image
        removeWrapper
        alt="Relaxing app background"
        className="z-0 w-full h-full object-cover"
        src={card.img}
      />

      <CardFooter className="absolute bottom-0 z-10 border-t-1 border-default-600 dark:border-default-100">
        <div className="flex flex-grow gap-2 items-center">
          <h6 className="text-lg">{card.name}</h6>
        </div>
        <Link href={card.route}>
          <Button radius="full" size="sm">
            Ver catálago
          </Button>
        </Link>
      </CardFooter>
    </Card>
  );
};

export default HorizontalCarousel;
