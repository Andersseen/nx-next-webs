'use client';
import { Image } from '@nextui-org/react';
import { motion } from 'framer-motion';

export default function About() {
  return (
    <section className="shadow-lg overflow-x-auto mx-auto w-full min-h-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
      <div className="grid grid-cols-1 gap-8 lg:grid-cols-2 lg:gap-16">
        <motion.div
          variants={{
            hidden: { opacity: 0, x: 75 },
            visible: { opacity: 1, x: 0 },
          }}
          initial="hidden"
          animate="visible"
          transition={{
            type: 'spring',
            bounce: 0.5,
            duration: 1,
            delay: 0.3,
          }}
          className="relative h-64 overflow-hidden rounded-lg sm:h-80 lg:order-last lg:h-full"
        >
          <Image
            alt="Beauty salon"
            src="/img/47.webp"
            className="h-full w-full object-cover"
          />
        </motion.div>

        <div className="lg:py-24">
          <motion.div
            variants={{
              hidden: { opacity: 0, y: -75 },
              visible: { opacity: 1, y: 0 },
            }}
            initial="hidden"
            animate="visible"
            transition={{
              type: 'spring',
              bounce: 0.5,
              duration: 1,
              delay: 0.3,
            }}
          >
            <h2 className="text-3xl font-bold sm:text-4xl">Paloma Molero</h2>
            <h4 className="text-lg font-semibold sm:text-xl">
              Centro de belleza
            </h4>
          </motion.div>

          <motion.p
            variants={{
              hidden: { opacity: 0, y: 75 },
              visible: { opacity: 1, y: 0 },
            }}
            initial="hidden"
            animate="visible"
            transition={{
              type: 'spring',
              bounce: 0.5,
              duration: 1,
              delay: 0.3,
            }}
            className="mt-4"
          >
            En Centro de Estética Paloma Molero ofrecemos un amplio abanico de
            servicios que ponemos a disposición de todos nuestros clientes para
            cuidar de su cuerpo, su salud y su belleza. Nos adaptamos a las
            necesidades y requerimientos del cliente. El centro se encuentran en
            el barrio de las letras de Madrid desde el año 1988. En nuestro
            centro encontrarás novedosas soluciones estéticas. Nuestro equipo de
            trabajo esta en constante formación para ofrecer servicios de
            calidad. Combinamos una tecnología más actual con primeras marcas en
            cosmética. Nuestros servicios personalizados se amoldan a cualquier
            cliente y circunstancia.
          </motion.p>
        </div>
      </div>
    </section>
  );
}
