'use client';
import { CheckIcon } from '@nx-next/shared';
import { TREATMENT_PRICES as treatmentPrices } from '@estetica-pm/data/treatment-prices';
import {
  Card,
  CardHeader,
  CardBody,
  Divider,
  Listbox,
  ListboxItem,
} from '@nextui-org/react';
import { motion } from 'framer-motion';

export const TreatmentPrices = () => {
  return (
    <div className="grid grid-cols-12 grid-rows-2 gap-4 px-0 md:px-8 mt-6">
      {treatmentPrices.map((item, index) => (
        <>
          {item.doubleRow ? (
            <motion.div
              variants={{
                hidden: { opacity: 0, y: 75 },
                visible: { opacity: 1, y: 0 },
              }}
              initial="hidden"
              animate="visible"
              transition={{
                type: 'spring',
                bounce: 0.5,
                duration: 1,
                delay: Number(`0.${index}`),
              }}
              key={item.id}
              className="col-span-12 md:col-span-6 lg:col-span-4 row-span-1"
            >
              <Card className=" bg-transparent">
                <CardHeader className="flex gap-3">
                  <CheckIcon />

                  <h5 className="text-md">{item.name}</h5>
                </CardHeader>
                <Divider />
                <CardBody>
                  <Listbox variant="flat">
                    {item.treatments.map((item, index) => (
                      <ListboxItem key={index}>
                        <div className="flex justify-between">
                          <span className="text-xs text-semibold text-center">
                            {item.name}
                          </span>
                          <span className="text-xs text-semibold text-center">
                            {item.price}
                          </span>
                        </div>
                      </ListboxItem>
                    ))}
                  </Listbox>
                </CardBody>
              </Card>
            </motion.div>
          ) : (
            <motion.div
              variants={{
                hidden: { opacity: 0, y: 75 },
                visible: { opacity: 1, y: 0 },
              }}
              initial="hidden"
              animate="visible"
              transition={{
                type: 'spring',
                bounce: 0.5,
                duration: 1,
                delay: Number(`0.${index}`),
              }}
              key={item.id}
              className="col-span-12 md:col-span-6 lg:col-span-4 row-span-2"
            >
              <Card className=" bg-transparent">
                <CardHeader className="flex gap-3">
                  <CheckIcon />

                  <h5 className="text-md">{item.name}</h5>
                </CardHeader>
                <Divider />
                <CardBody>
                  <Listbox variant="flat">
                    {item.treatments.map((item, index) => (
                      <ListboxItem key={index}>
                        <div className="flex justify-between flex-wrap whitespace-normal">
                          <span className="text-xs text-semibold text-center w-5/6 sm:w-fit flex justify-start sm:inline">
                            {item.name}
                          </span>
                          <span className="text-xs text-semibold text-center w-1/6 sm:w-fit flex justify-end sm:inline">
                            {item.price}
                          </span>
                        </div>
                      </ListboxItem>
                    ))}
                  </Listbox>
                </CardBody>
              </Card>
            </motion.div>
          )}
        </>
      ))}
    </div>
  );
};
