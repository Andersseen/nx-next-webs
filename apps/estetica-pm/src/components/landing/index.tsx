import { Image, Button, Link } from '@nextui-org/react';
import { SVGProps } from 'react';
export default function Landing() {
  return (
    <section className="w-full my-24 mx-auto md:px-6 shadow-lg">
      <div className="pb-24">
        <h2 className="mb-16 text-center text-xl sm:text-4xl font-bold uppercase">
          Nuestros tratamintos y servicios
        </h2>
        {/* ===faacial=== */}
        <div className="mb-16 flex flex-wrap">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pr-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/44.webp" className="w-full" alt="Image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              Tratamintos faciales
            </h3>

            <p className="text-justify mb-6 px-8 sm:p-4 pl-12">
              La piel es el principal órgano de belleza, revela nuestra edad,
              nuestra salud y hasta nuestro estado de ánimo. A ella se lo
              debemos todo. Nos protege de agentes nocivos, regula el
              intercambio molecular de oxígeno, agua, minerales…
            </p>
            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/facial"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
        {/* ===body=== */}
        <div className="mb-16 flex flex-wrap lg:flex-row-reverse">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pl-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/21.webp" className="w-full" alt="image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              Tratamintos corporales
            </h3>

            <p className="text-justify mb-6 px-8 sm:p-4 pl-12">
              El paso del tiempo, hábitos de vida poco saludables, estrés, sol…
              hace que nuestro organismo experimente cambios físicos
              considerables: celulitis, flacidez cutánea y muscular, sobrepeso…
            </p>
            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/body"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
        {/* ===depilacion=== */}
        <div className="mb-16 flex flex-wrap">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pr-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/13.webp" className="w-full" alt="Image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              DEPILACIÓN DIODO ALTA POTENCIA
            </h3>

            <p className="text-justify mb-6 px-8 sm:p-4 pl-12">
              Si te molesta el vello, hoy cambiará tu vida. Llega el nuevo Láser
              DIODO ALTA POTENCIA. Es la última innovación tecnológica para
              eliminar el vello.
            </p>
            <p className="text-justify mb-6 px-8 sm:p-4 pl-12">
              +Eficaz +Rápido
            </p>
            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/laser"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
        {/* ===BRONCEADO=== */}
        <div className="mb-16 flex flex-wrap lg:flex-row-reverse">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pl-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/26.webp" className="w-full" alt="image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              BRONCEADO
            </h3>

            <h6 className="mb-4 text-xl font-bold p-x12 sm:p-4 pl-12">
              - CAÑA DE AZÚCAR
            </h6>
            <h6 className="mb-4 text-xl font-bold p-x12 sm:p-4 pl-12">
              - Cabina vertical RAYOS UVA
            </h6>
            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/bronceado"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
        {/* ===PEDICURA - MANICURA=== */}
        <div className="mb-16 flex flex-wrap">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pr-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/39.webp" className="w-full" alt="Image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              PEDICURA - MANICURA
            </h3>

            <p className="text-justify mb-6 px-8 sm:p-4 pl-12">
              Es importante dedicar tiempo a las manos para mantenerlas sanas,
              hidratadas y con las cutículas y las uñas en buen estado. Unas
              manos cuidadas y bonitas son la mejor carta de presentación...
            </p>
            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/manicura"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
        {/* ===Other=== */}
        <div className="mb-16 flex flex-wrap lg:flex-row-reverse">
          <div className="mb-6 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-6/12 lg:pl-6">
            <div
              className="ripple relative overflow-hidden rounded-lg bg-cover bg-[50%] bg-no-repeat shadow-lg dark:shadow-black/20"
              data-te-ripple-init
              data-te-ripple-color="light"
            >
              <Image src="/img/46.webp" className="w-full" alt="image" />
            </div>
          </div>

          <div className="flex flex-col justify-center align-center w-full shrink-0 grow-0 basis-auto lg:w-6/12 lg:pl-6">
            <h3 className="mb-4 text-center text-xl sm:text-4xl font-bold uppercase">
              Otros tratamientos
            </h3>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Lifting y tinte de pestañas
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Tinte de cejas
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Depilación con hilo
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Depilación con cera
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Maquillaje
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Micropigmentación
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Microshading
            </h6>
            <h6 className="text-xl font-bold px-12 sm:py-2 pl-12">
              - Microblanding
            </h6>

            <div className="mt-2 md:mt-4 flex justify-end sm:justify-start px-4">
              <Button
                variant="faded"
                startContent={<BackIcon />}
                as={Link}
                href="/pages/services/other"
              >
                LEER MÁS
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export function BackIcon(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 24 24"
      {...props}
    >
      <g fill="none">
        <path d="M24 0v24H0V0h24ZM12.593 23.258l-.011.002l-.071.035l-.02.004l-.014-.004l-.071-.035c-.01-.004-.019-.001-.024.005l-.004.01l-.017.428l.005.02l.01.013l.104.074l.015.004l.012-.004l.104-.074l.012-.016l.004-.017l-.017-.427c-.002-.01-.009-.017-.017-.018Zm.265-.113l-.013.002l-.185.093l-.01.01l-.003.011l.018.43l.005.012l.008.007l.201.093c.012.004.023 0 .029-.008l.004-.014l-.034-.614c-.003-.012-.01-.02-.02-.022Zm-.715.002a.023.023 0 0 0-.027.006l-.006.014l-.034.614c0 .012.007.02.017.024l.015-.002l.201-.093l.01-.008l.004-.011l.017-.43l-.003-.012l-.01-.01l-.184-.092Z" />
        <path
          fill="currentColor"
          d="M7.16 10.972A7 7 0 0 1 19.5 15.5a1.5 1.5 0 1 0 3 0c0-5.523-4.477-10-10-10a9.973 9.973 0 0 0-7.418 3.295L4.735 6.83a1.5 1.5 0 1 0-2.954.52l1.042 5.91c.069.391.29.74.617.968c.403.282.934.345 1.385.202l5.644-.996a1.5 1.5 0 1 0-.52-2.954l-2.788.491Z"
        />
      </g>
    </svg>
  );
}
