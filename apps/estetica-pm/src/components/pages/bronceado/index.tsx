import { Image } from '@nextui-org/react';
import { Button, Link } from '@nextui-org/react';
import { IconWhatsapp } from '@nx-next/shared';
import ContentSection, {
  BackgroundSVG,
  ContentSectionData,
} from '../content-section';

export default function BronceadoPage() {
  const TanningContentPage: ContentSectionData[] = [
    {
      backgroundFlip: false,
      imgSrc: '/img/26.webp',
      button: true,
      imgSide: 'R',
      pageTitle: true,
      title: 'BRONCEADO: CAÑA DE AZÚCAR',
      description: [
        `El Bronceado en DHA es la forma más sana y libre de riesgos para
        lograr un bonito bronceado de una manera rápida e inocua. Ideal
        para eventos, bodas, noches de fiesta y otras ocasiones
        especiales o por el simple placer de lucir un bronceado todo el
        año.`,
      ],
    },
    {
      backgroundFlip: false,
      imgSrc: '/img/48.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'Cabina vertical RAYOS UVA',
      description: [
        `Mejora el aspecto de la piel. Los tratamientos de rayos UVA son especialmente beneficiosos en casos de psoriasis, vitíligo o problemas de acné. Antes de comenzar a tomar rayos UVA será necesario consultar con un dermatólogo.
        Levanta el estado de ánimo. Los rayos UVA aumentan la producción de un neurotransmisor que se encuentra en nuestro cerebro llamado serotonina. La serotonina interviene en la regulación del sueño, la temperatura del cuerpo y la conducta sexual alejando el estrés o el cansancio. ¿Nunca has notado que durante los días de sol estamos mucho más relajados?
        Mejora el reuma y la artritis. Los rayos UV ayudan a producir vitamina D, una vitamina fundamental para la mineralización de los huesos.
        Es rápido y cómodo. Es la forma más rápida de lucir un bronceado cuando el clima no acompaña o no tenemos tiempo para tomar el sol.
        El resultado es duradero y uniforme.`,
      ],
    },
  ];

  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={TanningContentPage[0]} />
      <SecondSection />
      <ContentSection content={TanningContentPage[1]} />
      <LastSection />
    </section>
  );
}

// const FirstSection = () => {
//   return (
//     <section className="mb-20 min-h-screen shadow-lg">
//       <BackgroundSVG flip={false} />

//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 BRONCEADO: CAÑA DE AZÚCAR
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg">
//                 El Bronceado en DHA es la forma más sana y libre de riesgos para
//                 lograr un bonito bronceado de una manera rápida e inocua. Ideal
//                 para eventos, bodas, noches de fiesta y otras ocasiones
//                 especiales o por el simple placer de lucir un bronceado todo el
//                 año.
//               </p>
//               <div className="mt-4 md:mt-8">
//                 <Button
//                   variant="faded"
//                   startContent={<IconWhatsapp />}
//                   as={Link}
//                   isExternal
//                   href="https://wa.me/+34696797483"
//                 >
//                   RESERVA TU TRATAMIENTO
//                 </Button>
//               </div>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/26.webp"
//               alt="BRONCEADO"
//               className="w-wull rounded-lg shadow-lg z-0"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
const SecondSection = () => {
  return (
    <section className="max-h-[560px]">
      <BackgroundSVG flip={true} />
      <div className="mx-auto w-full h-full">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
          <div className="grid grid-cols-2 gap-4 md:grid-cols-1 lg:grid-cols-2">
            <Image
              alt="Student"
              src="/img/28.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full max-h-[560px]"
            />

            <Image
              alt="Student"
              src="/img/27.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full max-h-[560px]"
            />
          </div>
          <div className="md:p-12 lg:px-16 lg:py-24">
            <div className="mx-auto max-w-xl text-center">
              <h6>RECOMENDACIONES</h6>
              <h5>ANTES DEL BRONCEADO</h5>
              <p>
                Exfoliarse el mismo día o el día anterior a la sesión de
                bronceado haciendo hincapié en manos, pies, codos y rodillas.
                Dejar pasar 24h entre una depilación o 2h tras el afeitado.
                Durante la sesión la piel debe estar limpia y libre de cremas,
                perfumes, desodorantes y maquillaje.
              </p>

              <h5>DESPUÉS DEL BRONCEADO</h5>
              <p>
                Evitar ducharse o ejercicios fuertes durante las 6-8h
                posteriores asegurando así una perfecta fijación del DHA.
                Utilizar ropa oscura y suelta. Después de la aplicación y hasta
                la primera ducha restos de producto pueden impregnarse en la
                ropa clara. Estos se lavan más fácilmente en las fibras
                naturales como el algodón que en las fibras sintéticas.
              </p>
              <h5>CONTRAINDICACIONES</h5>
              <p>
                No aplicar durante los primeros tres meses de embarazo. Aunque
                no tiene efectos nocivos ni sobre la madre ni el feto en estos
                días se pueden producir alteraciones naturales en la melanina de
                la mujer. No aplicar en el pecho durante la lactancia.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
const LastSection = () => {
  return (
    <section className="overflow-hidden bg-[url(/img/29.webp)] bg-cover bg-center bg-no-repeat shadow-lg">
      <div className="bg-black/20 p-8 md:p-12 lg:px-16 lg:py-24">
        <div className="text-center ltr:sm:text-left rtl:sm:text-right">
          <h2 className="text-2xl font-bold sm:text-3xl md:text-5xl">
            BROCEADO SIN SOL
          </h2>

          <div className="mt-4 md:mt-8">
            <Button
              variant="faded"
              startContent={<IconWhatsapp />}
              as={Link}
              isExternal
              href="https://wa.me/+34696797483"
            >
              RESERVA TU TRATAMIENTO
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};
