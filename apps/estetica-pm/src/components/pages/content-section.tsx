'use client';
import { Image } from '@nextui-org/react';
import { Button, Link } from '@nextui-org/react';
import { IconWhatsapp } from '@nx-next/shared';
import { Variants, motion } from 'framer-motion';

type ImgSide = 'L' | 'R';

export type ContentSectionData = {
  backgroundFlip: boolean;
  imgSrc: string;
  imgSide: ImgSide;
  button: boolean;
  pageTitle: boolean;
  title: string;
  description: string[];
};

const sectionLeftVariants: Variants = {
  offscreen: {
    x: 100,
    opacity: 0,
  },
  onscreen: {
    x: 0,
    opacity: 1,

    transition: {
      type: 'spring',
      bounce: 0.5,
      duration: 1,
    },
  },
};
const sectionRightVariants: Variants = {
  offscreen: {
    x: -100,
    opacity: 0,
  },
  onscreen: {
    x: 0,
    opacity: 1,

    transition: {
      type: 'spring',
      bounce: 0.5,
      duration: 1,
    },
  },
};
const sectionTopVariants: Variants = {
  offscreen: {
    y: -100,
    opacity: 0,
  },
  onscreen: {
    y: 0,
    opacity: 1,

    transition: {
      type: 'spring',
      bounce: 0.5,
      duration: 1,
    },
  },
};
const sectionDownVariants: Variants = {
  offscreen: {
    y: 100,
    opacity: 0,
  },
  onscreen: {
    y: 0,
    opacity: 1,

    transition: {
      type: 'spring',
      bounce: 0.5,
      duration: 1,
    },
  },
};
export default function ContentSection({
  content,
}: {
  content: ContentSectionData;
}) {
  return (
    <section className="max-w-screen min-h-fit lg:min-h-[560px]">
      <BackgroundSVG flip={content.backgroundFlip} />
      <div className="w-full pt-8 mx-auto text-center lg:text-left xl:px-32">
        <div className="grid items-center lg:grid-cols-2">
          {content.imgSide === 'L' ? (
            <>
              <div className="hidden md:flex w-full justify-center">
                <Image
                  src={content.imgSrc}
                  className="w-full rounded-lg shadow-lg z-0 max-h-[520px]"
                  alt="image"
                  loading="lazy"
                />
              </div>
              <div className="md:hidden w-full flex justify-center">
                <Image
                  src={content.imgSrc}
                  className="w-full rounded-lg shadow-lg z-0 max-h-[520px]"
                  alt="image"
                  loading="lazy"
                />
              </div>
              <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-ml-14">
                {content.pageTitle ? (
                  <h2 className="mb-4 text-4xl sm:text-4xl md:mb-8 md:text-6xl uppercase">
                    {content.title}
                  </h2>
                ) : (
                  <h2 className="mb-4 md:mb-8 text-lg font-bold sm:text-2xl md:text-4xl uppercase">
                    {content.title}
                  </h2>
                )}

                {content.description.map((pharagraph, index) => (
                  <p key={index} className="w-full leading-relaxed xl:text-lg">
                    {pharagraph}
                  </p>
                ))}
                {content.button && (
                  <div className="mt-4 md:mt-8">
                    <Button
                      variant="faded"
                      startContent={<IconWhatsapp />}
                      as={Link}
                      isExternal
                      href="https://wa.me/+34696797483"
                    >
                      RESERVA TU TRATAMIENTO
                    </Button>
                  </div>
                )}
              </div>
            </>
          ) : (
            <>
              <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
                {content.pageTitle ? (
                  <h2 className="mb-4 text-4xl sm:text-4xl md:mb-8 md:text-6xl">
                    {content.title}
                  </h2>
                ) : (
                  <h2 className="mb-4 md:mb-8 text-lg font-bold sm:text-2xl  md:text-4xl">
                    {content.title}
                  </h2>
                )}

                {content.description.map((pharagraph, index) => (
                  <p key={index} className="w-full leading-relaxed xl:text-lg">
                    {pharagraph}
                  </p>
                ))}
                {content.button && (
                  <div className="mt-4 md:mt-8">
                    <Button
                      variant="faded"
                      startContent={<IconWhatsapp />}
                      as={Link}
                      isExternal
                      href="https://wa.me/+34696797483"
                    >
                      RESERVA TU TRATAMIENTO
                    </Button>
                  </div>
                )}
              </div>

              <div className="hidden md:flex w-full justify-center">
                <Image
                  src={content.imgSrc}
                  className="w-full rounded-lg shadow-lg z-0 max-h-[520px]"
                  alt="image"
                  loading="lazy"
                />
              </div>
              <div className="md:hidden w-full flex justify-center">
                <Image
                  src={content.imgSrc}
                  className="w-full rounded-lg shadow-lg z-0 max-h-[520px]"
                  alt="image"
                  loading="lazy"
                />
              </div>
            </>
          )}
        </div>
      </div>
    </section>
  );
}
export const BackgroundSVG = ({ flip }: { flip: boolean }) => {
  return (
    <span className="[&>svg]:absolute [&>svg]:-z-10 [&>svg]:hidden [&>svg]:h-[560px] [&>svg]:w-full [&>svg]:lg:block">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
        preserveAspectRatio="none"
        id="svg-theming-light"
        className="pointer-events-none absolute hidden w-full overflow-hidden transition-opacity duration-300 dark:opacity-0 lg:block"
        style={{
          transform: flip ? 'scaleY(-1) scaleX(-1)' : '',
        }}
      >
        <defs>
          <linearGradient
            id="gradient-svg-theming-light"
            x1="0"
            x2="0"
            y1="1"
            y2="0"
          >
            <stop stopColor="hsl(209, 92.2%, 100%)" offset="0%"></stop>
            <stop stopColor="hsl(209, 92.2%, 92.1%)" offset="50%"></stop>
            <stop stopColor="hsl(209, 92.2%, 98%)" offset="80%"></stop>
            <stop stopColor="hsl(209, 92.2%, 100%)" offset="100%"></stop>
          </linearGradient>
        </defs>
        <path
          fill="url(#gradient-svg-theming-light)"
          d="M 0.351 306.998 C 0.351 306.998 106.387 303.855 120.22 303.067 C 376.703 288.465 418.036 245.269 555.376 156.785 C 654.124 93.165 748.046 49.314 901.379 38.868 C 1053.87 28.479 1440.135 82.672 1440.135 82.672 L 1440 0 L 1360 0 C 1280 0 1120 0 960 0 C 800 0 640 0 480 0 C 320 0 160 0 80 0 L 0 0 L 0.351 306.998 Z"
        ></path>
      </svg>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
        preserveAspectRatio="none"
        id="svg-theming-dark"
        className="pointer-events-none absolute hidden w-full opacity-0 transition-opacity duration-300 dark:opacity-100 lg:block"
        style={{
          transform: flip ? 'scaleY(-1) scaleX(-1)' : '',
        }}
      >
        <defs>
          <linearGradient
            id="gradient-svg-theming-dark"
            x1="0"
            x2="0"
            y1="1"
            y2="0"
          >
            <stop stopColor="hsl(0, 0%, 24%)" offset="0%"></stop>
            <stop stopColor="hsl(0, 0%, 19%)" offset="50%"></stop>
            <stop stopColor="hsl(0, 0%, 17%)" offset="80%"></stop>
            <stop stopColor="hsl(0, 0%, 15%)" offset="100%"></stop>
          </linearGradient>
        </defs>
        <path
          fill="url(#gradient-svg-theming-dark)"
          d="M 0.351 306.998 C 0.351 306.998 106.387 303.855 120.22 303.067 C 376.703 288.465 418.036 245.269 555.376 156.785 C 654.124 93.165 748.046 49.314 901.379 38.868 C 1053.87 28.479 1440.135 82.672 1440.135 82.672 L 1440 0 L 1360 0 C 1280 0 1120 0 960 0 C 800 0 640 0 480 0 C 320 0 160 0 80 0 L 0 0 L 0.351 306.998 Z"
        ></path>
      </svg>
    </span>
  );
};
