import ContentSection, { ContentSectionData } from '../content-section';

export default function BodyPage() {
  const BodyContentPage: ContentSectionData[] = [
    {
      backgroundFlip: false,
      imgSrc: '/img/21.webp',
      button: true,
      imgSide: 'R',
      pageTitle: true,
      title: 'CORPORAL',
      description: [
        `El paso del tiempo, hábitos de vida poco saludables, estrés, sol…
    hace que nuestro organismo experimente cambios físicos
    considerables: celulitis, flacidez cutánea y muscular, sobrepeso…
    Nuestro objetivo es ofrecer un tratamiento integral para obtener
    el mejor resultado, por lo que es importante un control de peso,
    ya que si durante el tratamiento el peso corporal aumenta los
    resultados no serán los óptimos. Combinaremos los tratamientos
    estéticos no invasivos más avanzados: endermología, cavitación,
    presoterapia, radiofrecuencia, drenaje linfático, masaje reductor,
    reafirmante… Se pautará el tratamiento más indicado de acuerdo con
    el problema a tratar y se irá adaptando a medida que avanza el
    tratamiento para conseguir el objetivo deseado.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/23.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'RADIOFRECUENCIA RESISTIVA :',
      description: [
        ' Provoca reducción de la capa grasa, disminución de la celulitis y activa sistema circulatorio y linfático.',
      ],
    },

    {
      backgroundFlip: false,
      imgSrc: '/img/31.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'ONDAS ELECTROMAGNETICAS TESLA :',
      description: [
        `El único equipo capaz de desarrollar musculo y eliminar grasa
        localizada. Glúteos, abdomen y muslos, super eficaz.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/32.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'PRESOTERAPIA:',
      description: [
        `Problemas circulatorios, retención de líquidos y drenaje
        linfático.`,
      ],
    },

    {
      backgroundFlip: false,
      imgSrc: '/img/33.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'DRENAJE LINFÁTICO MANUAL:',
      description: [
        `Masaje manual que ayuda a mejorar la circulación sanguínea y
          linfática.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/52.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'ROLLACTIVE :',
      description: [
        `ROLLACTIVE es sin duda el
        perfecto aliado en los centros de estética.
        La perfecta unión entre el masaje con rodillo por microvibración
        compresiva y el masaje por rotación 360° de alta velocidad hacen el equilibrio perfecto para todos los problemas que nuestros clientes tienen : celulitis más rebelde, tonificar todos los tejidos blandos, eliminar los líquidos retenidos y la inflamación de los tejidos, remodelar y eliminar contracturas.`,
      ],
    },

    {
      backgroundFlip: false,
      imgSrc: '/img/35.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'Maderoterapia:',
      description: [
        `  Es una terapia natural que consiste en estimular el cuerpo
          mediante una técnica de masaje con utensilios de madera.
          Mediante movimientos y presión en puntos del cuerpo se busca una
          reacción de bienestar, reducir los niveles de estrés, y aliviar
          los dolores musculares y articulares.`,
      ],
    },
  ];

  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={BodyContentPage[0]} />
      <ContentSection content={BodyContentPage[1]} />
      <ContentSection content={BodyContentPage[2]} />
      <ContentSection content={BodyContentPage[3]} />
      <ContentSection content={BodyContentPage[4]} />
      <ContentSection content={BodyContentPage[5]} />
      <ContentSection content={BodyContentPage[6]} />
    </section>
  );
}
