import { Image } from '@nextui-org/react';
import { Button, Link } from '@nextui-org/react';
import { IconWhatsapp } from '@nx-next/shared';
import ContentSection, {
  BackgroundSVG,
  ContentSectionData,
} from '../content-section';

export default function DepilationLaserPage() {
  const DepilationContentPage: ContentSectionData[] = [
    {
      backgroundFlip: false,
      imgSrc: '/img/22.jpg',
      button: true,
      imgSide: 'R',
      pageTitle: true,
      title: 'DEPILACIÓN DIODO ALTA POTENCIA',
      description: [
        `Si te molesta el vello, hoy cambiará tu vida. Llega el nuevo
        Láser DIODO ALTA POTENCIA. Es la última innovación tecnológica
        para eliminar el vello.`,
        '+Eficaz +Rápido',
      ],
    },
  ];

  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={DepilationContentPage[0]} />
      <SecondSection />
      <LastSection />
    </section>
  );
}

// const FirstSection = () => {
//   return (
//     <section className="mb-20 min-h-screen shadow-lg">
//       <BackgroundSVG flip={false} />

//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 DEPILACIÓN DIODO ALTA POTENCIA
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg">
//                 Si te molesta el vello, hoy cambiará tu vida. Llega el nuevo
//                 Láser DIODO ALTA POTENCIA. Es la última innovación tecnológica
//                 para eliminar el vello.
//               </p>
//               <p>+Eficaz +Rápido </p>
//               <div className="mt-4 md:mt-8">
//                 <Button
//                   variant="faded"
//                   startContent={<IconWhatsapp />}
//                   as={Link}
//                   isExternal
//                   href="https://wa.me/+34696797483"
//                 >
//                   RESERVA TU TRATAMIENTO
//                 </Button>
//               </div>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/22.jpg"
//               className="w-wull rounded-lg shadow-lg z-0"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
const SecondSection = () => {
  return (
    <section className="pb-20 shadow-lg">
      <BackgroundSVG flip={true} />
      <div className="mx-auto max-w-screen-2xl px-4 py-8 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
          <div className="grid grid-cols-2 gap-4 md:grid-cols-1 lg:grid-cols-2">
            <Image
              alt="Student"
              src="/img/10.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full"
            />

            <Image
              alt="Student"
              src="/img/11.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full"
            />
          </div>
          <div className="p-8 md:p-12 lg:px-16 lg:py-24">
            <div className="mx-auto max-w-xl text-center">
              <p className="sm:mt-4 sm:block md:text-xl">
                Si estás buscando resultados permanentes en la eliminación del
                vello, una piel lisa y perfecta y olvidarte para siempre de la
                foliculitis, tu tratamiento ideal es el láser diodo alta
                potencia. A parte de la eliminación del bello corporal, tu piel
                lucirá mas lisa, fina e hidratada y sin marcas.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
const LastSection = () => {
  return (
    <section className="overflow-hidden bg-[url(/img/36.webp)] bg-cover bg-center bg-no-repeat shadow-lg">
      <div className="bg-black/20 p-8 md:p-12 lg:px-16 lg:py-24">
        <div className="text-center ltr:sm:text-left rtl:sm:text-right">
          <h2 className="text-2xl font-bold sm:text-3xl md:text-5xl">
            ¿QUIERES PROBARLO Y CONSTATAR POR TI MISM@ LA REVOLUCIÓN EN
            DEPILACIÓN?
          </h2>

          <div className="mt-4 md:mt-8">
            <Button
              variant="faded"
              startContent={<IconWhatsapp />}
              as={Link}
              isExternal
              href="https://wa.me/+34696797483"
            >
              RESERVA TU TRATAMIENTO
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};
