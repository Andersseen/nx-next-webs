import ContentSection, { ContentSectionData } from '../content-section';

export default function OtherPage() {
  const TanningContentPage: ContentSectionData[] = [
    {
      backgroundFlip: false,
      imgSrc: '/img/18.webp',
      button: true,
      imgSide: 'R',
      pageTitle: false,
      title: 'Laminado de cejas. Lifting y tinte de pestañas :',
      description: [
        `  Unas pestañas bonitas ayudan a enmarcar la mirada y hacen que
        los ojos parezcan más grandes y expresivos. El tinte de pestañas
        se aplica sobre las pestañas naturales para aportar color y
        luminosidad al ojo aumentando así la intensidad de la mirada. Es
        la solución perfecta si tienes las pestañas claras o si tus
        pestañas son escasas ya que les dará mayor espesor. La
        permanente de pestañas (moldeado) permite rizar la pestaña
        natural. La curvatura del rizo dependerá de la longitud de la
        pestaña y del efecto que deseemos conseguir. El resultado será
        una mirada mas abierta y unas pestañas bien peinadas y en forma
        de abanico.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/1.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'Henna, tinte de cejas y pestañas :',
      description: [
        `Conseguirás que tus cejas y pestañas parezcan más espesas, cubrirás las canas y harás
        que resalten, al darle un tono intenso.`,
        '(La henna se usa solo para las cejas)',
      ],
    },
    {
      backgroundFlip: false,
      imgSrc: '/img/14.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'Depilación con hilo :',
      description: [
        `  Se trata de un método natural porque solo se utiliza un hilo de
          algodón (o de seda) para arrancar de raíz el vello. El hilo
          elimina el vello con una precisión milimétrica, que no fatiga ni
          maltrata la piel del rostro. Al ser un método nada dañino ni
          erosivo, permite la extracción de líneas enteras de vello.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/9.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'Depilación con cera :',
      description: [
        `Las ceras depilatorias son un tratamiento que sigue utilizándose
          a pesar de la existencia de la fotodepilación y otros métodos
          también efectivos. Su función consiste en eliminar el vello, de
          diferentes partes del cuerpo, directamente desde la raíz.`,
      ],
    },
    {
      backgroundFlip: false,
      imgSrc: '/img/45.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'Maquillaje :',
      description: [
        ` MAQUILLAJE 100% VEGANO MÁXIMA CALIDAD Y PRACTICIDAD`,
        ` El maquillaje no solo transforma tu imagen, también tu estado de
         ánimo y seguridad en ti misma.`,
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/49.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'MICROPIGMENTACION :',
      description: [
        `La micropigmentación, conocida también como maquillaje permanente, es un tratamiento facial que pretende embellecer, rejuvenecer, equilibrar y armonizar las líneas maestras del rostro.`,
        '(eye-liner, contorno de labios)',
      ],
    },
    {
      backgroundFlip: false,
      imgSrc: '/img/51.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'MICROSHADING :',
      description: [
        `Es una técnica con efecto maquillaje para rellenar cejas escasas y claras a base de pequeños puntos oscuros de una apariencia similar a un degradado.`,
        '(cejas)',
      ],
    },
    {
      backgroundFlip: true,
      imgSrc: '/img/50.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'MICROBLANDING :',
      description: [
        `El microblading es una técnica de maquillaje semipermanente, cuyo objetivo es corregir o reconstruir completamente una ceja carente de pelo o ausente, realizando pelos de forma artística, creando un efecto hiperrealista y natural.`,
        '(cejas pelo a pelo)',
      ],
    },
  ];

  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={TanningContentPage[0]} />
      <ContentSection content={TanningContentPage[1]} />
      <ContentSection content={TanningContentPage[2]} />
      <ContentSection content={TanningContentPage[3]} />
      <ContentSection content={TanningContentPage[4]} />
      <ContentSection content={TanningContentPage[5]} />
      <ContentSection content={TanningContentPage[6]} />
      <ContentSection content={TanningContentPage[7]} />
    </section>
  );
}

// const FirstSection = () => {
//   return (
//     <section className="mb-20 min-h-screen shadow-lg">
//       <BackgroundSVG flip={false} />

//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 Lifting y tinte de pestañas
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg">
//                 Unas pestañas bonitas ayudan a enmarcar la mirada y hacen que
//                 los ojos parezcan más grandes y expresivos. El tinte de pestañas
//                 se aplica sobre las pestañas naturales para aportar color y
//                 luminosidad al ojo aumentando así la intensidad de la mirada. Es
//                 la solución perfecta si tienes las pestañas claras o si tus
//                 pestañas son escasas ya que les dará mayor espesor. La
//                 permanente de pestañas (moldeado) permite rizar la pestaña
//                 natural. La curvatura del rizo dependerá de la longitud de la
//                 pestaña y del efecto que deseemos conseguir. El resultado será
//                 una mirada mas abierta y unas pestañas bien peinadas y en forma
//                 de abanico.
//               </p>

//               <div className="mt-4 md:mt-8">
//                 <Button
//                   variant="faded"
//                   startContent={<IconWhatsapp />}
//                   as={Link}
//                   isExternal
//                   href="https://wa.me/+34696797483"
//                 >
//                   RESERVA TU TRATAMIENTO
//                 </Button>
//               </div>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/18.webp"
//               className="w-wull rounded-lg shadow-lg z-0"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const SecondSection = () => {
//   return (
//     <section>
//       <BackgroundSVG flip={true} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div>
//             <Image
//               src="/img/1.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 Tinte de cejas :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Conseguirás que parezcan más espesas, cubrirás las canas y harás
//                 que resalten, al darle un tono intenso.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const ThirdSection = () => {
//   return (
//     <section>
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 Depilación con hilo :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Se trata de un método natural porque solo se utiliza un hilo de
//                 algodón (o de seda) para arrancar de raíz el vello. El hilo
//                 elimina el vello con una precisión milimétrica, que no fatiga ni
//                 maltrata la piel del rostro. Al ser un método nada dañino ni
//                 erosivo, permite la extracción de líneas enteras de vello.
//               </p>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/14.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const FirthSection = () => {
//   return (
//     <section>
//       <BackgroundSVG flip={true} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div>
//             <Image
//               src="/img/9.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 Depilación con cera :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Las ceras depilatorias son un tratamiento que sigue utilizándose
//                 a pesar de la existencia de la fotodepilación y otros métodos
//                 también efectivos. Su función consiste en eliminar el vello, de
//                 diferentes partes del cuerpo, directamente desde la raíz.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const FiftSection = () => {
//   return (
//     <section className="shadow-lg">
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 Maquillaje :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 MAQUILLAJE 100% VEGANO MÁXIMA CALIDAD Y PRACTICIDAD
//               </p>
//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 El maquillaje no solo transforma tu imagen, también tu estado de
//                 ánimo y seguridad en ti misma.
//               </p>
//             </div>
//           </div>
//           <div>
//             <Image
//               src="/img/45.webp"
//               className="w-full rounded-lg shadow-lg h-[450px] z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
