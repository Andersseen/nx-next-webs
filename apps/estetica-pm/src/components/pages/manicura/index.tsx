import { Image } from '@nextui-org/react';
import { Button, Link } from '@nextui-org/react';
import { IconWhatsapp } from '@nx-next/shared';
import ContentSection, {
  ContentSectionData,
  BackgroundSVG,
} from '../content-section';

export default function ManicuraPage() {
  const ManicuraContentPage: ContentSectionData[] = [
    {
      backgroundFlip: false,
      imgSrc: '/img/20.webp',
      button: true,
      imgSide: 'R',
      pageTitle: true,
      title: 'PEDICURA - MANICURA',
      description: [
        `Es importante dedicar tiempo a las manos para mantenerlas sanas,
        hidratadas y con las cutículas y las uñas en buen estado. Unas
        manos cuidadas y bonitas son la mejor carta de presentación. De
        igual manera tener los pies libres de durezas y callosidades y
        con las uñas bien recortadas además de belleza le aportará salud
        y descanso.`,
      ],
    },
  ];

  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={ManicuraContentPage[0]} />
      <SecondSection />
      <LastSection />
    </section>
  );
}

// const FirstSection = () => {
//   return (
//     <section className="mb-20 min-h-screen shadow-lg">
//       <BackgroundSVG flip={false} />

//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 PEDICURA - MANICURA
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg">
//                 Es importante dedicar tiempo a las manos para mantenerlas sanas,
//                 hidratadas y con las cutículas y las uñas en buen estado. Unas
//                 manos cuidadas y bonitas son la mejor carta de presentación. De
//                 igual manera tener los pies libres de durezas y callosidades y
//                 con las uñas bien recortadas además de belleza le aportará salud
//                 y descanso.
//               </p>
//               <div className="mt-4 md:mt-8">
//                 <Button
//                   variant="faded"
//                   startContent={<IconWhatsapp />}
//                   as={Link}
//                   isExternal
//                   href="https://wa.me/+34696797483"
//                 >
//                   RESERVA TU TRATAMIENTO
//                 </Button>
//               </div>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/20.webp"
//               alt="BRONCEADO"
//               className="w-wull rounded-lg shadow-lg z-0"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
const SecondSection = () => {
  return (
    <section className="shadow-lg">
      <BackgroundSVG flip={true} />
      <div className="mx-auto max-w-screen-2xl px-4 py-8 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
          <div className="grid grid-cols-2 gap-4 md:grid-cols-1 lg:grid-cols-2">
            <Image
              alt="Student"
              src="/img/39.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full"
            />

            <Image
              alt="Student"
              src="/img/38.webp"
              className="h-40 w-full object-cover sm:h-56 md:h-full"
            />
          </div>
          <div className="p-8 md:p-12 lg:px-16 lg:py-24">
            <div className="mx-auto max-w-xl text-center">
              <h6>Algunos de los tratamientos más destacados son:</h6>
              <h5>Esmaltado permanente</h5>
              <p>
                Permanece hasta 2-3 semanas en perfectas condiciones en
                manicuras e incluso más en pedicuras.
              </p>

              <p>
                Su aplicación es un poco más larga que la manicura convencional
                ya que es necesario secar las distintas capas con lámpara de
                LED.
              </p>

              <p>
                Es aconsejable acudir a un centro de estética especializado para
                su retirada.
              </p>
              <p>
                Disponible en una gran variedad de colores y en manicura
                francesa.
              </p>
              <p>
                Ahorrarás tiempo y tus uñas lucirán siempre como recién
                pintadas.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
const LastSection = () => {
  return (
    <section className="overflow-hidden bg-[url(/img/37.webp)] bg-cover bg-center bg-no-repeat shadow-lg">
      <div className="bg-black/20 p-8 md:p-12 lg:px-16 lg:py-24">
        <div className="text-center ltr:sm:text-left rtl:sm:text-right">
          <div className="mt-8 md:mt-16">
            <Button
              variant="faded"
              startContent={<IconWhatsapp />}
              as={Link}
              isExternal
              href="https://wa.me/+34696797483"
            >
              RESERVA TU TRATAMIENTO
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};
