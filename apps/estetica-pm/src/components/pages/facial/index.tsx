import ContentSection, { ContentSectionData } from '../content-section';

export default function FacialPage() {
  const FacialContentPage: ContentSectionData[] = [
    // First Section
    {
      backgroundFlip: false,
      imgSrc: '/img/40.webp',
      button: true,
      imgSide: 'R',
      pageTitle: true,
      title: 'FACIAL',
      description: [
        `La piel es el principal órgano de belleza, revela nuestra edad,
        nuestra salud y hasta nuestro estado de ánimo. A ella se lo
        debemos todo. Nos protege de agentes nocivos, regula el
        intercambio molecular de oxígeno, agua, minerales… Por todo ello
        hay que saber compensarla y mantenerla con los tratamientos más
        adecuados. Nuestra filosofía se basa en un seguimiento
        personalizado, calidad y duración en nuestros tratamientos. Con
        el paso del tiempo es inevitable que se formen líneas y arrugas
        porque la piel pierde flexibilidad y elasticidad. Para impedir
        este deterioro disponemos de una amplia carta de tratamientos
        personalizados que previenen y combaten el envejecimiento de
        nuestra piel.`,
      ],
    },
    // Second Section
    {
      backgroundFlip: true,
      imgSrc: '/img/41.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'CARBOXITERAPIA Coolifting :',
      description: [
        `Proyecta sobre los tejidos faciales un potente flujo de CO2,a
        baja temperatura y alta presión combinado con una elevada
        concentración de activos(ácido hialuronico)`,
      ],
    },
    // third Section
    {
      backgroundFlip: false,
      imgSrc: '/img/2.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'OXIGENOTERAPIA :',
      description: [
        `Activación Celular Natural, aplicación de principios activos (ácido hialuronico, Vitaminas) mediante presión hiperbárica con oxígeno al 99,5% Reequilibrante`,
      ],
    },
    // fourth Section
    {
      backgroundFlip: true,
      imgSrc: '/img/42.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'RADIOFRECUENCIA :',
      description: [
        `Efecto Lifting intensivo, combatiendo los radicales libres y devolviendo su energía a los tejidos. Favorece la nutrición y la formación de colágeno y elastina, prevención del envejecimiento.`,
      ],
    },
    // fifth Section
    {
      backgroundFlip: false,
      imgSrc: '/img/8.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'MULTIVITAMINAS (MESOBOOST) : ',
      description: [
        `Electroporación transdermal meso-chip system. Abre decenas de miles de microcanales en la epidermis para introducir en la dermis activos de manera eficiente, rápida y sin dolor.`,
        '· BENEFICIOS PARA LA PIEL:',
        '- Acelera el metabolismo de recuperación cutánea. Mejora la textura, firmeza y calidad de la piel.',
        ' - Efecto de rejuvenecimiento global.',

        ' - Efecto de rejuvenecimiento global.',

        ' EN TAN SOLO 3 SESIONES 100% PIEL MAS JOVEN, MAS FIRME Y ARRUGAS MENOS VISIBLES',
      ],
    },
    // 7 Section
    {
      backgroundFlip: true,
      imgSrc: '/img/16.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'Peeling Carbón Hollywood :',
      description: [
        `Beneficios del peeling de carbón
        Primero realiza una exfolicación de la capa córnea de la piel. Tiene un efecto bactericida por lo que es muy recomendando en acné y pieles grasas, reduce el sebo y la secreción grasa. Cierra el poro consiguiendo un efecto de alisado, corrigiendo las imperfecciones superficiales de la piel.
        Aclara y unifica el tono de la piel.
        Estimula la producción de colágeno y elastina dando un efecto tensor y
        lifting.
        Los efectos suelen notarse desde la primera sesión, aunque para un tratamiento duradero se recomiendan entre 4-6 sesiones. Se pueden realizar cada 15 días o mensualmente. El protocolo irá en función al tipo de piel, estado de la misma y otros factores que evaluará el servicio médico para tu caso concreto.`,
      ],
    },
    {
      backgroundFlip: false,
      imgSrc: '/img/43.webp',
      button: false,
      imgSide: 'R',
      pageTitle: false,
      title: 'ELIMINACIÓN HIPERPIGMENTACIONES ESTÉTICAS: ',
      description: [
        'REDUCE TUS MANCHAS EN 2-4 SESIONES + TRATAMIENTO EN CASA',

        'LENTIGOS O MANCHAS SENILES',

        'Exposición a la luz del sol',

        ' · Son las zonas más comunes.',

        '· Aparecen en las zonas más expuestas a los rayos UV.',

        '· Se acentúan con el paso de los años.',

        'Factores endocrinos u hormonales v Más frecuentes en mujeres.',

        ' · Más frecuentes en mujeres.',

        '· Aparecen durante y después del embarazo.',

        ' · También con tratamientos con anticonceptivos hormonales.',

        ' HIPERPIGMENTACIÓN POST-INFLAMATORIA',

        '  Procesos inflamatorioss.',

        '· Suele afectar a pieles con tendencia acnéica.',

        '· Aparecen tras una inflamación o una lesión de la piel.',

        '· Se agrava con la exposición al sol.',
      ],
    },
    // 8 Section
    {
      backgroundFlip: true,
      imgSrc: '/img/3.webp',
      button: false,
      imgSide: 'L',
      pageTitle: false,
      title: 'MICROEXFOLIACIÓN  :',
      description: [
        `Exfoliación con punta de diamante ,controlada y progresiva de la capa córnea de la piel. Se retira las células muertas, prepara la piel y facilita el paso de sustancias al interior de la dermis`,
      ],
    },
  ];
  return (
    <section className="h-fit shadow-lg">
      <ContentSection content={FacialContentPage[0]} />
      <ContentSection content={FacialContentPage[1]} />
      <ContentSection content={FacialContentPage[2]} />
      <ContentSection content={FacialContentPage[3]} />
      <ContentSection content={FacialContentPage[4]} />
      <ContentSection content={FacialContentPage[5]} />
      <ContentSection content={FacialContentPage[6]} />
      <ContentSection content={FacialContentPage[7]} />
    </section>
  );
}

// const FirstSection = () => {
//   return (
//     <section className="mb-20 min-h-screen shadow-lg">
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl  sm:text-4xl md:mb-8 md:text-8xl">
//                 FACIAL
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg">
//                 La piel es el principal órgano de belleza, revela nuestra edad,
//                 nuestra salud y hasta nuestro estado de ánimo. A ella se lo
//                 debemos todo. Nos protege de agentes nocivos, regula el
//                 intercambio molecular de oxígeno, agua, minerales… Por todo ello
//                 hay que saber compensarla y mantenerla con los tratamientos más
//                 adecuados. Nuestra filosofía se basa en un seguimiento
//                 personalizado, calidad y duración en nuestros tratamientos. Con
//                 el paso del tiempo es inevitable que se formen líneas y arrugas
//                 porque la piel pierde flexibilidad y elasticidad. Para impedir
//                 este deterioro disponemos de una amplia carta de tratamientos
//                 personalizados que previenen y combaten el envejecimiento de
//                 nuestra piel.
//               </p>
//               <div className="mt-4 md:mt-8">
//                 <Button
//                   variant="faded"
//                   startContent={<IconWhatsapp />}
//                   as={Link}
//                   isExternal
//                   href="https://wa.me/+34696797483"
//                 >
//                   RESERVA TU TRATAMIENTO
//                 </Button>
//               </div>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/40.webp"
//               className="w-full rounded-lg shadow-lg z-0"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const SecondSection = () => {
//   return (
//     <div className="container my-24 mx-auto md:px-6">
//       <section className="mb-32">
//         <div className="flex flex-wrap">
//           <div className="mb-12 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-5/12">
//             <div className="flex lg:py-12">
//               <Image
//                 src="/img/41.webp"
//                 alt="image"
//                 className="w-wull rounded-lg shadow-lg dark:shadow-black/20 lg:ml-[50px] z-[10]"
//               />
//             </div>
//           </div>
//           <div className="w-full shrink-0 grow-0 basis-auto lg:w-7/12">
//             <div className="flex h-full items-center rounded-lg shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px] p-6 text-center  lg:pl-12 lg:text-left">
//               <div className="lg:pl-12">
//                 <h2 className="mb-6 text-3xl font-bold">
//                   CARBOXITERAPIA Coolifting :
//                 </h2>

//                 <p>
//                   Proyecta sobre los tejidos faciales un potente flujo de CO2,a
//                   baja temperatura y alta presión combinado con una elevada
//                   concentración de activos(ácido hialuronico)
//                 </p>
//               </div>
//             </div>
//           </div>
//         </div>
//       </section>
//     </div>
//   );
// };
// const ThirdSection = () => {
//   return (
//     <section className="">
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 OXIGENOTERAPIA:
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Activación Celular Natural, aplicación de principios activos
//                 (ácido hialuronico, Vitaminas) mediante presión hiperbárica con
//                 oxígeno al 99,5% Reequilibrante
//               </p>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/2.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const FirthSection = () => {
//   return (
//     <section className="">
//       <BackgroundSVG flip={true} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div>
//             <Image
//               src="/img/42.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 RADIOFRECUENCIA :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Efecto Lifting intensivo, combatiendo los radicales libres y
//                 devolviendo su energía a los tejidos. Favorece la nutrición y la
//                 formación de colágeno y elastina, prevención del envejecimiento.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const FifthSection = () => {
//   return (
//     <section className="">
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 MULTIVITAMINAS (MESOBOOST)
//               </h2>
//               <p className="w-full leading-relaxed xl:text-lg pr-8">
//                 Electroporación transdermal meso-chip system. Abre decenas de
//                 miles de microcanales en la epidermis para introducir en la
//                 dermis activos de manera eficiente, rápida y sin dolor.
//               </p>
//               <p className="w-full leading-relaxed xl:text-lg pr-8">
//                 · BENEFICIOS PARA LA PIEL:
//               </p>
//               <p>
//                 - Acelera el metabolismo de recuperación cutánea. Mejora la
//                 textura, firmeza y calidad de la piel.
//               </p>
//               <p className="w-full leading-relaxed xl:text-lg pr-8">
//                 - Efecto de rejuvenecimiento global.
//               </p>
//               <p className="w-full leading-relaxed xl:text-lg pr-8">
//                 EN TAN SOLO 3 SESIONES 100% PIEL MAS JOVEN, MAS FIRME Y ARRUGAS
//                 MENOS VISIBLES.
//               </p>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/8.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const SixSection = () => {
//   return (
//     <section className="">
//       <BackgroundSVG flip={true} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div>
//             <Image
//               src="/img/16.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 HYDRALURONIC :
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Tratamiento de Hidratación y relleno de arrugas con ACIDO
//                 HIALURONICO de 3 pesos moleculares. -Terapia facial que aporta
//                 hidratación profunda y mejora la elasticidad, firmeza, arrugas y
//                 densidad cutánea.
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };
// const SevenSection = () => {
//   return (
//     <section className="shadow-lg pb-4">
//       <BackgroundSVG flip={false} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 PIGMENT THERAPY
//               </h2>
//               <h3 className="mb-4 font-bold sm:text-xl md:mb-8 md:text-2xl">
//                 GERMAINE DE CAPUCCINI EXPERT LAB
//               </h3>

//               <p className="w-full font-semibold xl:text-lg pr-12 mb-4">
//                 REDUCE TUS MANCHAS EN 2-4 SESIONES + TRATAMIENTOS EN CASA
//               </p>
//               <p className="w-full leading-relaxed xl:text-lg">
//                 LENTIGOS O MANCHAS SENILES
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 Exposición a la luz del sol
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Son las zonas más comunes.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Aparecen en las zonas más expuestas a los rayos UV.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Se acentúan con el paso de los años.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">MELASMA</p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 Factores endocrinos u hormonales v Más frecuentes en mujeres.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Más frecuentes en mujeres.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Aparecen durante y después del embarazo.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · También con tratamientos con anticonceptivos hormonales.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 HIPERPIGMENTACIÓN POST-INFLAMATORIA
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 Procesos inflamatorioss.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Suele afectar a pieles con tendencia acnéica.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Aparecen tras una inflamación o una lesión de la piel.
//               </p>
//               <p className="w-full leading-relaxed xl:text-md">
//                 · Se agrava con la exposición al sol.
//               </p>
//             </div>
//           </div>

//           <div>
//             <Image
//               src="/img/43.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };

// const EightSection = () => {
//   return (
//     <section className="">
//       <BackgroundSVG flip={true} />
//       <div className="container pt-24 mx-auto text-center lg:text-left xl:px-32">
//         <div className="flex grid items-center lg:grid-cols-2">
//           <div>
//             <Image
//               src="/img/3.webp"
//               className="w-full rounded-lg shadow-lg z-10"
//               alt="image"
//               loading="lazy"
//             />
//           </div>
//           <div className="mb-12 lg:mb-0">
//             <div className="relative z-[1] block rounded-lg px-6 py-12 shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] backdrop-blur-[30px]  md:px-12 lg:-mr-14">
//               <h2 className="mb-4 text-4xl font-bold sm:text-2xl md:mb-8 md:text-4xl">
//                 MICROEXFOLIACIÓN:
//               </h2>

//               <p className="w-full leading-relaxed xl:text-lg pr-4">
//                 Exfoliación con punta de diamante ,controlada y progresiva de la
//                 capa córnea de la piel. Se retira las células muertas, prepara
//                 la piel y facilita el paso de sustancias al interior de la
//                 dermis
//               </p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };

// const BackgroundSVG = ({ flip }: { flip: boolean }) => {
//   return (
//     <span className="[&>svg]:absolute [&>svg]:-z-10 [&>svg]:hidden [&>svg]:h-[560px] [&>svg]:w-full [&>svg]:lg:block">
//       <svg
//         xmlns="http://www.w3.org/2000/svg"
//         viewBox="0 0 1440 320"
//         preserveAspectRatio="none"
//         id="svg-theming-light"
//         className="pointer-events-none absolute hidden w-full overflow-hidden transition-opacity duration-300 dark:opacity-0 lg:block"
//         style={{
//           transform: flip ? 'scaleY(-1) scaleX(-1)' : '',
//         }}
//       >
//         <defs>
//           <linearGradient
//             id="gradient-svg-theming-light"
//             x1="0"
//             x2="0"
//             y1="1"
//             y2="0"
//           >
//             <stop stopColor="hsl(209, 92.2%, 100%)" offset="0%"></stop>
//             <stop stopColor="hsl(209, 92.2%, 92.1%)" offset="50%"></stop>
//             <stop stopColor="hsl(209, 92.2%, 98%)" offset="80%"></stop>
//             <stop stopColor="hsl(209, 92.2%, 100%)" offset="100%"></stop>
//           </linearGradient>
//         </defs>
//         <path
//           fill="url(#gradient-svg-theming-light)"
//           d="M 0.351 306.998 C 0.351 306.998 106.387 303.855 120.22 303.067 C 376.703 288.465 418.036 245.269 555.376 156.785 C 654.124 93.165 748.046 49.314 901.379 38.868 C 1053.87 28.479 1440.135 82.672 1440.135 82.672 L 1440 0 L 1360 0 C 1280 0 1120 0 960 0 C 800 0 640 0 480 0 C 320 0 160 0 80 0 L 0 0 L 0.351 306.998 Z"
//         ></path>
//       </svg>
//       <svg
//         xmlns="http://www.w3.org/2000/svg"
//         viewBox="0 0 1440 320"
//         preserveAspectRatio="none"
//         id="svg-theming-dark"
//         className="pointer-events-none absolute hidden w-full opacity-0 transition-opacity duration-300 dark:opacity-100 lg:block"
//         style={{
//           transform: flip ? 'scaleY(-1) scaleX(-1)' : '',
//         }}
//       >
//         <defs>
//           <linearGradient
//             id="gradient-svg-theming-dark"
//             x1="0"
//             x2="0"
//             y1="1"
//             y2="0"
//           >
//             <stop stopColor="hsl(0, 0%, 24%)" offset="0%"></stop>
//             <stop stopColor="hsl(0, 0%, 19%)" offset="50%"></stop>
//             <stop stopColor="hsl(0, 0%, 17%)" offset="80%"></stop>
//             <stop stopColor="hsl(0, 0%, 15%)" offset="100%"></stop>
//           </linearGradient>
//         </defs>
//         <path
//           fill="url(#gradient-svg-theming-dark)"
//           d="M 0.351 306.998 C 0.351 306.998 106.387 303.855 120.22 303.067 C 376.703 288.465 418.036 245.269 555.376 156.785 C 654.124 93.165 748.046 49.314 901.379 38.868 C 1053.87 28.479 1440.135 82.672 1440.135 82.672 L 1440 0 L 1360 0 C 1280 0 1120 0 960 0 C 800 0 640 0 480 0 C 320 0 160 0 80 0 L 0 0 L 0.351 306.998 Z"
//         ></path>
//       </svg>
//     </span>
//   );
// };
