'use client';
import React from 'react';
import { NavigationLink } from '@nx-next/shared';
import { useIsVisible } from './use-is-visible';
import { FooterSection } from './footer';
export const FooterWrapper = ({ navLinks }: { navLinks: NavigationLink[] }) => {
  const ref = React.useRef();
  const isVisible = useIsVisible(ref);

  const title = 'Estetica Paloma Molero';
  const [footerHeight, setFooterHeight] = React.useState(0);

  const handleFooterHeight = (height) => {
    setFooterHeight(height);
  };

  return (
    <>
      <div
        ref={ref}
        style={{ height: footerHeight }}
        className="bg-transparent"
      ></div>

      <FooterSection
        footerHeight={handleFooterHeight}
        title={title}
        isVisible={isVisible}
        pages={navLinks}
      />
    </>
  );
};
