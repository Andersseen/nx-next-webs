'use client';
import React from 'react';
import { Canvas, useFrame } from '@react-three/fiber';
import { Environment, ContactShadows, OrbitControls } from '@react-three/drei';
import { useSpring } from '@react-spring/core';
import { a as three } from '@react-spring/three';
import { a as web } from '@react-spring/web';
import { ModelOne } from './models';
import { Vector3 } from 'three';

export default function LaptopOpenClose() {
  // This flag controls open state, alternates between true & false
  const [open, setOpen] = React.useState(false);
  const [position, setPosition] = React.useState<Vector3>(
    new Vector3(0, 0, -30)
  );

  const handleButtonClick = () => {
    setOpen(false);
    setPosition(new Vector3(0, 0, -30));
  };

  const openLaptop = (e) => {
    e.stopPropagation();
    if (e.intersections.length > 3) {
      setOpen(true);
      setPosition(new Vector3(-5, 0, -15));
    }
  };

  // We turn this into a spring animation that interpolates between 0 and 1
  const props = useSpring({ open: Number(open) });

  return (
    <web.main
      className="bg-[#12071f] h-screen w-full"
      style={{ background: props.open.to([0, 1], ['#f0f0f0', '#f0f0f0']) }}
    >
      <web.h1
        className="text-black"
        style={{
          opacity: props.open.to([0, 1], [1, 0]),
          transform: props.open.to(
            (o) => `translate3d(-50%,${o * 50 - 100}px,0)`
          ),
        }}
      >
        click
      </web.h1>
      <Canvas dpr={[1, 2]} camera={{ fov: 50 }}>
        <CameraRig position={position} />
        <three.pointLight
          position={[10, 10, 10]}
          intensity={1.5}
          color={props.open.to([0, 1], ['#f0f0f0', '#f0f0f0'])}
        />
        <React.Suspense fallback={null}>
          <group rotation={[0, Math.PI, 0]} onClick={(e) => openLaptop(e)}>
            <ModelOne
              open={open}
              hinge={props.open.to([0, 1], [1.575, -0.425])}
              onButtonClick={handleButtonClick}
            />
          </group>
          <Environment preset="city" />
        </React.Suspense>
        <ContactShadows position={[0, -4.5, 0]} scale={20} blur={2} far={4.5} />
        <OrbitControls
          enablePan={false}
          enableZoom={false}
          minPolarAngle={Math.PI / 2.2}
          maxPolarAngle={Math.PI / 2.2}
        />
      </Canvas>
    </web.main>
  );
}
function CameraRig({ position: [x, y, z] }) {
  useFrame((state) => {
    state.camera.position.lerp({ x, y, z }, 0.1);
  });
}
