'use client';
import React, { Suspense } from 'react';
import { Canvas, useFrame } from '@react-three/fiber';
import { Points, PointMaterial, Sparkles } from '@react-three/drei';
import * as random from 'maath/random/dist/maath-random.esm';
import { Hero } from './text';

export default function BackgroundStars() {
  return (
    <Canvas
      className="bg-[#12071f] w-full h-screen font-sans"
      camera={{ position: [0, 0, -1], fov: 60 }}
    >
      <Suspense fallback={'Loading'}>
        <Stars
          radius={100}
          depth={100}
          count={4000}
          factor={4}
          saturation={0}
          fade
          speed={0.2}
        />
        <Sparkles
          count={300}
          size={3}
          speed={0.02}
          opacity={1}
          scale={10}
          color="#fff3b0"
        />
        <Hero />
      </Suspense>
      <ambientLight intensity={0.6} color={'#e9ddf1'} />
    </Canvas>
  );
}

function Overlay() {
  return (
    <div className="absolute top-0 left-0 pointer-events-none w-full h-full">
      <div className="absolute top-1/2 left-1/2 w-full transform -translate-x-1/2 -translate-y-1/2 text-center">
        <h1 className="m-0 p-0 text-3xl sm:text-9xl font-semibold tracking-wide bg-gradient-to-r from-[#996dff] to-[#50208f] text-transparent bg-clip-text">
          Andersseen Dev
        </h1>
      </div>
    </div>
  );
}

function Stars(props) {
  const ref = React.useRef();
  const [sphere] = React.useState(() =>
    random.inSphere(new Float32Array(5000), { radius: 1.5 })
  );
  useFrame((state, delta) => {
    ref.current.rotation.x -= delta / 10;
    ref.current.rotation.y -= delta / 15;
  });
  return (
    <group rotation={[0, 0, Math.PI / 4]}>
      <Points
        ref={ref}
        positions={sphere}
        stride={3}
        frustumCulled={false}
        {...props}
      >
        <PointMaterial
          transparent
          color="#ffa0e0"
          size={0.005}
          sizeAttenuation={true}
          depthWrite={false}
        />
      </Points>
    </group>
  );
}
