'use client';
import { useRef } from 'react';
import { Text3D, Center, Float, useMatcapTexture } from '@react-three/drei';
import { useThree } from '@react-three/fiber';
import { Physics } from '@react-three/cannon';

export function Hero() {
  const [matcapTexture] = useMatcapTexture('CB4E88_F99AD6_F384C3_ED75B9');
  const ref = useRef();

  const { width: w, height: h } = useThree((state) => state.viewport);

  return (
    <>
      <Center scale={[0.2, 0.2, 0.2]}>
        <Physics gravity={5}>
          <Float speed={1}>
            <Text3D
              position={[0, 0, 1]}
              scale={[-0.5, 0.5, 0.5]}
              ref={ref}
              size={w / 3}
              maxWidth={[-w / 3, -h * 1, 3]}
              font={'/gt.json'}
              curveSegments={24}
              brevelSegments={1}
              bevelEnabled
              bevelSize={0.08}
              bevelThickness={0.03}
              height={1}
              lineHeight={0.9}
              letterSpacing={0.3}
            >
              {`Andersseen Dev`}
              <meshMatcapMaterial color="#9157b9" matcap={matcapTexture} />
            </Text3D>
          </Float>
        </Physics>
      </Center>
    </>
  );
}

// export default function App() {
//   return (
//     <div className="scene">
//       <Canvas camera={{ position: [0, 0, -10], fov: 60 }}>
//         <OrbitControls
//           enableZoom={true}
//           autoRotate={true}
//           autoRotateSpeed={-0.1}
//           enablePan={true}
//           azimuth={[-Math.PI / 4, Math.PI / 4]}
//           zoomSpeed={0.15}
//           dampingFactor={0.05}
//         />

//         <Suspense fallback={'Loading'}>
//           <Stars
//             radius={100}
//             depth={100}
//             count={4000}
//             factor={4}
//             saturation={0}
//             fade
//             speed={0.2}
//           />
//           <Sparkles
//             count={300}
//             size={3}
//             speed={0.02}
//             opacity={1}
//             scale={10}
//             color="#fff3b0"
//           />
//           <Hero />
//         </Suspense>
//         <ambientLight intensity={0.6} color={'#dee2ff'} />
//       </Canvas>
//     </div>
//   );
// }
