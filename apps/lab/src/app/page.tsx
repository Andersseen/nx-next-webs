import BackgroundBuble from '../components/background-buble';
import ParticleRing from '../components/background-circle-3d';
import BackgroundStars from '../components/background-stars';
import CanNavigation from '../components/laptop/can-navigation';
import LaptopOpenClose from '../components/laptop/open-close';
import styles from './page.module.scss';

export default function Index() {
  return (
    <>
      {/* <ParticleRing /> */}
      <BackgroundStars />
      {/* <BackgroundBuble /> */}
      {/* <BackgroundBuble /> */}
      {/* <LaptopOpenClose /> */}
      {/* <CanNavigation /> */}
    </>
  );
}
