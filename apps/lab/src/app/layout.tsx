import './global.css';

export const metadata = {
  title: 'Welcome to Andersseen Dev',
  description: 'By Andersseen',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
