'use client';
import React, { useState } from 'react';
import { SliderItem } from '@soul-alegria/data/slides';
import SliderCard from './slider-card';
import Link from 'next/link';
import { motion } from 'framer-motion';

type Props = {
  data: SliderItem[];
};

function Slides({ data }: Props) {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => setIsHovered(true);
  const handleMouseLeave = () => setIsHovered(false);

  return (
    <div
      className="flex w-full gap-6"
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      {data.map((data, index) => {
        return (
          <Link key={`${data.id}-${index}`} href={data.route}>
            <motion.div
              initial={{ filter: 'none' }}
              animate={{
                filter: isHovered ? 'blur(2px)' : 'none',
              }}
              transition={{ type: 'spring', duration: 0.3 }}
              whileHover={{ filter: 'none', scale: 1.1 }}
            >
              <SliderCard key={data.img} data={data} />
            </motion.div>
          </Link>
        );
      })}
    </div>
  );
}

export default Slides;
