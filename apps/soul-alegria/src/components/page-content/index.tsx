'use client';
import {
  HeaderData,
  HeaderSection,
  TextContent,
  TextContentData,
} from '@nx-next/shared';
import { useTheme } from 'next-themes';
import React from 'react';

export const PageContent = ({
  headerData,
  textContentData,
}: {
  headerData: HeaderData | null;
  textContentData: TextContentData;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      {headerData ? (
        <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      ) : (
        <></>
      )}
      <TextContent textContentData={textContentData} />
    </>
  );
};

export const PageWithHeader = ({
  headerData,
  children,
}: {
  headerData: HeaderData;
  children: React.ReactNode;
}) => {
  const { resolvedTheme } = useTheme();

  return (
    <>
      <HeaderSection headerData={headerData} resolvedTheme={resolvedTheme} />
      {children}
    </>
  );
};
