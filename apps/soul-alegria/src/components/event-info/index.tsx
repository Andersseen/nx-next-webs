import React from 'react';

const EventInfo: React.FC = () => {
  return (
    <section className="px-6 py-12 sm:px-12 lg:px-24">
      <h2 className="text-3xl font-semibold text-center  mb-8">
        EL MEJOR EVENTO ADAPTADO A TU PRESUPUESTO
      </h2>
      <p className="text-lg leading-relaxed mb-8 text-center max-w-2xl mx-auto">
        Ayudamos a las empresas en la organización de sus actos haciéndolos
        especiales, únicos, creativos e impactando en sus públicos para
        conseguir el objetivo deseado.
      </p>

      <div className="space-y-6">
        <h3 className="text-2xl font-semibold">Nos encargamos de:</h3>
        <ul className="list-disc list-inside space-y-2">
          <li>La creación de la idea.</li>
          <li>Selección de lugar y detalles.</li>
          <li>Contratación de proveedores.</li>
          <li>Organización de viajes.</li>
          <li>
            Creación y coordinación del programa, actividades, cenas, eventos
            flamencos, música, conciertos, invitados VIPs, etc.
          </li>
        </ul>

        <p className="text-lg leading-relaxed mt-6">
          Gracias a nuestro network, conseguimos sitios especiales con
          descuentos. Además de famosos e instituciones para que el evento, si
          se desea, tenga repercusión en medios y hacerlo así más visual y
          multiplicar su impacto.
        </p>

        <h3 className="text-2xl font-semibold mt-8">
          Algunos de nuestros tops:
        </h3>
        <ul className="list-disc list-inside space-y-2">
          <li>
            Evento exclusivo para directivos en el Palacio de Liria, con cena en
            sus bodegas.
          </li>
          <li>
            Eventos para directivos con actividades, cena en restaurantes con
            estrellas Michelin, showcooking, recorridos por Madrid, etc.
          </li>
          <li>Visitas privadas en museos con cenas en sus salones.</li>
          <li>
            Eventos sociales a través de fundaciones con destino a causas
            sociales.
          </li>
        </ul>
      </div>
    </section>
  );
};

export default EventInfo;
