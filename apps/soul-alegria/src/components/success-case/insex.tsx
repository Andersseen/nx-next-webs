const SuccessCase: React.FC = () => {
  return (
    <section className="px-6 max-w-3xl mx-auto">
      <h2 className="text-3xl font-semibold text-center mb-8">CASO DE ÉXITO</h2>

      <p className="text-lg leading-relaxed mb-8 text-center">
        Soul & Alegria ha creado un proyecto de signoguía con subtitulado
        dirigido a los hipoacúsicos (personas con Pérdida Auditiva), con el
        mecenazgo de BANKINTER para el MUSEO NACIONAL THYSSEN BORNAMISZA.
      </p>

      <div className="space-y-4 text-center">
        <p className="text-lg leading-relaxed">
          Creamos proyectos sociales dentro de tu plan de sostenibilidad y con
          su correspondiente desgrabación fiscal.
        </p>
      </div>
    </section>
  );
};

export default SuccessCase;
