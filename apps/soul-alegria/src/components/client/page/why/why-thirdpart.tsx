import { SVGProps } from 'react';

export type WhatBenefits = {
  title: string;
  benefits: string[];
  howPart: 'right' | 'left';
};

type WhyThirdpartProps = {
  benefits: WhatBenefits;
};

export const WhyThirdpart = ({ benefits }: WhyThirdpartProps) => {
  return (
    <section>
      <div className="max-w-screen-xl px-4 py-8 sm:px-6 sm:py-12 lg:px-8 lg:py-16">
        <div className="grid grid-cols-1 gap-y-8 lg:grid-cols-3 lg:items-center lg:gap-x-16">
          <div
            className={
              benefits.howPart === 'right'
                ? 'block lg:hidden mx-auto text-center'
                : 'mx-auto text-center'
            }
          >
            <h2 className="text-3xl font-bold sm:text-4xl">{benefits.title}</h2>
          </div>

          <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 sm:col-span-2">
            {benefits.benefits.map((item: string, index: number) => (
              <span
                key={index}
                className="block rounded-xl border border-[var(--foreground-color)] p-4 shadow-sm hover:border-main-400 focus:outline-none focus:ring"
              >
                <span className="inline-block rounded-lg p-3">
                  <WhatIcon index={index} />
                </span>

                <p className="sm:mt-1 sm:text-sm">{item}</p>
              </span>
            ))}
          </div>
          {benefits.howPart === 'right' && (
            <div className="hidden lg:block mx-auto text-center">
              <h2 className="text-3xl font-bold sm:text-4xl">
                {benefits.title}
              </h2>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export function WhatIcon({ index }: { index: number }) {
  if (index === 0) {
    return <Vs1Square />;
  }
  if (index === 1) {
    return <Vs2Square />;
  }
  if (index === 2) {
    return <Vs3Square />;
  }
  if (index === 3) {
    return <Vs4Square />;
  }
  return <TablerNumber />;
}

export function TablerNumber(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        fill="none"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M4 17V7l7 10V7m4 10h5m-5-7a2.5 3 0 1 0 5 0a2.5 3 0 1 0-5 0"
      />
    </svg>
  );
}

export function Vs1Square(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 1792 1792"
      {...props}
    >
      <path
        fill="currentColor"
        d="M336 0h1120q139 0 237.5 98.5T1792 336v1120q0 139-98.5 237.5T1456 1792H336q-139 0-237.5-98.5T0 1456V336Q0 197 98.5 98.5T336 0zm709 387q0-48-17-68t-62-20H747q-19 48-76.5 110.5T568 488q-18 6-33.5 30T519 563v98q0 30 22 52t53 22q20 0 38.5-5t30.5-10.5t27-19t20.5-19t20-23.5t16.5-20v557H591q-30 0-52 21.5t-22 52.5v149q0 31 22 53t52 22h609q31 0 53-22t22-53v-149q0-31-22-52.5t-53-21.5h-155V387z"
      />
    </svg>
  );
}

export function Vs2Square(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 1792 1792"
      {...props}
    >
      <path
        fill="currentColor"
        d="M1455 0H336Q197 0 98.5 98.5T0 336v1119q0 139 98 237.5t238 98.5h1119q139 0 237.5-98.5T1791 1455V336q0-139-98.5-237.5T1455 0zM346 1437v-11q0-70 59.5-155.5T557 1108t200.5-144T967 857q86-35 126.5-71t41.5-80q1-67-77-119t-157-52q-46 0-104 25t-107 66.5t-68 84.5q-3 6-6.5 14.5T611 736t-3.5 6t-3.5 4t-5 1t-7 0t-10.5-1H404q-30 0-48-26t-10-51q55-168 212-269t354-101q87 0 173.5 26t156.5 73t113.5 119.5T1399 674q0 81-24 145t-65.5 106.5t-92 75.5t-109 60t-112 52.5t-106.5 61t-86 76.5h595q19 0 33.5 16.5t14.5 39.5v130q0 23-14.5 39.5T1399 1493H394q-19 0-33.5-16.5T346 1437z"
      />
    </svg>
  );
}

export function Vs3Square(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 1792 1792"
      {...props}
    >
      <path
        fill="currentColor"
        d="M1456 0H336Q197 0 98.5 99T0 337v1119q0 139 98.5 237.5T336 1792h1120q139 0 237.5-98.5T1792 1456V337q0-139-98.5-238T1456 0zM906 514q0-1-12.5-1.5t-32.5 1t-43.5 5t-47.5 13t-44 23t-32.5 36T681 642q0 12-8.5 18t-15.5 6H434q-11 0-17.5-16t-6.5-28q3-71 32.5-127t75-92t107-60t126-34T885 299q77 0 157.5 16.5T1202 366t129.5 97.5T1382 611q0 182-195 285q195 103 195 285q0 84-50.5 147.5T1202 1426t-159.5 50.5T885 1493q-70 0-134.5-10t-126-34t-107-60t-75-92t-32.5-127q0-12 6.5-27.5T434 1127h223q8 0 16 6t8 18q0 29 12.5 51.5T726 1238t44 22.5t47.5 13t43.5 5t32.5 1.5t12.5-1q51-2 96-17t79-48.5t34-81.5q0-130-184-130H823q-34 0-42.5-8.5T771 953V839q1-32 10-40.5t42-8.5h108q184 0 184-130q-1-70-64-106.5T906 514z"
      />
    </svg>
  );
}

export function Vs4Square(props: SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3em"
      height="3em"
      viewBox="0 0 1792 1792"
      {...props}
    >
      <path
        fill="currentColor"
        d="M1456 1q139 0 237.5 98t98.5 237v1120q0 139-98.5 237.5T1456 1792H336q-139 0-237.5-98.5T0 1456V336Q0 197 98.5 99T336 1h1120zM384 926q-15 17-24.5 37.5t-15 34.5t-8 41t-3 37.5t0 45.5t.5 44q0 31 20.5 53t49.5 22h576v178q0 30 22.5 52t52.5 22h127q31 0 53-21.5t22-52.5v-178h132q29 0 49.5-22t20.5-53v-105q0-31-20.5-53t-49.5-22h-132V373q0-30-22-52t-53-22h-127q-149 0-171 25zm256 60q106-123 340-401v401H640z"
      />
    </svg>
  );
}
