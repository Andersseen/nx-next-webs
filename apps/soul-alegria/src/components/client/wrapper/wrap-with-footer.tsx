'use client';
import Footer from '../layout/footer';
import { ReactNode, useRef } from 'react';
import { useInView } from 'framer-motion';

interface WrapWithFooterProps {
  children: ReactNode;
}

export const WrapWithFooter = ({ children }: WrapWithFooterProps) => {
  const footerRef = useRef(null);
  const observedFooterRef = useInView(footerRef, {
    margin: '0px 0px -40px 0px',
  });

  return (
    <>
      <section
        className="content shadow-2xl"
        style={{
          zIndex: '20',
          position: 'relative',
          background: 'var(--main-color)',
        }}
      >
        {children}
      </section>
      <Footer ref={footerRef} isVisible={observedFooterRef} />
    </>
  );
};
