import { CheckedIcon } from '@nx-next/shared';

const FeatureCheck = ({ children }: { children: React.ReactNode }) => (
  <div className="flex items-start gap-4">
    <div className="shrink-0">
      <CheckedIcon />
    </div>
    <h3 className="text-lg font-semibold">{children}</h3>
  </div>
);
export default FeatureCheck;
