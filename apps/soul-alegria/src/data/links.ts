export const LINKS = [
  { label: 'INICIO', route: '/' },
  { label: 'NOSOTROS', route: '/pages/about' },
  { label: 'MISIÓN', route: '/pages/mission' },
  { label: 'PRODUCTOS Y SERVICIOS', route: '/pages/product' },
  { label: 'BLOG', route: '/pages/blog' },
  { label: 'CONTACTO', route: '/pages/contact' },
];
