import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';
import Hero from '../components/client/page/hero';

export default function Page() {
  return (
    <PageWrapperWithTransition>
      <Hero />
    </PageWrapperWithTransition>
  );
}
