import { CheckedIcon, LightWrapper } from '@nx-next/shared';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';

export default function Page() {
  const featureTexts = [
    'Escuchar a nuestros clientes para ofrecer soluciones proactivas, realistas, únicas de Ventas y de Marketing.',
    'ROI A+ es nuestro ADN y con nuestras herramientas financieras (Smart Trading, Media Credit, Comercio Inteligente, Full Barter,…) podrá conseguir más con el mismo presupuesto. ',
    'Tu producto es un modo de pago y puede financiar parte de tus campañas publicitarias.',
    'Ofrecemos nuevas salidas a tu producto tanto nuevos, stock, baja rotación, slow movers,  sobreproducción, etc. a nuevos mercados (nacional e internacional).',
    'Agencia Boutique con una relación única con los medios de comunicación (cubrimos tus necesidades de eventos, acciones promocionales, concursos, flota, electrónica, medios, etc.) a cambio de condiciones ventajosas.',
    'Optimizar tus campañas offline/online con calidad/precios “low cost” donde llevamos planificando, negociando y comprando medios desde 1999. ',
    'Especialización en Medios Exteriores, Programática y Re-Targeting.',
    'Trabajamos en toda Europa.',
  ];

  return (
    <PageWrapperWithTransition>
      <LightWrapper>
        <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
          <div className="max-w-xl">
            <h2 className="text-3xl font-bold sm:text-4xl">
              Que sabemos hacer:
            </h2>
          </div>

          <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
            {featureTexts.map((text, index) => (
              <Feature key={index}>{text}</Feature>
            ))}
          </div>
        </section>
      </LightWrapper>
    </PageWrapperWithTransition>
  );
}

const Feature = ({ children }: { children: React.ReactNode }) => (
  <div className="flex items-start gap-4">
    <div className="shrink-0">
      <CheckedIcon />
    </div>
    <h3 className="text-lg font-semibold">{children}</h3>
  </div>
);
