import {
  Contact,
  FormContact,
} from '@soul-alegria/components/client/page/contact';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';

export default function Page() {
  return (
    <PageWrapperWithTransition>
      <Contact>
        <FormContact />
      </Contact>
    </PageWrapperWithTransition>
  );
}
