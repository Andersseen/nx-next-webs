import { CheckedIcon, LightWrapper } from '@nx-next/shared';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';

export default function Page() {
  const featureTexts = [
    'Servicio de marketing personalizado, atención y dedicación plena a nuestros clientes con nuestra agencia boutique, experta en planificación, negociación y compra en medios de comunicación online-offline.',
    'Nuevas ventas a nuevos mercados identificados. Canales de compradores, tanto nacionales como internacionales (distribuidor, B2B y marketplaces), que le da valor y salida a tu producto. ',
    'Herramientas financieras internacionalmente reconocidas para la mejora de tu productividad como es el Media Credit, Smart Trading, Full Barter, Comercio Inteligente todo para transformar tu ROI en ROI A+.',
    'Realización de proyectos sociales, servicios de accesibilidad y creación de eventos.',
  ];
  return (
    <PageWrapperWithTransition>
      <LightWrapper>
        <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
          <div className="max-w-xl">
            <h2 className="text-3xl font-bold sm:text-4xl">Que ofrecemos:</h2>
          </div>

          <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
            {featureTexts.map((text, index) => (
              <Feature key={index}>{text}</Feature>
            ))}
          </div>
        </section>
      </LightWrapper>
    </PageWrapperWithTransition>
  );
}

const Feature = ({ children }: { children: React.ReactNode }) => (
  <div className="flex items-start gap-4">
    <div className="shrink-0">
      <CheckedIcon />
    </div>
    <h3 className="text-lg font-semibold">{children}</h3>
  </div>
);
