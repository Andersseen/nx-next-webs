import { TextContentData } from '@nx-next/shared';
import { Publicity } from '@soul-alegria/components/client/page/publicity';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';
import FeatureCheck from '@soul-alegria/components/feature-check';
import { PageContent } from '@soul-alegria/components/page-content';

export default function Page() {
  const texteContent: TextContentData = {
    title: `Servicio de marketing personalizado,`,
    paragraphs: [
      `Marketing personalizado: atención y dedicación plena a nuestros clientes con nuestra agencia boutique, planificación, negociación y compra de medios de comunicación online y offline para la consecución de tus objetivos.`,
      'Optimizar tus campañas offline/online con calidad/precios “low cost”.',
      'ROI A+ porque le ofrecemos planteamientos innovadores de tu inversión publicitaria que podrá pagar parte con tu producto o conseguir más publicidad o acciones sociales con el mismo presupuesto.',
      'Agencia Boutique con una relación única con los medios de comunicación (cubrimos sus necesidades de eventos, acciones promocionales, concursos, flota, electrónica, medios, etc.) a cambio de condiciones ventajosas.',
    ],
  };
  const featureTexts = [
    'Agencia Boutique planificando en medios de calidad con condiciones ventajosas y diferenciadoras.',
    'Con tu presupuesto de publicidad y sin que te cueste más, invertimos en acciones sociales.',
    'Dentro de tu inversión te ofreceremos acciones punto de venta (ej: geolocalización).',
    'Posibilidad de pagar parte en producto.',
  ];
  return (
    <PageWrapperWithTransition>
      <PageContent headerData={null} textContentData={texteContent} />
      <Publicity />
      <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
        <h2 className="text-xl font-bold sm:text-4xl text-center">
          GARANTIZAMOS RETORNO DE SU INVERSION:
        </h2>

        <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
          {featureTexts.map((text, index) => (
            <FeatureCheck key={index}>{text}</FeatureCheck>
          ))}
        </div>
      </section>
    </PageWrapperWithTransition>
  );
}
