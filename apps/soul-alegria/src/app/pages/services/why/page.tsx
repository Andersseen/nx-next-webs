import { WhyBenefits } from '@soul-alegria/components/client/page/why/why-benefits';
import {
  WhatBenefits,
  WhyThirdpart,
} from '@soul-alegria/components/client/page/why/why-thirdpart';
import { WhyTop } from '@soul-alegria/components/client/page/why/why-top';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';
import FeatureCheck from '@soul-alegria/components/feature-check';

export default function Page() {
  const comertialBenefits: WhatBenefits = {
    title: 'Ventajas Comerciales',
    benefits: [
      'Ventas adicionales, nuevas oportunidades de Trade, partnerinternacionales. Somos especialistas de producto',
      `Compramos partidas de producto procedentes de sobreproducción,
    promoción, baja rotación, liquidación, etc. y los colocamos en
    mercados identificados tanto al nivel nacional como
    internacional.`,
      ` Nueva antena comercial: mayor visibilidad del producto y nuevos
    clientes compradores.`,
      ` Contará con nuestro experimentado partner de Logístics y de
    Transporte.`,
    ],
    howPart: 'right',
  };
  const financeBenefits: WhatBenefits = {
    title: 'Ventajas Financieras',
    benefits: [
      `Ayudamos a la consecución de objetivos comerciales, marketing y compras,  incremento ventas y optimizando compras con nuestras herramientas de Smart Trading, Media Credit, compensaciona Medios.`,
      `Mejora de liquidez, optimización del balance y de la cuenta de resultados, mejorando el Ebitda.`,
      `Reducción de provisiones contables.`,
      `Analizamos y minimizamos riesgos.`,
    ],
    howPart: 'left',
  };

  const marketingBenefits: WhatBenefits = {
    title: 'Ventajas Marketing y Comunicación',
    benefits: [
      `Gracias a nuestras diferentes formulas (Smart trading, Media Credit, Reciprocidad Comercial, Compensacion a Medios de Comunicación) nos adaptamos a las necesidades de marketing y publicidad de cada cliente, dando más valor a su marca y a su estrategia.`,
      `Somos agencia boutique, optimizamos de los presupuestos de marketing  y comunicación. .`,
      `Acceso a nuevos medios y soportes publicitarios. Especialista en programática RTB y Retargetting. Experiencia en compra de medios desde 1999.`,
      `Si lo desea, podemos colaborar con su agencia de medios y crear una relación win-win`,
    ],
    howPart: 'right',
  };

  const socialBenefits: WhatBenefits = {
    title: 'Acción Social',
    benefits: [
      `Creación de proyectos solidarios a los  colectivos de hipoacúsicos (personas con deficiencias auditivas de diversos tipos). `,
      `Proyectos de accesibilidad en el arte y la cultura para todo el colectivo hipoacúsico con visual guía.`,
      `Ofrecemos servicios de subtitulados para juntas de accionistas, eventos, teatros, comidas sociales o empresariales`,
      `Aportamos  y gestionamos proyectos sociales con patrocinadores, instituciones y todo con ventajas fiscales.`,
    ],
    howPart: 'left',
  };
  const featureTexts = [
    'Tenemos un enfoque único, ya que ofrecemos experiencia en Ventas y en Publicidad, especialmente en medios digitales como DOOH, programática, retargetting y geolocalización. ',
    'Importantes casos de éxito utilizando las herramientas financieras de Media Credit, Full Barter, Smart Trading, Comercio Inteligente todo para convertir tu ROI en ROI A+ Tras un diagnóstico, resolvemos problemáticas y juntos tomamos las mejores decisiones.',
    'Importante conocimiento de la industria de gran consumo - Cosmética, Lujo (Relojes/Alta Bisutería/Joyas), Bebidas, Electrónica/Electrodoméstica, Auto VC-VI-VO, Medios de Comunicación, Académias e Instituciones Españolas.',
  ];
  return (
    <PageWrapperWithTransition>
      <WhyTop />

      <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
        <div className="max-w-xl">
          <h2 className="text-3xl font-bold sm:text-4xl">Quienes somos:</h2>
        </div>

        <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
          {featureTexts.map((text, index) => (
            <FeatureCheck key={index}>{text}</FeatureCheck>
          ))}
        </div>
      </section>

      <WhyBenefits />
      <WhyThirdpart benefits={comertialBenefits} />
      <WhyThirdpart benefits={financeBenefits} />
      <WhyThirdpart benefits={marketingBenefits} />
      <WhyThirdpart benefits={socialBenefits} />
    </PageWrapperWithTransition>
  );
}
