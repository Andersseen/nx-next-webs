import { LightWrapper } from '@nx-next/shared';
import { PageWrapperWithTransition } from '@soul-alegria/components/client/wrapper/new-page-wrapper';
import FeatureCheck from '@soul-alegria/components/feature-check';

export default function Page() {
  const featureTexts = [
    'Equipo multicultural con más de 25 años de experiencia en el mundo de marketing/publicidad y ventas/trade. Asesor financiero con herramientas internacionalmente reconocidas.',
    'Experiencia y soluciones creativa a tus retos de marketing y ventas. ',
    'Creemos en ROI A+ y en la mejora de productividad empresarial.',
    'Nuestros clientes y casos de éxito son nuestra aportación a tus posibles necesidades.',
  ];

  return (
    <PageWrapperWithTransition>
      <LightWrapper>
        <section className="min-w-screen px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
          <div className="max-w-xl">
            <h2 className="text-3xl font-bold sm:text-4xl">Quienes somos:</h2>
          </div>

          <div className="mt-8 lg:mx-16 grid grid-cols-1 gap-8 md:mt-16">
            {featureTexts.map((text, index) => (
              <FeatureCheck key={index}>{text}</FeatureCheck>
            ))}
          </div>
        </section>
      </LightWrapper>
    </PageWrapperWithTransition>
  );
}
